package app.etl;

import biz.c24.io.swift2016.MT564Message;
import org.codehaus.commons.compiler.CompilerFactoryFactory;
import org.codehaus.commons.compiler.IScriptEvaluator;

abstract public class AbstractBaseTest {
    public abstract Object getSwiftMessage();
    protected Object buildAndExecuteExpression(String expression) throws Exception {
        IScriptEvaluator evaluator = CompilerFactoryFactory.getDefaultCompilerFactory().newScriptEvaluator();
        evaluator.setDefaultImports(new String[]{"biz.c24.io.swift2016.*", "org.apache.commons.lang3.StringUtils", "org.joda.time.DateTime", "java.util.Objects", "java.util.Optional"
                , "org.joda.time.format.DateTimeFormat", "org.joda.time.format.ISODateTimeFormat", "java.util.Date", "app.etl.iso15022.util.Utility"});
        evaluator.setReturnType(Object.class);
        evaluator.setParameters(new String[]{"swiftMessage"}, new Class[]{MT564Message.class});
        evaluator.cook(expression);
        try {
            return evaluator.evaluate(new Object[]{getSwiftMessage()});
        } catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }
}
