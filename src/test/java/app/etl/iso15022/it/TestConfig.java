package app.etl.iso15022.it;

import freemarker.template.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;

import java.io.File;

@org.springframework.context.annotation.Configuration
@ComponentScan({"app.etl.iso15022", "app.etl.templating"})
public class TestConfig {
    @Bean
    public freemarker.template.Configuration freeMarkerConfig() throws Exception{
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
        cfg.setDirectoryForTemplateLoading(new ClassPathResource("/templates").getFile());
        cfg.setDefaultEncoding("UTF-8");
        return cfg;
    }
}
