package app.etl.iso15022.it;

import app.etl.iso15022.converter.CamelRouteConverters;
import app.etl.iso15022.parser.RawFileParser;
import biz.c24.io.swift2016.MT564Message;
import org.apache.camel.*;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.dataformat.XmlJsonDataFormat;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.apache.camel.test.spring.CamelTestContextBootstrapper;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import java.util.HashMap;
import java.util.Map;


@RunWith(CamelSpringRunner.class)

@ContextConfiguration(locations = {"classpath:/META-INF/spring/camel-context-iso.xml"}, classes = TestConfig.class)
@BootstrapWith(CamelTestContextBootstrapper.class)
@TestExecutionListeners({DirtiesContextTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ISO15022IntegrationTest {

    private static final Logger LOG = LoggerFactory.getLogger(ISO15022IntegrationTest.class);
    @Autowired
    private CamelContext camelContext;

    @Produce(uri = "direct:start")
    private ProducerTemplate producer;

    @EndpointInject(uri = "mock:catchTestEndpoint")
    private MockEndpoint mockXML;

    private static String[] validFunctions = {"NEWM", "REPL", "REPE"};

    private Resource testFile = new ClassPathResource("data/15022/edi_20171206_Top10_MT564_MT568.txt");
    private Resource testFileSingle = new ClassPathResource("data/15022/mt-564_single.txt");
    @Test
    public void singleMessageFlow() throws Exception {
        testMessageFlow(testFileSingle);
    }
    public void testMessageFlow(Resource resource) throws Exception {
        wireTapRoute("xml", "direct:valid");
        //wireTapRoute("processUpdate", "direct:xml");
        RawFileParser.parseFileAndProduceMessages(resource.getFile()).forEach(aMessage -> {
            try {
                MT564Message swiftMessage = CamelRouteConverters.convertTo(aMessage);
                mockXML.reset();
                mockXML.whenAnyExchangeReceived(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        Document body = exchange.getIn().getBody(Document.class);
                        //new XMLOutputter(Format.getPrettyFormat()).output(new DOMBuilder().build(body), System.out);
                        Assert.assertNotNull(body);
                        writeDocumentToConsole(body);
                        //Assert.assertEquals("C77F8E009597", body.getElementsByTagName("Checksum").item(0).getTextContent());
                    }
                });
                if(ArrayUtils.contains(validFunctions, swiftMessage.getBlock4().getSeqA()
                        .getField23GFunctionOfTheMessage().getG().getFunction())){
                    mockXML.setExpectedMessageCount(1);
                }
                producer.sendBody(aMessage);
                mockXML.assertIsSatisfied();
            } catch (Exception e) {
                LOG.error("Failed message parsing!!", e);
            }
        });
    }

    private void wireTapRoute(String fromRoute, String toRoute) throws Exception {
        Map<String, String> xmlJsonOptions = new HashMap<>();

        xmlJsonOptions.put(XmlJsonDataFormat.ENCODING, "UTF-8");
        xmlJsonOptions.put(XmlJsonDataFormat.FORCE_TOP_LEVEL_OBJECT, "true");
        xmlJsonOptions.put(XmlJsonDataFormat.ROOT_NAME, "Document");
        xmlJsonOptions.put(XmlJsonDataFormat.SKIP_NAMESPACES, "true");
        xmlJsonOptions.put(XmlJsonDataFormat.TRIM_SPACES, "true");
        xmlJsonOptions.put(XmlJsonDataFormat.REMOVE_NAMESPACE_PREFIXES, "true");
        camelContext.getRouteDefinition(fromRoute).adviceWith(camelContext, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                interceptSendToEndpoint(toRoute).multicast().to("mock:catchTestEndpoint", "direct:jsonPipe");
            }
        });
    }
    private void writeDocumentToConsole(Document body) throws Exception{
        DOMImplementationLS domImplementation = (DOMImplementationLS) body.getImplementation();
        LSSerializer lsSerializer = domImplementation.createLSSerializer();
        System.out.println(lsSerializer.writeToString(body));
    }

}
