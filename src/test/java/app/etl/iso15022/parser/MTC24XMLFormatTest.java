package app.etl.iso15022.parser;

import app.etl.iso15022.converter.CamelRouteConverters;
import biz.c24.io.api.C24;
import biz.c24.io.swift2016.MT564Message;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.StringWriter;

/**
 * Created by munia on 16/02/18.
 */
public class MTC24XMLFormatTest {
    @Test
    public void convertToXML() throws Exception {
        MT564Message swiftMessage = CamelRouteConverters.convertTo(RawFileParser.parseFileAndProduceMessages(new ClassPathResource("data/15022/mt-564_single.txt").getFile()).iterator().next());
        StringWriter strWriter = new StringWriter();
        C24.write(swiftMessage).as(C24.Format.XML).to(strWriter);
        Assert.assertTrue(strWriter.toString().length() > 1);
        System.out.println(strWriter.toString());
    }
}
