package app.etl.iso15022.parser;

import biz.c24.io.api.C24;
import biz.c24.io.swift2016.MT564Message;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Created by munia on 13/02/18.
 */
public class RawFileParserTest {
    @Test
    public void shouldBeAbleToParseMessageToSwiftFormat() throws Exception {
        MT564Message mt564Message = RawFileParser.parseFileAndProduceMessages(new File("/home/munia/oneMessage.txt"))
                .stream().map(aMessage -> {
                    try {
                        return C24.parse(MT564Message.class).from(aMessage);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }).findFirst().get();
        Assert.assertNotNull(mt564Message);
    }
}
