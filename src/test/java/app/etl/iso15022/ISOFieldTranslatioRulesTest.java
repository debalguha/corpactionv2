package app.etl.iso15022;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;
import java.util.Optional;

import app.etl.AbstractBaseTest;
import biz.c24.io.swift2016.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.commons.compiler.CompilerFactoryFactory;
import org.codehaus.commons.compiler.IScriptEvaluator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import app.etl.iso15022.converter.CamelRouteConverters;
import app.etl.iso15022.parser.RawFileParser;
import app.etl.iso15022.util.Utility;


/**
 * Created by munia on 15/02/18.
 */
public class ISOFieldTranslatioRulesTest extends AbstractBaseTest{

    private static MT564Message swiftMessage;
    private static String[] field98a2PossibleXpaths = new String[]{"/a/qualifier", "/b/qualifier", "/c/qualifier"};
    private static String[] field69aPossibleXpaths = new String[]{"/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"};

    @BeforeClass
    public static void setup() throws Exception {
        swiftMessage = CamelRouteConverters.convertTo(RawFileParser.parseFileAndProduceMessages(new ClassPathResource("data/15022/mt-564_single.txt").getFile()).iterator().next());
    }

    @Test
    public void shouldBeAbleToLoadCorpActnNtfctnNtfctnTp() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/CorpActnNtfctnNtfctnTp.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("NEWM", result);
    }

    @Test
    public void shouldBeAbleToLoadEvtCmpltnsSts() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/EvtCmpltnsSts.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("COMP", result);
    }

    @Test
    public void shouldBeAbleToLoadEvtConfSts() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/EvtConfSts.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("CONF", result);
    }

    @Test
    public void shouldBeAbleToLoadElgblBalInd() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/ElgblBalInd.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadEvtPrcgTp() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/EvtPrcgTp.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadEvtTp() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/EvtTp.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("CONV", result);
    }

    @Test
    public void shouldBeAbleToLoadMndtryVlntryEvtTp() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/MndtryVlntryEvtTp.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("VOLU", result);
    }

    @Test
    public void shouldBeAbleToLoadPrvsNtfctnIdId() throws Exception {
        //Todo: Clarification required from Seshan
    }

    @Test
    public void shouldBeAbleToLoadPrvsNtfctnIdIdLkgTp() throws Exception {
        //Todo: Clarification required from Seshan
    }

    @Test
    public void shouldBeAbleToLoadInstrId() throws Exception {
        //Todo: Clarification required from Seshan
    }

    @Test
    public void shouldBeAbleToLoadAcctSvcrDocId() throws Exception {
        //Todo: Clarification required from Seshan
    }

    @Test
    public void shouldBeAbleToLoadShrtNb() throws Exception {
        //Todo: Clarification required from Seshan
    }

    @Test
    public void shouldBeAbleToLoadLkgTpCd() throws Exception {
        //Todo: Clarification required from Seshan
    }

    @Test
    public void shouldBeAbleToLoadEvtsLkgLkdOffclCorpActnEvtId() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/EvtsLkgLkdOffclCorpActnEvtId.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadEvtsLkgLkgTp() throws Exception {
        //Todo: Clarification required from Seshan
        //Translation rules seems wrong as Field22a seems like an Array
    }

    @Test
    public void shouldBeAbleToLoadClssActnNb() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/ClssActnNb.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadISIN() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/isin.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("GG00B55K7B92", result);
    }

    @Test
    public void shouldBeAbleToLoadSectyIdntfctnOthrIdntftcn() throws Exception {
        String[] result = Utility.buildOtherIdentificationFromField35BIdentificationOfSecurity(swiftMessage.getBlock4().getSeqB().getField35BIdentificationOfSecurity().getB());
        Assert.assertNotNull(result);
        Assert.assertEquals("RUSP", result[0]);
        Assert.assertEquals("TIKR", result[1]);
    }

    @Test
    public void shouldBeAbleToLoadPlcOfListgMktIdrCd() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/PlcOfListgMktIdrCd.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("PRIM", result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyClssfctnTp() throws Exception {
        //swiftMessage.getBlock4().getSeqB().getSeqB1().getField12aTypeOfFinancialInstrument()[0].getC()
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyClssfctnTp.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("EPRXXR", result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyOptnStyle() throws Exception {
        swiftMessage.getBlock4().getSeqB().getSeqB1().getField12aTypeOfFinancialInstrument();
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyOptnStyle.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyDnmtnCcy() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygDnmtnCcy.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("USD", result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyUndrlygSctyDayCntBsis() throws Exception {
        //swiftMessage.getBlock4().getSeqA().getField98a2().getA().
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyDayCntBsis.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyXpryDt() throws Exception {
        swiftMessage.getBlock4().getSeqA().getField98a2().getA().getDateYYYYMMDD();
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyXpryDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyFltgRateFxgDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyFltgRateFxgDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyMtrtyDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyMtrtyDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyIsseDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyIsseDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyNxtCpnDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyNxtCpnDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyNxtCllblDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyNxtCllblDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyPutblDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyPutblDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyDtdDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyDtdDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyConvsDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyConvsDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyIntrstRate() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyIntrstRate.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals(new BigDecimal("4.12500"), (BigDecimal) result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyNxtIntrstRate() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyNxtIntrstRate.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyPctgOfDebtClm() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyPctgOfDebtClm.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyPrvsFctr() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyPrvsFctr.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyNxtFctr() throws Exception {
        for (Field92aType13 field92aRate : swiftMessage.getBlock4().getSeqB().getSeqB1().getField92aRate()) {
            if (Objects.nonNull(field92aRate.getA()) && field92aRate.getA().getQualifier().equals("NWFC"))
                System.out.println(field92aRate.getA().getSignedRate().getRate());
        }
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyNxtFctr.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyMinNmnlQty() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyMinNmnlQty.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals(new BigDecimal("100000.00000"), (BigDecimal) result);
    }

    @Test
    public void shouldBeAbleToLoadUndrlygSctyCtrctSz() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/UndrlygSctyCtrctSz.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("20.0", result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyQty() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyQty.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyRnncblEntitlmntStsTpPrtryId() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyRnncblEntitlmntStsTpPrtryId.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyRnncblEntitlmntStsTpPrtryIssr() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyRnncblEntitlmntStsTpPrtryIssr.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyRnncblEntitlmntStsTpPrtrySchemeNm() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyRnncblEntitlmntStsTpPrtrySchemeNm.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyRnncblEntitlmntStsTpCd() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyRnncblEntitlmntStsTpCd.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyFrctnDspstnCd() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyFrctnDspstnCd.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyFrctnDspstnPrtryId() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyFrctnDspstnPrtryId.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyFrctnDspstnPrtryIssr() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyFrctnDspstnPrtryIssr.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyFrctnDspstnPrtrySchmeNm() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyFrctnDspstnPrtrySchmeNm.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyIntrmdtSctiesToUndrlygRatioQty1() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesToUndrlygRatioQty1.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyIntrmdtSctiesToUndrlygRatioQty2() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesToUndrlygRatioQty2.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyMktPricAmtPricTp() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesMktPricAmtPricTp.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyMktPricAmtPricPricValCcy() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesMktPricAmtPricPricValCcy.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyMktPricAmtPricPricVal() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesMktPricAmtPricPricVal.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyXpryDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesXpryDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyXpryDtCd() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesXpryDtCd.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyPstngDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesPstngDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyPstngDtCd() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyIntrmdtSctiesPstngDtCd.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyTradgPrdStartDtDtDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyTradgPrdStartDtDtDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyTradgPrdStartDtDtTm() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyTradgPrdStartDtDtTm.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyTradgPrdStartDtNotSpcfdDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyTradgPrdStartDtNotSpcfdDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyTradgPrdEndDtDtDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyTradgPrdEndDtDtDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyTradgPrdEndDtDtTm() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyTradgPrdEndDtDtTm.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyTradgPrdEndDtNotSpcfdDt() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/IntrmdtSctyTradgPrdEndDtNotSpcfdDt.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNull(result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyUinstdBal() throws Exception {
        Object result = Utility.buildIntrmdtSctyBalance(swiftMessage, "UinstdBal", "UNBA");
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadIntrmdtSctyInstdBal() throws Exception {
        Object result = Utility.buildIntrmdtSctyBalance(swiftMessage, "InstdBal", "INBA");
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsIntrmdtSctyAnncmntDt() throws Exception {
        Object result = Utility.getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "AnncmntDt", "ANOU", field98a2PossibleXpaths);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsDtDtlsFctvDt() throws Exception {
        Object result = Utility.getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "FctvDt", "EFFD", field98a2PossibleXpaths);
        Assert.assertNotNull(result);
        System.out.println(result);
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsPrdDtlsPricClctnPrd() throws Exception {
        Object result = Utility.buildPeriod3Choice(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "PricClctnPrd", "PRIC", field69aPossibleXpaths);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsIntrstAcrdNbOfDays() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/CorpActnDtlsIntrstAcrdNbOfDays.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Ignore
    public void shouldBeAbleToLoadCorpActnDtlsCpnNb() throws Exception {
        for (Field13aType12 field13aType12 : swiftMessage.getBlock4().getSeqD().getField13aNumberIdentification()) {
            if (Objects.nonNull(field13aType12.getA()) && field13aType12.getA().getQualifier().equals("COUP")) {
                field13aType12.getA().getNumberIdentificationCode();
            } else if (Objects.nonNull(field13aType12.getB()) && field13aType12.getB().getQualifier().equals("COUP")) {
                if (StringUtils.isEmpty(field13aType12.getB().getDataSourceScheme())) {
                    field13aType12.getB().getNumberIdentificationNumber();
                } else {

                }
            }
        }

        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/CorpActnDtlsIntrstAcrdNbOfDays.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertEquals("", result);
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsDvddTp() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField22a1(), new String[]{"/f/qualifier"}, "DIVI");
        Assert.assertTrue(objOptional.isPresent());
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsOfferTp() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField22a1(), new String[]{"/f/qualifier"}, "OFFE");
        Assert.assertTrue(objOptional.isPresent());
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsEvtStag() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), new String[]{"/f/qualifier"}, "ESTA");
        Assert.assertTrue(objOptional.isPresent());
    }

    @Test
    public void shouldBeAbleToLoadCorpActnDtlsAddtlBizPrcInd() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), new String[]{"/f/qualifier"}, "ADDB");
        Assert.assertFalse(objOptional.isPresent());
    }
    @Test
    public void shouldBeAbleToLoadCorpActnDtlsChngTp() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), new String[]{"/f/qualifier"}, "CHAN");
        Assert.assertTrue(objOptional.isPresent());
    }
    @Test
    public void shouldBeAbleToLoadCorpActnDtlsNewPlcOfIncorprtn() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField94aPlace(), new String[]{"/e/qualifier"}, "NPLI");
        Assert.assertTrue(objOptional.isPresent());
    }
    @Test
    public void shouldBeAbleToLoadCorpActnDtlsOfrr() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField70aNarrative(), new String[]{"/e/qualifier"}, "OFFO");
        Assert.assertTrue(objOptional.isPresent());
        Assert.assertEquals("Scarlet Bid", StringUtils.join(((Field70aType26)objOptional.get()).getE().getNarrative10Lines().getLine35x(), ","));
    }
    @Test
    public void shouldBeAbleToLoadCorpActnDtlsNewCpnyNm() throws Exception {
        Optional<?> objOptional = Utility.filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField70aNarrative(), new String[]{"/e/qualifier"}, "NAME");
        Assert.assertFalse(objOptional.isPresent());
    }

    @Test
    public void shouldBeAbleToLoadPreviousNotifictionId() throws Exception {

    }

    @Override
    public Object getSwiftMessage() {
        return swiftMessage;
    }
}
