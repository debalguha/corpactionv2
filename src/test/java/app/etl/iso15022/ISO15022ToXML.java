package app.etl.iso15022;

import app.etl.iso15022.converter.CamelRouteConverters;
import app.etl.iso15022.parser.RawFileParser;
import biz.c24.io.api.C24;
import biz.c24.io.swift2016.MT564Message;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

public class ISO15022ToXML {
    private static MT564Message swiftMessage;
    @BeforeClass
    public static void setup() throws Exception{
        swiftMessage = CamelRouteConverters.convertTo(RawFileParser.parseFileAndProduceMessages(new ClassPathResource("data/15022/mt-564_single.txt").getFile()).iterator().next());
    }
    @Test
    public void shouldBeAbleToGenerateXML() throws Exception {
        C24.write(swiftMessage).as(C24.Format.XML).to(System.out);
    }
}
