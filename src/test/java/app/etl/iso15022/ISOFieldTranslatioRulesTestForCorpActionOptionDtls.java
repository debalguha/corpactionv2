package app.etl.iso15022;

import app.etl.AbstractBaseTest;
import app.etl.iso15022.converter.CamelRouteConverters;
import app.etl.iso15022.parser.RawFileParser;
import app.etl.iso15022.util.Utility;
import biz.c24.io.swift2016.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.commons.compiler.CompilerFactoryFactory;
import org.codehaus.commons.compiler.IScriptEvaluator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;


/**
 * Created by munia on 15/02/18.
 */
public class ISOFieldTranslatioRulesTestForCorpActionOptionDtls extends AbstractBaseTest{
    private static MT564Message swiftMessage;
    private static String[] field98a2PossibleXpaths = new String[]{"/a/qualifier", "/b/qualifier", "/c/qualifier"};
    private static String[] field69aPossibleXpaths = new String[]{"/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"};

    @BeforeClass
    public static void setup() throws Exception {
        swiftMessage = CamelRouteConverters.convertTo(RawFileParser.parseFileAndProduceMessages(new ClassPathResource("data/15022/mt-564_single.txt").getFile()).iterator().next());
    }

    @Override
    public Object getSwiftMessage() {
        return swiftMessage;
    }

    @Ignore
    public void shouldBeAbleToLoadCorpActnNtfctnNtfctnTp() throws Exception {
        String janinoExpression = FileUtils.readFileToString(new FileSystemResource("config/janino/CorpActnNtfctnNtfctnTp.janino").getFile(), "UTF-8");
        Object result = buildAndExecuteExpression(janinoExpression);
        Assert.assertNotNull(result);
        Assert.assertEquals("NEWM", result);
    }
    @Test
    public void shouldBeAbleToLoadCorpActnOptnDtlsOptnNb() throws Exception {
        Assert.assertEquals("001", swiftMessage.getBlock4().getSeqE()[0].getField13aNumberIdentification().getA().getNumberIdentificationCode());
    }
    @Test
    public void shouldBeAbleToLoadCorpActnOptnDtlsOptnTp() throws Exception {
        Assert.assertEquals("SECU", ((Field22aType97)Utility.filterArrayWithQualifier(swiftMessage.getBlock4().getSeqE()[0].getField22a1(), new String[]{"/f/qualifier"}, "CAOP").get()).getF().getIndicator());
    }
    @Test
    public void shouldBeAbleToLoadCorpActnOptnDtlsOptnAvlbtySts() throws Exception {
        Assert.assertEquals("SECU", ((Field22aType97)Utility.filterArrayWithQualifier(swiftMessage.getBlock4().getSeqE()[0].getField22a1(), new String[]{"/f/qualifier"}, "OSTA").get()).getF().getIndicator());
    }
    @Test
    public void shouldBeAbleToLoadCorpActnOptnDtlsOptnCcyOptn() throws Exception {
        Assert.assertEquals("GBP", swiftMessage.getBlock4().getSeqE()[0].getField11ACurrency().getA().getCurrencyCode());
    }
    @Test
    public void shouldBeAbleToLoadCorpActnOptnDtlsDtDtlsEarlyRspnDdln() throws Exception {
        swiftMessage.getBlock4().getSeqB().getSeqB2()[0].getField97aAccount().getC().getAccountCode();
        swiftMessage.getBlock4().getSeqB().getSeqB2()[0].getField94aPlace().getF().getBIC().getBankCode();
        swiftMessage.getBlock4().getSeqB().getSeqB2()[0].getField93aBalance()[0].getB().getSignedBalance().getBalance();
        swiftMessage.getBlock4().getSeqB().getSeqB2()[0].getField93aBalance()[0].getC().getBalanceTypeCode();


        swiftMessage.getBlock4().getSeqF().getField95aParty()[0].getR().getPartyProprietaryCode();

        MT564SequenceECorporateActionOptions corporateActionOption = swiftMessage.getBlock4().getSeqE()[0];
        for (MT564SequenceE2CashMovements corpActnOptnSeqE2 : corporateActionOption.getSeqE2()) {
            corpActnOptnSeqE2.getField22a1()[0].getH().getIndicator();
            corpActnOptnSeqE2.getField92aRate()[0].getJ().getRateStatusCode();
            corpActnOptnSeqE2.getField90aPrice()[0].getJ();
        }

        MT564SequenceE1SecuritiesMovement corpActnOptnSeqE1 = swiftMessage.getBlock4().getSeqE()[0].getSeqE1()[0];
        corpActnOptnSeqE1.getField36aQuantityOfFinancialInstrument();
        corpActnOptnSeqE1.getField98a2()[0].getA().getDateYYYYMMDD();
        corpActnOptnSeqE1.getField98a2()[0].getC().getTimeHHMMSS();
        corpActnOptnSeqE1.getField98a2()[0].getE().getUTCIndicator();
        corpActnOptnSeqE1.getField98a2()[0].getB().getDateCode();

        corporateActionOption.getField70aNarrative()[0].getE().getNarrative10Lines().getLine35x();
        corporateActionOption.getField70aNarrative()[0].getE();
        /*corpActnOptnSeqE1.getField11ACurrency().getA().getCurrencyCode();
        corpActnOptnSeqE1.getField69aPeriod();
        corpActnOptnSeqE1.getField90aPrice();*/
        //corpActnOptnSeqE1.getField92aRate()[0].getA();
        //swiftMessage.getBlock4().getSeqE()[0].getSeqE1()[0].getField22a1()[0].getF().getIndicator()
        //swiftMessage.getBlock4().getSeqE()[0].getField36aQuantityOfFinancialInstrument()
        //swiftMessage.getBlock4().getSeqE()[0].getField90aPrice()
        //swiftMessage.getBlock4().getSeqE()[0].getField92aRate()[1].getJ().getRateTypeCode()
        /*MT564SequenceECorporateActionOptions.Field35BType field35BIdentificationOfSecurity = swiftMessage.getBlock4().getSeqE()[0].getField98a2();
        field35BIdentificationOfSecurity.getB().getField35BIdOfSecurity().getISINNumber();
        field35BIdentificationOfSecurity.getB().getField35BIdOfSecurity().getIdentificationOfSecurity().getDescriptionOfSecurity4Lines().getLine35x();
        Assert.assertEquals("GBP", swiftMessage.getBlock4().getSeqE()[0].getField11ACurrency().getA().getCurrencyCode());*/
    }
}
