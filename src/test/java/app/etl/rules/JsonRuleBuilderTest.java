package app.etl.rules;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.util.Collection;

public class JsonRuleBuilderTest {
    @Test
    public void shouldBeAbleToBuildRulesFromFile() throws Exception {
        Collection<Rule> rules = JsonRuleBuilder.buildRules(new ClassPathResource("mapping-validations.json"));
        Assert.assertNotNull(rules);
        Assert.assertEquals(1, rules.size());
        Assert.assertEquals("CP_D", rules.iterator().next().getForKey());
    }
}
