<#assign field98a2PossibleXpaths = ["/a/qualifier", "/b/qualifier", "/c/qualifier"]>
<#assign field69aPossibleXpaths = ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"]>
<#import "module/lib/Macros.ftl" as macros/>
<#import "module/RateAndAmtDtls.ftl" as rateAndAmtDtls/>
<#import "module/PriceDtls.ftl" as priceDtls/>
<#import "module/SctiesQty.ftl" as sctiesQty/>
<#import "module/SctiesMvmntDtls.ftl" as sctiesMvmnt/>
<#import "module/CashMvmntDtls.ftl" as cashMvmnt/>
<#import "module/AcctsListAndBalDtls.ftl" as acctsListAndBal>
<?xml version="1.0" encoding="UTF-8"?>
<p:Document xmlns:p="p:iso:std:iso:20022:tech:xsd:seev.031.001.07" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="p:iso:std:iso:20022:tech:xsd:seev.031.001.07 seev.031.001.07.xsd ">
    <p:CorpActnNtfctn>
        <p:Pgntn>
            <p:PgNb/>
            <p:LastPgInd/>
        </p:Pgntn>
        <p:NtfctnGnlInf>
            <p:NtfctnTp>${(CorpActnNtfctnNtfctnTp)!}</p:NtfctnTp>
            <p:PrcgSts>
                <p:Cd>
                    <p:EvtCmpltnsSts>${(EvtCmpltnsSts)!}</p:EvtCmpltnsSts>
                    <p:EvtConfSts>${(EvtConfSts)!}</p:EvtConfSts>
                </p:Cd>
                <#--<p:Prtry>
                    <p:Id>string</p:Id>
                    <p:Issr>string</p:Issr>
                    <!--Optional:&ndash;&gt;
                    <p:SchmeNm>string</p:SchmeNm>
                </p:Prtry>-->
            </p:PrcgSts>
            <p:ElgblBalInd>${(ElgblBalInd)!}</p:ElgblBalInd>
        </p:NtfctnGnlInf>
        <p:PrvsNtfctnId>
            <p:Id></p:Id>
            <!--Optional:-->
            <p:LkgTp>
                <!--You have a CHOICE of the next 2 items at this level-->
                <p:Cd>AFTE</p:Cd>
                <p:Prtry>
                    <p:Id>string</p:Id>
                    <p:Issr>string</p:Issr>
                    <!--Optional:-->
                    <p:SchmeNm>string</p:SchmeNm>
                </p:Prtry>
            </p:LkgTp>
        </p:PrvsNtfctnId>
        <p:InstrId>
            <p:Id>p:Id</p:Id>
        </p:InstrId>
        <p:OthrDocId>
            <p:Id>
                <p:AcctSvcrDocId>p:AcctSvcrDocId</p:AcctSvcrDocId>
            </p:Id>
            <p:DocNb>
                <p:ShrtNb>100</p:ShrtNb>
            </p:DocNb>
            <p:LkgTp>
                <p:Cd>AFTE</p:Cd>
            </p:LkgTp>
        </p:OthrDocId>
        <p:EvtsLkg>
            <p:EvtId>
                <p:LkdOffclCorpActnEvtId>${(EvtsLkgLkdOffclCorpActnEvtId)!}</p:LkdOffclCorpActnEvtId>
            </p:EvtId>
            <p:LkgTp>
                <p:Cd>AFTE</p:Cd>
            </p:LkgTp>
        </p:EvtsLkg>
        <p:CorpActnGnlInf>
            <p:CorpActnEvtId>p:CorpActnEvtId</p:CorpActnEvtId>
            <p:OffclCorpActnEvtId>p:OffclCorpActnEvtId</p:OffclCorpActnEvtId>
            <p:ClssActnNb>CACN</p:ClssActnNb>
            <p:EvtPrcgTp>
                <#assign CorpActnGnlInfEvtPrcgTp=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqA().getField22a1(),  ["/f/qualifier"], "CAEP")/>
                <@macros.ProprietaryIdentificationWithOptional CorpActnGnlInfEvtPrcgTp "f" "indicator" "Cd"/>
            </p:EvtPrcgTp>
            <p:EvtTp>
                <#assign CorpActnGnlInfEvtTp=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqA().getField22a1(),  ["/f/qualifier"], "CAEV")/>
                <@macros.ProprietaryIdentificationWithOptional CorpActnGnlInfEvtTp "f" "indicator" "Cd"/>
            </p:EvtTp>
            <p:MndtryVlntryEvtTp>
                <#assign CorpActnGnlInfMndtryVlntryEvtTp=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqA().getField22a1(),  ["/f/qualifier"], "CAMV")/>
                <@macros.ProprietaryIdentificationWithOptional CorpActnGnlInfMndtryVlntryEvtTp "f" "indicator" "Cd"/>
            </p:MndtryVlntryEvtTp>
            <p:UndrlygScty>
                <#if swiftMessage.block4.seqB.field35BIdentificationOfSecurity.b??>
                    <p:FinInstrmId>
                        <@macros.FinInstrmId field35BIdentificationOfSecurity=swiftMessage.block4.seqB.field35BIdentificationOfSecurity/>
                    </p:FinInstrmId>
                </#if>
                <p:PlcOfListg>
                    <p:MktIdrCd>${(swiftMessage.block4.seqB.seqB1.field94BPlace.b.placeCode)!}</p:MktIdrCd>
                </p:PlcOfListg>
                <p:DayCntBsis>
                    <#if (swiftMessage.block4.seqB.seqB1.field22FIndicator.f)??>
                        <@macros.ProprietaryIdentification swiftMessage.block4.seqB.seqB1.field22FIndicator.f "indicator" "Cd"/>
                    </#if>`
                </p:DayCntBsis>
                <p:ClssfctnTp>
                    <#--<p:ClssfctnFinInstrm>${(UndrlygSctyClssfctnTp)!}</p:ClssfctnFinInstrm>-->
                    <@macros.FinInstrClssfctnTp classFctnObj=swiftMessage.getBlock4().getSeqB().getSeqB1() possibleXPaths=["/a/qualifier", "/c/qualifier"] qualifier="CLAS"/>
                </p:ClssfctnTp>
                <p:OptnStyle>
                    <@macros.UndrlygSctyOptnStyle classFctnObj=swiftMessage.getBlock4().getSeqB().getSeqB1() possibleXPaths=["/b/qualifier"] qualifier="OPST"/>
                </p:OptnStyle>
                <p:DnmtnCcy>${(UndrlygSctyDnmtnCcy)!}</p:DnmtnCcy>
                <p:NxtCpnDt>${(macros.extractDateFromField98A2(swiftMessage, "COUP")?iso_utc)!""}</p:NxtCpnDt>
                <p:XpryDt>${(macros.extractDateFromField98A2(swiftMessage, "EXPI")?iso_utc)!""}</p:XpryDt>
                <p:FltgRateFxgDt>${(macros.extractDateFromField98A2(swiftMessage, "FRNR")?iso_utc)!""}</p:FltgRateFxgDt>
                <p:MtrtyDt>${(macros.extractDateFromField98A2(swiftMessage, "MATU")?iso_utc)!""}</p:MtrtyDt>
                <p:IsseDt>${(macros.extractDateFromField98A2(swiftMessage, "ISSU")?iso_utc)!""}</p:IsseDt>
                <p:NxtCllblDt>${(macros.extractDateFromField98A2(swiftMessage, "CALD")?iso_utc)!""}</p:NxtCllblDt>
                <p:PutblDt>${(macros.extractDateFromField98A2(swiftMessage, "PUTT")?iso_utc)!""}</p:PutblDt>
                <p:DtdDt>${(macros.extractDateFromField98A2(swiftMessage, "DDTE")?iso_utc)!""}</p:DtdDt>
                <p:ConvsDt>${(macros.extractDateFromField98A2(swiftMessage, "CONV")?iso_utc)!""}</p:ConvsDt>
                <@rateAndAmtDtls.RateFormat12Choice tag="IntrstRate" qualifier="INTR" possibleXPaths=["/a/qualifier", "/k/qualifier"] field92aRateArray=swiftMessage.getBlock4().getSeqB().getSeqB1().getField92aRate()/>
                <@rateAndAmtDtls.RateFormat12Choice tag="NxtIntrstRate" qualifier="NXRT" possibleXPaths=["/a/qualifier", "/k/qualifier"] field92aRateArray=swiftMessage.getBlock4().getSeqB().getSeqB1().getField92aRate()/>
                <@rateAndAmtDtls.RateFormat12Choice tag="PctgOfDebtClm" qualifier="DECL" possibleXPaths=["/a/qualifier", "/k/qualifier"] field92aRateArray=swiftMessage.getBlock4().getSeqB().getSeqB1().getField92aRate()/>
                <@rateAndAmtDtls.RateFormat12Choice tag="PrvsFctr" qualifier="PRFC" possibleXPaths=["/a/qualifier", "/k/qualifier"] field92aRateArray=swiftMessage.getBlock4().getSeqB().getSeqB1().getField92aRate()/>
                <@rateAndAmtDtls.RateFormat12Choice tag="NxtFctr" qualifier="NWFC" possibleXPaths=["/a/qualifier", "/k/qualifier"] field92aRateArray=swiftMessage.getBlock4().getSeqB().getSeqB1().getField92aRate()/>
                <p:WarrtParity>
                    <p:Qty1>0.0</p:Qty1>
                    <p:Qty2>0.0</p:Qty2>
                </p:WarrtParity>
                <p:MinNmnlQty>
                    <#assign undrlygSctyMinNmnlQty=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqB().getSeqB1().getField36aQuantityOfFinancialInstrument(), ["/b/qualifier"], "MINO")/>
                    <#if undrlygSctyMinNmnlQty.isPresent()>
                        <@sctiesQty.FinancialInstrumentQuantity1Choice undrlygSctyMinNmnlQty.get().b/>
                    </#if>
                </p:MinNmnlQty>
                <p:CtrctSz>
                    <#assign undrlygSctyMinNmnlQty=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqB().getSeqB1().getField36aQuantityOfFinancialInstrument(), ["/b/qualifier"], "SIZE")/>
                    <#if undrlygSctyMinNmnlQty.isPresent()>
                        <@sctiesQty.FinancialInstrumentQuantity1Choice undrlygSctyMinNmnlQty.get().b/>
                    </#if>
                </p:CtrctSz>
            </p:UndrlygScty>
        </p:CorpActnGnlInf>
        <p:AcctDtls>
            <!--You have a CHOICE of the next 2 items at this level-->
            <#assign seqB2=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.block4.seqB.getSeqB2(), ["/field97aAccount/c/qualifier"], "SAFE")/>
            <#if seqB2.isPresent() && seqB2.get().size() == 1 && seqB2.get()[0].field97aAccount.c??>
                <p:ForAllAccts>
                    <p:IdCd>${seqB2.get()[0].field97aAccount.c.accountCode}</p:IdCd>
                </p:ForAllAccts>
            <#else>
                <@acctsListAndBal.AcctsListAndBalDtls swiftMessage.block4.seqB.getSeqB2()/>
            </#if>
        </p:AcctDtls>
        <p:IntrmdtScty>
            <p:SctyId>
                <p:ISIN>${(isin)!}</p:ISIN>
            </p:SctyId>
            <p:Qty>${(IntrmdtSctyQty)!}</p:Qty>
            <p:RnncblEntitlmntStsTp>
                <p:Cd>${(IntrmdtSctyRnncblEntitlmntStsTpCd)!}</p:Cd>
                <p:Prtry>
                    <p:Id>${(IntrmdtSctyRnncblEntitlmntStsTpPrtrId)!}</p:Id>
                    <p:Issr>${(IntrmdtSctyRnncblEntitlmntStsTpPrtrIssr)!}</p:Issr>
                    <!--Optional:-->
                    <p:SchmeNm>${(IntrmdtSctyRnncblEntitlmntStsTpPrtrSchemeNm)!}</p:SchmeNm>
                </p:Prtry>
            </p:RnncblEntitlmntStsTp>
            <p:FrctnDspstn>
                <!--You have a CHOICE of the next 2 items at this level-->
                <p:Cd>${(IntrmdtSctyFrctnDspstnCd)!}</p:Cd>
                <p:Prtry>
                    <p:Id>${(IntrmdtSctyFrctnDspstnPrtryId)!}</p:Id>
                    <p:Issr>${(IntrmdtSctyFrctnDspstnPrtryIssr)!}</p:Issr>
                    <!--Optional:-->
                    <p:SchmeNm>${(IntrmdtSctyFrctnDspstnPrtrySchmeNm)!}</p:SchmeNm>
                </p:Prtry>
            </p:FrctnDspstn>
            <p:IntrmdtSctiesToUndrlygRatio>
                <p:Qty1>${(IntrmdtSctyIntrmdtSctiesToUndrlygRatioQty1)!}</p:Qty1>
                <p:Qty2>${(IntrmdtSctyIntrmdtSctiesToUndrlygRatioQty2)!}</p:Qty2>
            </p:IntrmdtSctiesToUndrlygRatio>
            <p:MktPric>
                <p:AmtPricTp>${(IntrmdtSctyIntrmdtSctiesMktPricAmtPricTp)!}</p:AmtPricTp>
                <p:PricVal Ccy="${(IntrmdtSctyIntrmdtSctiesMktPricAmtPricPricValCcy)!}">${(IntrmdtSctyIntrmdtSctiesMktPricAmtPricPricVal)!}</p:PricVal>
            </p:MktPric>
            <p:XpryDt>
                <!--You have a CHOICE of the next 2 items at this level-->
                <p:Dt>${(IntrmdtSctyIntrmdtSctiesXpryDt)!}</p:Dt>
                <p:DtCd>
                    <!--You have a CHOICE of the next 2 items at this level-->
                    <p:Cd>${(IntrmdtSctyIntrmdtSctiesXpryDtCd)!}</p:Cd>
                </p:DtCd>
            </p:XpryDt>
            <p:PstngDt>
                <!--You have a CHOICE of the next 2 items at this level-->
                <p:Dt>${(IntrmdtSctyIntrmdtSctiesPstngDt)!}</p:Dt>
                <p:DtCd>
                    <!--You have a CHOICE of the next 2 items at this level-->
                    <p:Cd>${(IntrmdtSctyIntrmdtSctiesPstngDtCd)!}</p:Cd>
                </p:DtCd>
            </p:PstngDt>
            <!--Optional:-->
            <p:TradgPrd>
                <p:StartDt>
                    <!--You have a CHOICE of the next 2 items at this level-->
                    <p:Dt>
                        <!--You have a CHOICE of the next 2 items at this level-->
                        <p:Dt>${(IntrmdtSctyTradgPrdStartDtDtDt)!}</p:Dt>
                        <p:DtTm>${(IntrmdtSctyTradgPrdStartDtDtTm)!}</p:DtTm>
                    </p:Dt>
                    <p:NotSpcfdDt>${(IntrmdtSctyTradgPrdStartDtNotSpcfdDt)!}</p:NotSpcfdDt>
                </p:StartDt>
                <p:EndDt>
                    <!--You have a CHOICE of the next 2 items at this level-->
                    <p:Dt>
                        <!--You have a CHOICE of the next 2 items at this level-->
                        <p:Dt>${(IntrmdtSctyTradgPrdEndDtDtDt)!}</p:Dt>
                        <p:DtTm>${(IntrmdtSctyTradgPrdEndDtDtTm)!}</p:DtTm>
                    </p:Dt>
                    <p:NotSpcfdDt>${(IntrmdtSctyTradgPrdEndDtNotSpcfdDt)!}</p:NotSpcfdDt>
                </p:EndDt>
            </p:TradgPrd>
            <!--Optional:-->
            <#assign IntrmdtSctyUinstdBal= statics['app.etl.iso15022.util.Utility'].buildIntrmdtSctyBalance(swiftMessage, "UinstdBal", "UNBA")/>
            ${(IntrmdtSctyUinstdBal)!}
            <#assign IntrmdtSctyInstdBal= statics['app.etl.iso15022.util.Utility'].buildIntrmdtSctyBalance(swiftMessage, "InstdnstdBal", "INBA")/>
            ${(IntrmdtSctyInstdBal)!}
        </p:IntrmdtScty>
        <p:CorpActnDtls>
            <p:DtDtls>
            <#assign CorpActnDtlsDtDtlsAnncmntDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "AnncmntDt", "ANOU", field98a2PossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsAnncmntDt)!}
            <#assign CorpActnDtlsDtDtlsCertfctnDdln= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "CertfctnDdln", "CERT", field98a2PossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsCertfctnDdln)!}
            <#assign CorpActnDtlsDtDtlsCrtApprvlDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "CrtApprvlDt", "COAP", field98a2PossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsCrtApprvlDt)!}                
            <#assign CorpActnDtlsDtDtlsEarlyClsgDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "EarlyClsgDt", "ECDT", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsEarlyClsgDt)!}                
            <#assign CorpActnDtlsDtDtlsFctvDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "FctvDt", "EFFD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsFctvDt)!}
            <#assign CorpActnDtlsDtDtlsEqulstnDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "EqulstnDt", "EQUL", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsEqulstnDt)!}            	
            <#assign CorpActnDtlsDtDtlsFrthrDtldAnncmntDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "FrthrDtldAnncmntDt", "FDAT", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsFrthrDtldAnncmntDt)!}
                <p:FxgDt>
                    <p:Dt>
                        <p:Dt>2001-01-01</p:Dt>
                    </p:Dt>
                </p:FxgDt>
            <#assign CorpActnDtlsDtDtlsLtryDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "LtryDt", "LOTO", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsLtryDt)!} 
            <#assign CorpActnDtlsDtDtlsNewMtrtyDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "NewMtrtyDt", "MATU", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsNewMtrtyDt)!} 
            <#assign CorpActnDtlsDtDtlsMtgDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "MtgDt", "MEET", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsMtgDt)!}                  
            <#assign CorpActnDtlsDtDtlsMrgnFxgDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "MrgnFxgDt", "MFIX", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsMrgnFxgDt)!}                
            <#assign CorpActnDtlsDtDtlsPrratnDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "PrratnDt", "PROD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsPrratnDt)!}  
            <#assign CorpActnDtlsDtDtlsRcrdDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "RcrdDt", "RDTE", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsRcrdDt)!}              	              
            <#assign CorpActnDtlsDtDtlsRegnDdln= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "RegnDdln", "REGI", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsRegnDdln)!} 
            <#assign CorpActnDtlsDtDtlsRsltsPblctnDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "RsltsPblctnDt", "RESU", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsRsltsPblctnDt)!}    
            <#assign CorpActnDtlsDtDtlsDdlnToSplt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "DdlnToSplt", "SPLT", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsDdlnToSplt)!}  
            <#assign CorpActnDtlsDtDtlsDdlnForTaxBrkdwnInstr= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "DdlnForTaxBrkdwnInstr", "TAXB", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsDdlnForTaxBrkdwnInstr)!}        
            <#assign CorpActnDtlsDtDtlsTradgSspdDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "TradgSspdDt", "TSDT", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsTradgSspdDt)!}   
            <#assign CorpActnDtlsDtDtlsUcondlDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "UcondlDt", "UNCO", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsUcondlDt)!}   
            <#assign CorpActnDtlsDtDtlsWhlyUcondlDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "WhlyUcondlDt", "WUCO", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsWhlyUcondlDt)!}   
                <p:ExDvddDt>
                    <p:Dt>
                        <p:Dt>2001-01-01</p:Dt>
                    </p:Dt>
                </p:ExDvddDt>
            <#assign CorpActnDtlsDtDtlsOffclAnncmntPblctnDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "OffclAnncmntPblctnDt", "OAPD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsOffclAnncmntPblctnDt)!}  
            <#assign CorpActnDtlsDtDtlsSpclExDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "SpclExDt", "SXDT", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsSpclExDt)!} 
            <#assign CorpActnDtlsDtDtlsGrntedPrtcptnDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "GrntedPrtcptnDt", "GUPA", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsGrntedPrtcptnDt)!}   
            <#assign CorpActnDtlsDtDtlsElctnToCtrPtyMktDdln = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "ElctnToCtrPtyMktDdln", "ECPD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsElctnToCtrPtyMktDdln)!}   
                <p:ElctnToCtrPtyRspnDdln>
                    <p:Dt>
                        <p:Dt>2001-01-01</p:Dt>
                    </p:Dt>
                </p:ElctnToCtrPtyRspnDdln>
            <#assign CorpActnDtlsDtDtlsLpsdDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "LpsdDt", "LAPD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsLpsdDt)!} 
            <#assign CorpActnDtlsDtDtlsPmtDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "PmtDt", "PAYD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsPmtDt)!}
            <#assign CorpActnDtlsDtDtlsThrdPtyDdln = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "ThrdPtyDdln", "TPDT", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsThrdPtyDdln)!}
            <#assign CorpActnDtlsDtDtlsEarlyThrdPtyDdln = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "EarlyThrdPtyDdln", "ETPD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsEarlyThrdPtyDdln)!}
            <#assign CorpActnDtlsDtDtlsMktClmTrckgEndDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "MktClmTrckgEndDt", "MCTD", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsMktClmTrckgEndDt)!}  
            <#assign CorpActnDtlsDtDtlsLeadPlntffDdln = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField98a2(), "LeadPlntffDdln", "PLDT", field98a2PossibleXpaths)/>
            	${(CorpActnDtlsDtDtlsLeadPlntffDdln)!}                    
                <p:FilgDt>
                    <p:Dt>2001-01-01</p:Dt>
                </p:FilgDt>
                <p:HrgDt>
                    <p:Dt>2001-01-01</p:Dt>
                </p:HrgDt>
            </p:DtDtls>
            <p:PrdDtls>
                <#assign CorpActnDtlsDtDtlsPrdDtlsPricClctnPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "PricClctnPrd", "PRIC", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsPricClctnPrd)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsIntrstPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "IntrstPrd", "INPE", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsIntrstPrd)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsCmplsryPurchsPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "CmplsryPurchsPrd", "CSPD", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsCmplsryPurchsPrd)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsBlckgPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "BlckgPrd", "BLOK", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsBlckgPrd)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsClmPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "ClmPrd", "CLCP", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsClmPrd)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForBookNtryTrf = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForBookNtryTrf", "DSBT", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForBookNtryTrf)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForDpstAtAgt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForDpstAtAgt", "DSDA", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForDpstAtAgt)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForDpst = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForDpst", "DSDE", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForDpst)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForPldg = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForPldg", "DSPL", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForPldg)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForSgrtn = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForSgrtn", "DSSE", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForSgrtn)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwlAtAgt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForWdrwlAtAgt", "DSWA", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwlAtAgt)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwlInNmneeNm = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForWdrwlInNmneeNm", "DSWN", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwlInNmneeNm)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwlInStrtNm = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "DpstrySspnsnPrdForWdrwlInStrtNm", "DSWS", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwlInStrtNm)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsBookClsrPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "BookClsrPrd", "BOCL", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsBookClsrPrd)!}
                <#assign CorpActnDtlsDtDtlsPrdDtlsBookClsrPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(swiftMessage.getBlock4().getSeqD().getField69aPeriod(), "BookClsrPrd", "BOCL", field69aPossibleXpaths)/>
                ${(CorpActnDtlsDtDtlsPrdDtlsBookClsrPrd)!}
                <p:CoDpstriesSspnsnPrd>
                    <p:Prd>
                        <p:StartDt>
                            <p:Dt>
                                <p:Dt>2001-01-01</p:Dt>
                            </p:Dt>
                        </p:StartDt>
                        <p:EndDt>
                            <p:Dt>
                                <p:Dt>2001-01-01</p:Dt>
                            </p:Dt>
                        </p:EndDt>
                    </p:Prd>
                </p:CoDpstriesSspnsnPrd>
                <p:SpltPrd>
                    <p:Prd>
                        <p:StartDt>
                            <p:Dt>
                                <p:Dt>2001-01-01</p:Dt>
                            </p:Dt>
                        </p:StartDt>
                        <p:EndDt>
                            <p:Dt>
                                <p:Dt>2001-01-01</p:Dt>
                            </p:Dt>
                        </p:EndDt>
                    </p:Prd>
                </p:SpltPrd>
            </p:PrdDtls>
            <@rateAndAmtDtls.CorpActnDtlsRateAndAmtDtlsRateAndAmtDtls rateVar=swiftMessage.getBlock4().getSeqD().getField92aRate()/>
            <@priceDtls.CorpActnDtlsPricDtls/>
            <@sctiesQty.CorpActnDtlsSctiesQty/>
            <p:IntrstAcrdNbOfDays>${CorpActnDtlsIntrstAcrdNbOfDays}</p:IntrstAcrdNbOfDays>
            <#if swiftMessage.getBlock4().getSeqD()??>
                <#list swiftMessage.getBlock4().getSeqD().getField13aNumberIdentification() as field31Array>
                    <p:CpnNb>
                        <@macros.IdentificationFormat3Choice(field31Array, "COUP")/>
                    </p:CpnNb>
                </#list>
            </#if>
            <p:CertfctnBrkdwnInd>${(statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField17BFlag(), ["/qualifier"], "CERT").getB().getFlag())???c}</p:CertfctnBrkdwnInd>
            <p:ChrgsApldInd>${(statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField17BFlag(), ["/qualifier"], "RCHG").getB().getFlag())???c}</p:ChrgsApldInd>
            <p:RstrctnInd>${(statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField17BFlag(), ["/qualifier"], "COMP").getB().getFlag())???c}</p:RstrctnInd>
            <p:AcrdIntrstInd>${(statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField17BFlag(), ["/qualifier"], "ACIN").getB().getFlag())???c}</p:AcrdIntrstInd>
            <p:LttrOfGrntedDlvryInd>true</p:LttrOfGrntedDlvryInd>
            <p:DvddTp>
                <#assign arrayObj=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "DIVI")/>
                <#if arrayObj.isPresent()>
                    <@macros.ProprietaryIdentification propIdntFctnObj=arrayObj.get().getF() byWhat="indicator"/>
                </#if>
            </p:DvddTp>
            <p:ConvsTp>
            <#assign arrayObj=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "CONV")/>
            <#if arrayObj.isPresent()>
                <@macros.ProprietaryIdentification propIdntFctnObj=arrayObj.get().getF() byWhat="indicator"/>
            </#if>
            </p:ConvsTp>
            <p:OcrncTp>
                <p:Cd>FINL</p:Cd>
            </p:OcrncTp>
            <p:OfferTp>
            <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "OFFE")/>
            <#if arrayObjs.isPresent()>
                <#list arrayObjs.get() as item>
                    <@macros.ProprietaryIdentification propIdntFctnObj=item.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:OfferTp>
            <p:RnncblEntitlmntStsTp>
            <#assign arrayObj=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "SELL")/>
            <#if arrayObj.isPresent()>
                <@macros.ProprietaryIdentification propIdntFctnObj=arrayObj.get().getF() byWhat="indicator"/>
            </#if>
            </p:RnncblEntitlmntStsTp>
            <p:EvtStag>
            <#assign arrayObjsEvt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "ESTA")/>
            <#if arrayObjsEvt.isPresent()>
                <#list arrayObjsEvt.get() as itemEvt>
                    <@macros.ProprietaryIdentification propIdntFctnObj=itemEvt.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:EvtStag>
            <p:AddtlBizPrcInd>
            <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "ADDB")/>
            <#if arrayObjs.isPresent()>
                <#list arrayObjs.get() as item>
                    <@macros.ProprietaryIdentification propIdntFctnObj=item.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:AddtlBizPrcInd>
            <p:ChngTp>
            <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "CHAN")/>
            <#if arrayObjs.isPresent()>
                <#list arrayObjs.get() as item>
                    <@macros.ProprietaryIdentification propIdntFctnObj=item.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:ChngTp>
            <p:IntrmdtSctiesDstrbtnTp>
            <#assign arrayObjsEvt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "RHDI")/>
            <#if arrayObjsEvt.isPresent()>
                <#list arrayObjsEvt.get() as itemEvt>
                    <@macros.ProprietaryIdentification propIdntFctnObj=itemEvt.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:IntrmdtSctiesDstrbtnTp>
            <p:CptlGnInOutInd>
            <#assign arrayObjsEvt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "ECIO")/>
            <#if arrayObjsEvt.isPresent()>
                <#list arrayObjsEvt.get() as itemEvt>
                    <@macros.ProprietaryIdentification propIdntFctnObj=itemEvt.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:CptlGnInOutInd>
            <p:TaxblIncmPerShrClctd>
            <#assign arrayObjsEvt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "TDTA")/>
            <#if arrayObjsEvt.isPresent()>
                <#list arrayObjsEvt.get() as itemEvt>
                    <@macros.ProprietaryIdentification propIdntFctnObj=itemEvt.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:TaxblIncmPerShrClctd>
            <p:ElctnTp>
            <#assign arrayObjsEvt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "ELCT")/>
            <#if arrayObjsEvt.isPresent()>
                <#list arrayObjsEvt.get() as itemEvt>
                    <@macros.ProprietaryIdentification propIdntFctnObj=itemEvt.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:ElctnTp>
            <p:LtryTp>
            <#assign arrayObjsEvt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "LOTO")/>
            <#if arrayObjsEvt.isPresent()>
                <#list arrayObjsEvt.get() as itemEvt>
                    <@macros.ProprietaryIdentification propIdntFctnObj=itemEvt.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:LtryTp>
            <p:CertfctnTp>
            <#assign arrayObjsEvt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(swiftMessage.getBlock4().getSeqD().getField22a1(), ["/f/qualifier"], "CEFI")/>
            <#if arrayObjsEvt.isPresent()>
                <#list arrayObjsEvt.get() as itemEvt>
                    <@macros.ProprietaryIdentification propIdntFctnObj=itemEvt.getF() byWhat="indicator"/>
                </#list>
            </#if>
            </p:CertfctnTp>
            <p:CnsntTp>
                <p:Cd>CTRM</p:Cd>
            </p:CnsntTp>
            <p:InfTp>
                <p:Cd>CONF</p:Cd>
            </p:InfTp>
            <p:NewPlcOfIncorprtn>
            <#assign arrayObj=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField94aPlace(), ["/e/qualifier"], "NPLI")/>
            <#if arrayObj.isPresent()>
                ${arrayObj.get().getE().getAddress().getLine35x()?join("")}
            </#if>
            </p:NewPlcOfIncorprtn>
            <p:AddtlInf>
                <p:Offerr>
                    <p:AddtlInf>
                    <#assign arrayObj=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField94aPlace(), ["/e/qualifier"], "OFFO")/>
                    <#if arrayObj.isPresent()>
                        ${arrayObj.get().getE().getNarrative10Lines().getLine35x()?join(",")}
                    </#if>
                    </p:AddtlInf>
                </p:Offerr>
                <p:NewCpnyNm>
                    <p:AddtlInf>
                    <#assign arrayObj=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField94aPlace(), ["/e/qualifier"], "NAME")/>
                    <#if arrayObj.isPresent()>
                        ${arrayObj.get().getE().getNarrative10Lines().getLine35x()?join(",")}
                    </#if>
                    </p:AddtlInf>
                </p:NewCpnyNm>
                <p:URLAdr>
                    <p:URLAdr>
                    <#assign arrayObj=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField94aPlace(), ["/e/qualifier"], "WEBB")/>
                    <#if arrayObj.isPresent()>
                        <#if arrayObj.get().getE()??>
                            ${arrayObj.get().getE().getNarrative10Lines().getLine35x()?join(",")}
                        <#elseif arrayObj.get().getG()??>
                             ${arrayObj.get().getG().getNarrative1035z().getNarrative35z()?join(",")}
                        </#if>
                    </#if>
                    </p:URLAdr>
                </p:URLAdr>
            </p:AddtlInf>
        </p:CorpActnDtls>
        <#assign corpActionOpts=swiftMessage.getBlock4().getSeqE()/>
        <#if corpActionOpts??>
            <#list corpActionOpts as corpActionOpt>
                <p:CorpActnOptnDtls>
                    <p:OptnNb>${corpActionOpt.getField13aNumberIdentification().getA().getNumberIdentificationCode()}</p:OptnNb>
                    <p:OptnTp>
                        <#assign optionTpObj = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField22a1(), ["/f/qualifier"], "CAOP")/>
                        <#if optionTpObj.isPresent()>
                            <@macros.ProprietaryIdentification propIdntFctnObj=optionTpObj.get().getF() byWhat="indicator"/>
                        </#if>
                    </p:OptnTp>
                    <p:FrctnDspstn>
                        <#assign optionTpObj = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField22a1(), ["/f/qualifier"], "DISF")/>
                        <#if optionTpObj.isPresent()>
                            <@macros.ProprietaryIdentification propIdntFctnObj=optionTpObj.get().getF() byWhat="indicator"/>
                        </#if>
                    </p:FrctnDspstn>
                    <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(corpActionOpt.getField22a1(), ["/f/qualifier"], "OFFE")/>
                    <#if arrayObjs.isPresent()>
                        <#list arrayObjs.get() as item>
                        <p:OfferTp>
                            <@macros.ProprietaryIdentification propIdntFctnObj=item.getF() byWhat="indicator"/>
                        </p:OfferTp>
                        </#list>
                    </#if>
                    <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(corpActionOpt.getField22a1(), ["/f/qualifier"], "OPTF")/>
                    <#if arrayObjs.isPresent()>
                        <#list arrayObjs.get() as item>
                        <p:OptnFeatrs>
                            <@macros.ProprietaryIdentification propIdntFctnObj=item.getF() byWhat="indicator"/>
                        </p:OptnFeatrs>
                        </#list>
                    </#if>
                    <p:OptnAvlbtySts>
                        <#assign optionTpObj = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField22a1(), ["/f/qualifier"], "OSTA")/>
                        <#if optionTpObj.isPresent()>
                            <@macros.ProprietaryIdentification propIdntFctnObj=optionTpObj.get().getF() byWhat="indicator"/>
                        </#if>
                    </p:OptnAvlbtySts>
                    <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(corpActionOpt.getField22a1(), ["/f/qualifier"], "CETI")/>
                    <#if arrayObjs.isPresent()>
                        <#list arrayObjs.get() as item>
                        <p:CertfctnBrkdwnTp>
                            <@macros.ProprietaryIdentification propIdntFctnObj=item.getF() byWhat="indicator"/>
                        </p:CertfctnBrkdwnTp>
                        </#list>
                    </#if>
                    <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(corpActionOpt.getField22a1(), ["/c/qualifier"], "NDOM")/>
                    <#if arrayObjs.isPresent()>
                        <#list arrayObjs.get() as item>
                        <p:NonDmclCtry>
                                ${(item.getC().getCountryCode())!}
                        </p:NonDmclCtry>
                        </#list>
                    </#if>
                    <#assign arrayObjs=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(corpActionOpt.getField22a1(), ["/c/qualifier"], "DOMI")/>
                    <#if arrayObjs.isPresent()>
                        <#list arrayObjs.get() as item>
                            <p:VldDmclCtry>
                            ${(item.getC().getCountryCode())!}
                            </p:VldDmclCtry>
                        </#list>
                    </#if>
                    <p:CcyOptn>
                        ${(corpActionOpt.getField11ACurrency().getA().getCurrencyCode())!}
                    </p:CcyOptn>
                    <p:DfltPrcgOrStgInstr>
                        <#assign dfltOptnInd = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField17BFlag(), ["/b/qualifier"], "DFLT")/>
                        <#assign stgInstrInd = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField17BFlag(), ["/b/qualifier"], "STIN")/>
                        <#if dfltOptnInd.isPresent()>
                            <p:DfltOptnInd>${dfltOptnInd.get().b.flag}</p:DfltOptnInd>
                        <#elseif stgInstrInd.isPresent()>
                            <p:StgInstrInd>${stgInstrInd.get().b.flag}</p:StgInstrInd>
                        </#if>
                    </p:DfltPrcgOrStgInstr>
                    <p:ChrgsApldInd>
                        <#assign chrgsApldInd = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField17BFlag(), ["/b/qualifier"], "RCHG")/>
                        <#if chrgsApldInd.isPresent()>
                            ${chrgsApldInd.get().b.flag}
                        </#if>
                    </p:ChrgsApldInd>
                    <p:CertfctnBrkdwnInd>
                        <#assign certfctnBrkdwnInd = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField17BFlag(), ["/b/qualifier"], "CERT")/>
                        <#if certfctnBrkdwnInd.isPresent()>
                            ${certfctnBrkdwnInd.get().b.flag}
                        </#if>
                    </p:CertfctnBrkdwnInd>
                    <p:WdrwlAllwdInd>
                        <#assign wdrwlAllwdInd = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField17BFlag(), ["/b/qualifier"], "WTHD")/>
                        <#if wdrwlAllwdInd.isPresent()>
                            ${wdrwlAllwdInd.get().b.flag}
                        </#if>
                    </p:WdrwlAllwdInd>
                    <p:ChngAllwdInd>
                        <#assign chngAllwdInd = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOpt.getField17BFlag(), ["/b/qualifier"], "CHAN")/>
                        <#if chngAllwdInd.isPresent()>
                            ${chngAllwdInd.get().b.flag}
                        </#if>
                    </p:ChngAllwdInd>
                    <p:ApldOptnInd>true</p:ApldOptnInd>
                    <#if (corpActionOpt.getField35BIdentificationOfSecurity().getB())??>
                        <p:FinInstrmId>
                            <@macros.FinInstrmId field35BIdentificationOfSecurity= corpActionOpt.getField35BIdentificationOfSecurity().getB().getField35BIdOfSecurity()/>
                        </p:FinInstrmId>
                    </#if>
                    <p:DtDtls>
                        <#assign CorpActnoptnDtDtlsEarlyRspnDdln = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "EarlyRspnDdln", "EARD", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/e/qualifier"])/>
                        ${(CorpActnoptnDtDtlsEarlyRspnDdln)!}
                        <#assign CorpActnoptnDtDtlsCoverXprtnDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "CoverXprtnDt", "CVPR", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/e/qualifier"])/>
                        ${(CorpActnoptnDtDtlsCoverXprtnDt)!}
                        <#assign CorpActnoptnDtDtlsPrtctDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "PrtctDt", "PODT", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/e/qualifier"])/>
                        ${(CorpActnoptnDtDtlsPrtctDt)!}
                        <#assign CorpActnoptnDtDtlsMktDdln = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "MktDdln", "MKDT", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/e/qualifier"])/>
                        ${(CorpActnoptnDtDtlsMktDdln)!}
                        <#assign CorpActnoptnDtDtlsRspnDdln = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "RspnDdln", "RDDT", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/e/qualifier", "/f/qualifier"])/>
                        ${(CorpActnoptnDtDtlsRspnDdln)!}
                        <#assign CorpActnoptnDtDtlsXpryDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "XpryDt", "EXPI", ["/a/qualifier", "/b/qualifier", "/c/qualifier"])/>
                        ${(CorpActnoptnDtDtlsXpryDt)!}
                        <#assign CorpActnoptnDtDtlsSbcptCostDbtDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "SbcptCostDbtDt", "SUBS", ["/a/qualifier", "/b/qualifier"])/>
                        ${(CorpActnoptnDtDtlsSbcptCostDbtDt)!}
                        <#assign CorpActnoptnDtDtlsDpstryCoverXprtnDt = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField98a2(), "DpstryCoverXprtnDt", "DVCP", ["/a/qualifier", "/b/qualifier", "/c/qualifier"])/>
                        ${(CorpActnoptnDtDtlsDpstryCoverXprtnDt)!}
                        <p:StockLndgDdln>
                            <p:Dt>
                                <p:Dt>2001-01-01</p:Dt>
                            </p:Dt>
                        </p:StockLndgDdln>
                        <p:BrrwrStockLndgDdln>
                            <p:StockLndgDdln>
                                <p:Dt>
                                    <p:Dt>2001-01-01</p:Dt>
                                </p:Dt>
                            </p:StockLndgDdln>
                            <p:Brrwr>
                                <p:AnyBIC>p:AnyBIC</p:AnyBIC>
                            </p:Brrwr>
                        </p:BrrwrStockLndgDdln>
                    </p:DtDtls>
                    <p:PrdDtls>
                        <#assign CorpActnOptnDtDtlsPrdDtlsPricClctnPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField69aPeriod(), "PricClctnPrd", "PRIC", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
                        ${(CorpActnOptnDtDtlsPrdDtlsPricClctnPrd)!}
                        <#assign CorpActnOptnDtDtlsPrdDtlsParllTradgPrd = statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActionOpt.getField69aPeriod(), "ParllTradgPrd", "PARL", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
                        ${(CorpActnOptnDtDtlsPrdDtlsParllTradgPrd)!}
                        <#assign CorpActnOptnDtDtlsPrdDtlsActnPrd = statics['app.etl.iso15022.util.Utility'].buildPeriod3Choice(corpActionOpt.getField69aPeriod(), "ActnPrd", "PWAL", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
                        ${(CorpActnOptnDtDtlsPrdDtlsActnPrd)!}
                        <#assign CorpActnOptnDtDtlsPrdDtlsRvcbltyPrd = statics['app.etl.iso15022.util.Utility'].buildPeriod3Choice(corpActionOpt.getField69aPeriod(), "RvcbltyPrd", "REVO", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
                        ${(CorpActnOptnDtDtlsPrdDtlsRvcbltyPrd)!}
                        <#assign CorpActnOptnDtDtlsPrdDtlsPrvlgSspnsnPrd = statics['app.etl.iso15022.util.Utility'].buildPeriod3Choice(corpActionOpt.getField69aPeriod(), "PrvlgSspnsnPrd", "SUSP", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
                        ${(CorpActnOptnDtDtlsPrdDtlsPrvlgSspnsnPrd)!}
                        <#assign CorpActnOptnDtDtlsPrdDtlsAcctSvcrRvcbltyPrd = statics['app.etl.iso15022.util.Utility'].buildPeriod3Choice(corpActionOpt.getField69aPeriod(), "AcctSvcrRvcbltyPrd", "AREV", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
                        ${(CorpActnOptnDtDtlsPrdDtlsAcctSvcrRvcbltyPrd)!}
                        <#assign CorpActnOptnDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwl = statics['app.etl.iso15022.util.Utility'].buildPeriod3Choice(corpActionOpt.getField69aPeriod(), "DpstrySspnsnPrdForWdrwl", "DSWO", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
                        ${(CorpActnOptnDtDtlsPrdDtlsDpstrySspnsnPrdForWdrwl)!}
                    </p:PrdDtls>
                    <@rateAndAmtDtls.CorpActnOptnRateAndAmtDtls corpActionOpt.getField92aRate()/>
                    <@priceDtls.CorpActnOptnDtlsPricDtls corpActionOpt/>
                    <@sctiesQty.CorpActnOptnSctiesQty corpActionOpt/>
                    <#if corpActionOpt.getSeqE1()??>
                        <#list corpActionOpt.getSeqE1() as corpActnOptnSeqE1>
                            <@sctiesMvmnt.SctiesMvmntDtls corpActnOptnSeqE1/>
                        </#list>
                    </#if>
                    <#if corpActionOpt.getSeqE2()??>
                        <#list corpActionOpt.getSeqE2() as corpActnOptnSeqE2>
                            <@cashMvmnt.CashMvmntDtls corpActnOptnSeqE2/>
                        </#list>
                    </#if>
                    <p:AddtlInf>
                        <@macros.createNarrativeFor corpActionOpt "ADTX" "AddtlTxt"/>
                        <@macros.createNarrativeFor corpActionOpt "TXNR" "NrrtvVrsn"/>
                        <@macros.createNarrativeForMulti corpActionOpt "INCO" "InfConds"/>
                        <@macros.createNarrativeForMulti corpActionOpt "COMP" "InfToCmplyWth"/>
                        <@macros.createNarrativeForMulti corpActionOpt "NSER" "SctyRstrctn"/>
                        <@macros.createNarrativeForMulti corpActionOpt "TAXE" "TaxtnConds"/>
                        <@macros.createNarrativeForMulti corpActionOpt "DISC" "Dsclmr"/>
                        <p:CertfctnBrkdwn>
                            <p:UpdDesc>p:UpdDesc</p:UpdDesc>
                            <p:UpdDt>2001-01-01</p:UpdDt>
                            <p:AddtlInf>p:AddtlInf</p:AddtlInf>
                        </p:CertfctnBrkdwn>
                    </p:AddtlInf>
                </p:CorpActnOptnDtls>
            </#list>
        </#if>
        <p:AddtlInf>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "ADTX" "AddtlTxt"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "TXNR" "NrrtvVrsn"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "INCO" "InfConds"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "COMP" "InfToCmplyWth"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "NSER" "SctyRstrctn"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "TAXE" "TaxtnConds"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "DISC" "Dsclmr"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "PACO" "PtyCtctNrrtv"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "REGI" "RegnDtls"/>
            <@macros.createNarrativeForMulti swiftMessage.getBlock4().getSeqF() "BAIN" "BsktOrIndxInf"/>
            <p:CertfctnBrkdwn>
                <p:UpdDesc>p:UpdDesc</p:UpdDesc>
                <p:UpdDt>2001-01-01</p:UpdDt>
                <p:AddtlInf>p:AddtlInf</p:AddtlInf>
            </p:CertfctnBrkdwn>
        </p:AddtlInf>
        <@macros.PartyIdentification71ChoiceMulti qualifier="ISAG" tag="IssrAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceMulti qualifier="PAYA" tag="PngAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceMulti qualifier="CODO" tag="SubPngAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceSingle qualifier="REGR" tag="Regar" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceMulti qualifier="RESA" tag="RsellngAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceSingle qualifier="PSAG" tag="PhysSctiesAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceSingle qualifier="DROP" tag="DrpAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceMulti qualifier="SOLA" tag="SlctnAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <@macros.PartyIdentification71ChoiceSingle qualifier="INFA" tag="InfAgt" field95aPartyArr=swiftMessage.getBlock4().getSeqF().getField95aParty()/>
        <p:SplmtryData>
            <p:PlcAndNm>p:PlcAndNm</p:PlcAndNm>
            <p:Envlp>
                <ANY-ELEMENT/>
            </p:Envlp>
        </p:SplmtryData>
    </p:CorpActnNtfctn>
</p:Document>
