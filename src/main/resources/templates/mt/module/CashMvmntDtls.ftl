<#import "lib/Macros.ftl" as macros/>
<#import "SctyDtls.ftl" as sctyDtls/>
<#import "SctiesQty.ftl" as sctyQty/>
<#import "PriceDtls.ftl" as priceDtls/>
<#import "RateAndAmtDtls.ftl" as rateAndAmnts/>

<#macro CashMvmntDtls corpActnOptnSeqE2>
<p:CshMvmntDtls>
    <p:CdtDbtInd>
        <#assign field22aType99=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField22a1(), ["/h/qualifier"], "CRDB")/>
        <#if field22aType99.isPresent()>
            ${field22aType99.get().getH().getIndicator()}
        </#if>
    </p:CdtDbtInd>
    <p:NonElgblPrcdsInd>
        <#assign field22aType99=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField22a1(), ["/f/qualifier"], "NELP")/>
        <#if field22aType99.isPresent()>
            <@macros.ProprietaryIdentificationWithOptional field22aType99, "f" "indicator"/>
        </#if>
    </p:NonElgblPrcdsInd>
    <p:IssrOfferrTaxbltyInd>
        <p:Cd>TXBL</p:Cd>
    </p:IssrOfferrTaxbltyInd>
    <p:IncmTp>
        <#assign field22aType99=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField22a1(), ["/f/qualifier"], "ITYP")/>
        <#if field22aType99.isPresent()>
            <@macros.GenericIdentification36 field22aType99.get() "indicator"/>
        </#if>
    </p:IncmTp>
    <p:XmptnTp>
        <p:Id>p:Id</p:Id>
        <p:Issr>p:Issr</p:Issr>
        <p:SchmeNm>p:SchmeNm</p:SchmeNm>
    </p:XmptnTp>
    <p:CtryOfIncmSrc>p:CtryOfIncmSrc</p:CtryOfIncmSrc>
    <p:CshAcctId>
        <#assign field97aAcctOpt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField97aAccount(), ["/a/qualifier", "/e/qualifier"], "CASH")/>
        <#if field97aAcctOpt.isPresent()>
            <#if field97aAcctOpt.get().getA()>
                <@macros.ProprietaryIdentification propIdntFctnObj=field97aAcctOpt.get().getA() byWhat="accountNumber"/>
            <#elseif field97aAcctOpt.get().getE()>
                <p:IBAN>${field97aAcctOpt.get().getE().getInternationalBankAccountNumber()}</p:IBAN>
            </#if>
        </#if>
    </p:CshAcctId>
    <@rateAndAmnts.CorpAtnOptnCashMvmntAmtDtls corpActnOptnSeqE2/>
    <p:DtDtls>
        <#assign CorpActnCashMvmntPmtDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE2.getField98a2(), "PmtDt", "PAYD", ["/b/qualifier", "/c/qualifier", "/a/qualifier"])/>
        ${(CorpActnCashMvmntPmtDt)!}
        <#assign CorpActnCashMvmntValDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE2.getField98a2(), "ValDt", "VALU", ["/b/qualifier", "/c/qualifier", "/a/qualifier"])/>
        ${(CorpActnCashMvmntValDt)!}
        <#assign CorpActnCashMvmntFXRateFxgDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE2.getField98a2(), "FXRateFxgDt", "FXDT", ["/b/qualifier", "/c/qualifier", "/a/qualifier"])/>
        ${(CorpActnCashMvmntFXRateFxgDt)!}
        <#assign CorpActnCashMvmntFXRateEarlstPmtDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE2.getField98a2(), "EarlstPmtDt", "EARL", ["/b/qualifier", "/c/qualifier", "/a/qualifier", "/e/qualifier"])/>
        ${(CorpActnCashMvmntFXRateEarlstPmtDt)!}
    </p:DtDtls>
    <p:FXDtls>
        <#assign RsltgAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField19aAmount(), ["/b/qualifier"], "STEX")/>
        <#if RsltgAmt.isPresent()>
            <p:RsltgAmt Ccy="${RsltgAmt.get().getCurrencyAmount().getCurrency()}">${RsltgAmt.get().getCurrencyAmount().getAmount()}</p:RsltgAmt>
        </#if>
        <#assign XchgRate=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField92aRate(), ["/b/qualifier"], "EXCH")/>
        <#if XchgRate.isPresent()>
            <p:XchgRate>${XchgRate.get().getB().getRate()}</p:XchgRate>
            <p:UnitCcy>${(XchgRate.get().getB().getCurrency())!}</p:UnitCcy>
            <p:QtdCcy>${(XchgRate.get().getB().getCurrency1())!}</p:QtdCcy>
        </#if>
        <p:XchgRate>0.0</p:XchgRate>
    </p:FXDtls>
    <#if corpActnOptnSeqE2.getField92aRate()??>
        <p:RateAndAmtDtls>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="AddtlTax" qualifier="ATAX" possibleXPaths=["/a/qualifier", "/f/qualifier", "/k/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArrWithLoop field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="GrssDvddRate" qualifier="GRSS" possibleXPaths=["/f/qualifier", "/k/qualifier", "/j/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArrWithLoop field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="IntrstRateUsdForPmt" qualifier="INTP" possibleXPaths=["/a/qualifier", "/f/qualifier", "/k/qualifier", "/j/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="WhldgTaxRate" qualifier="TAXR" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
            <p:ScndLvlTax>
                <p:Rate>0.0</p:Rate>
            </p:ScndLvlTax>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="EarlySlctnFeeRate" qualifier="ESOF" possibleXPaths=["/a/qualifier", "/m/qualifier", "/k/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="FsclStmp" qualifier="FISC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
            <p:ThrdPtyIncntivRate>
                <p:Rate>0.0</p:Rate>
            </p:ThrdPtyIncntivRate>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArrWithLoop field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="NetDvddRate" qualifier="NETT" possibleXPaths=["/j/qualifier", "/f/qualifier", "/k/qualifier", "/j/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="NonResdtRate" qualifier="FISC" possibleXPaths=["/a/qualifier", "/f/qualifier", "/k/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="AplblRate" qualifier="RATE" possibleXPaths=["/a/qualifier", "/f/qualifier", "/k/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="SlctnFeeRate" qualifier="SOFE" possibleXPaths=["/a/qualifier", "/m/qualifier", "/k/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArrWithLoop field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="TaxCdtRate" qualifier="TAXC" possibleXPaths=["/a/qualifier", "/f/qualifier", "/k/qualifier", "/j/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="TaxOnIncm" qualifier="TXIN" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="TaxOnPrfts" qualifier="TXPR" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
            <@rateAndAmnts.RateAndAmountFormat37ChoiceArr field92aRateArray=corpActnOptnSeqE2.getField92aRate() tag="TaxRclmRate" qualifier="TXRC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
            <p:EqulstnRate>
                <p:Amt Ccy="">0.0</p:Amt>
            </p:EqulstnRate>
        </p:RateAndAmtDtls>
    </#if>

    <p:PricDtls>
        <p:GncCshPricPdPerPdct>
            <#assign GncCshPricPdPerPdct=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField90aPrice(), ["/a/qualifier", "/b/qualifier", "/e/qualifier", "/k/qualifier"], "PRPP")/>
            <#if GncCshPricPdPerPdct.isPresent()>
                <@priceDtls.PriceFormat44Choice GncCshPricPdPerPdct.get()/>
            </#if>
        </p:GncCshPricPdPerPdct>
        <p:GncCshPricRcvdPerPdct>
            <#assign GncCshPricRcvdPerPdct=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE2.getField90aPrice(), ["/a/qualifier", "/b/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier", "/k/qualifier"], "PRPP")/>
            <#if GncCshPricRcvdPerPdct.isPresent()>
                <@priceDtls.PriceFormat47Choice GncCshPricRcvdPerPdct.get()/>
            </#if>
        </p:GncCshPricRcvdPerPdct>
    </p:PricDtls>
</p:CshMvmntDtls>
</#macro>