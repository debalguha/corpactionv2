<#import "lib/Macros.ftl" as macros/>
<#assign field36aQuantityOfFinancialInstrumentPossibleXPaths = ["/c/qualifier", "/b/qualifier"]>
<#macro FinancialInstrumentQuantity1Choice field36aType29B byWhat="quantity">
    <#if field36aType29B.quantityTypeCode == "UNIT">
    <p:Unit>${field36aType29B[byWhat]}</p:Unit>
    <#elseif field36aType29B.quantityTypeCode == "FAMT">
    <p:FaceAmt>${field36aType29B[byWhat]}</p:FaceAmt>
    <#elseif field36aType29B.quantityTypeCode == "AMOR">
    <p:AmtsdVal>${field36aType29B[byWhat]}</p:AmtsdVal>
    </#if>
</#macro>
<#macro SignedQuantityFormat6 field93abalanceOptionX>
    <#if (field93abalanceOptionX.signedbalance.sign)??>
        <p:ShrtLngPos>SHOR</p:ShrtLngPos>
    <#else>
        <p:ShrtLngPos>LONG</p:ShrtLngPos>
    </#if>
    <@SignedQuantityFormat6Qty field93abalanceOptionX/>
</#macro>
<#macro SignedQuantityFormat6Qty field93abalanceOptionX>
    <p:Qty>
        <#if field93abalanceOptionX.quantityTypeCode == "UNIT">
            <p:Unit>${field93abalanceOptionX.signedBalance.balance}</p:Unit>
        <#elseif field93abalanceOptionX.quantityTypeCode == "FAMT">
            <p:FaceAmt>${field93abalanceOptionX.signedBalance.balance}</p:FaceAmt>
        <#elseif field93abalanceOptionX.quantityTypeCode == "AMOR">
            <p:AmtsdVal>${field93abalanceOptionX.signedBalance.balance}</p:AmtsdVal>
        </#if>
    </p:Qty>
</#macro>
<#macro SignedQuantityFormat7 field93abalanceOptionB>
    <#if (field93abalanceOptionX.signedbalance.sign)??>
        <p:ShrtLngPos>SHOR</p:ShrtLngPos>
    <#else>
        <p:ShrtLngPos>LONG</p:ShrtLngPos>
    </#if>
    <#if field93aBalanceOptionB.dataSourceScheme??>
        <p:PrtryQty>
            <p:Qty>${field93aBalanceOptionB.signedBalance.balance}</p:Qty>
            <p:QtyTp>${field93aBalanceOptionB.quantityTypeCode}</p:QtyTp>
            <@macros.IssurSchemeNm field93aBalanceOptionB/>
        </p:PrtryQty>
    <#else>
        <@SignedQuantityFormat6Qty field93abalanceOptionB/>
    </#if>
</#macro>
<#macro Quantity6Choice field36aType29Array qualifier tag>
    <#assign field36aType29 = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(field36aType29Array,  ["/b/qualifier"], qualifier)>
    <#if field36aType29.isPresent()>
        <p:${tag}>
            <#if field36aType29.get()?size == 1>
                <p:Qty>
                    <@FinancialInstrumentQuantity1Choice field36aType29.get()[0].b/>
                </p:Qty>
            <#elseif field36aType29.get()?size == 2>
                <p:OrgnlAndCurFace>
                    <p:FaceAmt>${statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(field36aType29.get(), ["/b/quantityTypeCode"], "FAMT").get().getB().getQuantity()}</p:FaceAmt>
                    <p:AmtsdVal>${statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(field36aType29.get(), ["/b/quantityTypeCode"], "AMOR").get().getB().getQuantity()}</p:AmtsdVal>
                </p:OrgnlAndCurFace>
            </#if>
        </p:${tag}>
    </#if>
</#macro>
<#macro Field36aType29 field36aType29Array qualifier tag>
    <#assign field36aType29 = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(field36aType29Array,  field36aQuantityOfFinancialInstrumentPossibleXPaths, qualifier)>
    <#if field36aType29.isPresent()>
        <#if field36aType29.get().b??>
        <p:${tag}>
            <@FinancialInstrumentQuantity1Choice field36aType29.get().b/>
        </p:${tag}>
        <#elseif field36aType29.get().getC()??>
        <p:${tag}>
            <p:Cd>${field36aType29.get().getC().getQuantityCode()}</p:Cd>
        </p:${tag}>
        </#if>
    </#if>
</#macro>
<#macro CorpActnDtlsSctiesQty>
    <p:SctiesQty>
        <@Field36aType29 swiftMessage.getBlock4().getSeqD().getField36aQuantityOfFinancialInstrument() "MQSO" "MaxQty"/>
        <@Field36aType29 swiftMessage.getBlock4().getSeqD().getField36aQuantityOfFinancialInstrument() "QTSO" "MinQtySght"/>
        <@Field36aType29 swiftMessage.getBlock4().getSeqD().getField36aQuantityOfFinancialInstrument() "NBLT" "NewBrdLotQty"/>
        <@Field36aType29 swiftMessage.getBlock4().getSeqD().getField36aQuantityOfFinancialInstrument() "NEWD" "NewDnmtnQty"/>
        <@Field36aType29 swiftMessage.getBlock4().getSeqD().getField36aQuantityOfFinancialInstrument() "BASE" "BaseDnmtn"/>
        <@Field36aType29 swiftMessage.getBlock4().getSeqD().getField36aQuantityOfFinancialInstrument() "INCR" "IncrmtlDnmtn"/>
    </p:SctiesQty>
</#macro>
<#macro CorpActnOptnSctiesQty corpActnOptn>
    <p:SctiesQty>
        <@Field36aType29 corpActnOptn.getField36aQuantityOfFinancialInstrument() "MAEX" "MaxQtyToInst"/>
        <@Field36aType29 corpActnOptn.getField36aQuantityOfFinancialInstrument() "MIEX" "MinQtyToInst"/>
        <@Field36aType29 corpActnOptn.getField36aQuantityOfFinancialInstrument() "MILT" "MinMltplQtyToInst"/>
        <@Field36aType29 corpActnOptn.getField36aQuantityOfFinancialInstrument() "NBLT" "NewBrdLotQty"/>
        <@Field36aType29 corpActnOptn.getField36aQuantityOfFinancialInstrument() "NEWD" "NewDnmtnQty"/>
        <@Field36aType29 corpActnOptn.getField36aQuantityOfFinancialInstrument() "FOLQ" "FrntEndOddLotQty"/>
        <@Field36aType29 corpActnOptn.getField36aQuantityOfFinancialInstrument() "BOLQ" "BckEndOddLotQty"/>
    </p:SctiesQty>
</#macro>