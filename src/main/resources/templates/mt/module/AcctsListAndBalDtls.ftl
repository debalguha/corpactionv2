<#import "lib/Macros.ftl" as macros>
<#import "SctiesQty.ftl" as sctiesQty>
<#macro SafeKeepingPlaceChoiceBCF field49aPlace>
    <#if (field49aPlace.c)??>
        <p:Ctry>${field49aPlace.c.countryCode}</p:Ctry>
    <#elseif (field49aPlace.f)??>
        <p:TpAndId>
            <p:SfkpgPlcTp>${field49aPlace.f.placeCode}</p:SfkpgPlcTp>
            <p:Id>${field49aPlace.f.bic.bankCode}${field49aPlace.f.bic.countryCode}${field49aPlace.f.bic.locationCode}${field49aPlace.f.bic.branchCode}</p:Id>
        </p:TpAndId>
    <#elseif (field49aPlace.b)??>
        <@macros.ProprietaryIdentification field49aPlace.b "placeCode" "Id"/>
    </#if>
</#macro>
<#macro buildGenericBalanceFragmentMulti tag qualifier field93aBalance>
    <#assign lookedUpBals=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(field93aBalance, ["/b/qualifier", "/C/qualifier"], "BLOK")/>
    <#if lookedUpBals.isPresent()>
        <#list lookedUpBals.get() as lookedUpBal>
            <@buildGenericBalanceFragment lookedUpBal tag qualifier field93aBalance/>
        </#list>
    </#if>
</#macro>
<#macro buildGenericBalanceFragmentSingle tag qualifier field93aBalance>
    <#assign lookedUpBal=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(field93aBalance, ["/b/qualifier", "/C/qualifier"], "BLOK")/>
    <#if lookedUpBal.isPresent()>
        <@buildGenericBalanceFragment lookedUpBal tag qualifier field93aBalance/>
    </#if>
</#macro>
<#macro buildGenericBalanceFragment lookedUpBal tag qualifier field93aBalance>
    <p:${tag}>
        <#if lookedUpBal.get().b??>
            <p:Bal><@sctiesQty.SignedQuantityFormat7 lookedUpBal.get().b/></p:Bal>
        <#elseif lookedUpBal.get().c??>
            <#if lookedUpBal.get().c.balanceTypeCode == "ELIG">
                <p:ElgblBal><@sctiesQty.SignedQuantityFormat6 lookedUpBal.get().c/></p:ElgblBal>
            <#else>
                <p:NotElgblBal><@sctiesQty.SignedQuantityFormat6 lookedUpBal.get().c/></p:NotElgblBal>
            </#if>
        </#if>
    </p:${tag}>
</#macro>
<#macro AcctsListAndBalDtls mt564SequenceB2AccountInformations>
    <#list mt564SequenceB2AccountInformations as mt564SequenceB2AccountInformation>
        <p:AcctsListAndBalDtls>
            <p:SfkpgAcct>${(mt564SequenceB2AccountInformation.field97aAccount.a.accountNumber)!}</p:SfkpgAcct>
            <!--Optional:-->
            <p:AcctOwnr>
                <#if mt564SequenceB2AccountInformation.field95aParty??>
                    <@macros.PartyIdentification51Choice mt564SequenceB2AccountInformation.field95aParty/>
                </#if>

            </p:AcctOwnr>
            <!--Optional:-->
            <p:SfkpgPlc>
                <#if mt564SequenceB2AccountInformation.field94aPlace??>
                    <@SafeKeepingPlaceChoiceBCF mt564SequenceB2AccountInformation.field94aPlace/>
                </#if>
            </p:SfkpgPlc>
            <#if mt564SequenceB2AccountInformation.getField93aBalance()??>
                <p:Bal>
                    <!--Optional:-->
                    <p:TtlElgblBal>
                        <#assign TtlElgblBal=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(mt564SequenceB2AccountInformation.getField93aBalance(), ["/b/qualifier"], "ELIG")/>
                        <p:Bal>
                            <p:QtyChc>
                                <!--You have a CHOICE of the next 2 items at this level-->
                                <#if TtlElgblBal.isPresent() && TtlElgblBal.get().size() > 1>
                                    <#assign famt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(TtlElgblBal.get(), ["/b/quantityTypeCode"], "FAMT")/>
                                    <p:OrgnlAndCurFaceAmt>
                                        <#if famt.isPresent() && (famet.get().b.signedBalance.sign)??>
                                            <p:ShrtLngPos>SHOR</p:ShrtLngPos>
                                        <#else>
                                            <p:ShrtLngPos>LONG</p:ShrtLngPos>
                                        </#if>
                                        <p:FaceAmt>${famt.get().b.signedBalance.balance}</p:FaceAmt>
                                        <p:AmtsdVal>${(statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(totalElgblBal.get(), ["/b/quantityTypeCode"], "AMOR").get().b.signedBalance.balance)!}</p:AmtsdVal>
                                    </p:OrgnlAndCurFaceAmt>
                                <#elseif TtlElgblBal.isPresent() && TtlElgblBal.get().size() == 1>
                                    <@sctiesQty.BalanceFormat5Choice TtlElgblBal.get()[0].b/>
                                </#if>
                            </p:QtyChc>
                        </p:Bal>
                        <!--Optional:-->

                        <!--Optional:-->
                    <#--<p:FullPrdUnits>
                        <p:ShrtLngPos>SHOR</p:ShrtLngPos>
                        <p:Qty>
                            <!--You have a CHOICE of the next 3 items at this level&ndash;&gt;
                            <p:Unit>1000.00</p:Unit>
                            <p:FaceAmt>1000.00</p:FaceAmt>
                            <p:AmtsdVal>1000.00</p:AmtsdVal>
                        </p:Qty>
                    </p:FullPrdUnits>-->
                        <!--Optional:-->
                    <#--<p:PartWayPrdUnits>
                        <p:ShrtLngPos>SHOR</p:ShrtLngPos>
                        <p:Qty>
                            <!--You have a CHOICE of the next 3 items at this level&ndash;&gt;
                            <p:Unit>1000.00</p:Unit>
                            <p:FaceAmt>1000.00</p:FaceAmt>
                            <p:AmtsdVal>1000.00</p:AmtsdVal>
                        </p:Qty>
                    </p:PartWayPrdUnits>-->
                    </p:TtlElgblBal>
                    <!--Optional:-->
                    <@buildGenericBalanceFragmentSingle "BlckdBal" "BLOK" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "BrrwdBal" "BORR" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "CollInBal" "COLI" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "CollOutBal" "COLO" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "OnLnBal" "LOAN" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentMulti "PdgDlvryBal" "PEND" mt564SequenceB2AccountInformation.getField93aBalance()/> <!-- FullPrdUnits and PartWayPrdUnits transformation not available-->
                    <@buildGenericBalanceFragmentMulti "PdgRctBal" "PENR" mt564SequenceB2AccountInformation.getField93aBalance()/> <!-- FullPrdUnits and PartWayPrdUnits transformation not available-->
                    <@buildGenericBalanceFragmentSingle "OutForRegnBal" "REGO" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "SttlmPosBal" "SETT" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "StrtPosBal" "SPOS" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "TradDtPosBal" "TRAD" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "InTrnsShipmntBal" "TRAN" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "OblgtdBal" "OBAL" mt564SequenceB2AccountInformation.getField93aBalance()/>
                    <@buildGenericBalanceFragmentSingle "UafctdBal" "UNAF" mt564SequenceB2AccountInformation.getField93aBalance()/>
                </p:Bal>
            </#if>
        </p:AcctsListAndBalDtls>
    </#list>
</#macro>