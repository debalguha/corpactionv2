<#import "lib/Macros.ftl" as macros/>
<#macro InterestRateUsedForPaymentFormat8Choice field92aRateArray qualifier tag possibleXPaths=[]>
    <#assign field92aRateSeq = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(field92aRateArray, possibleXPaths, qualifier)>
    <#if field92aRateSeq.isPresent()>
        <#list field92aRateSeq.get() as field29aRate>
            <@InterestRateUsedForPaymentFormat8ChoiceVal field29aRate tag/>
        </#list>
    </#if>
</#macro>
<#macro InterestRateUsedForPaymentFormat8ChoiceVal field29aRate tag>
    <#if field29aRate.getJ()??>
        <@RateTpAndAmtAndRateSts field29aRate tag/>
    <#else>
        <@RateAndAmountFormat37Choice field29aRate tag/>
    </#if>
</#macro>
<#macro RateTpAndAmtAndRateSts field29aRate tag>
    <p:RateTpAndAmtAndRateSts>
        <p:RateTp>
            <#if (field29aRate.getJ().getDataSourceScheme())??>
                <@macros.GenericIdentification36 field29aRate.getJ() "numberIdentificationNumber"/>
            <#else>
                <p:Cd>${field29aRate.getJ().getRateTypeCode()}</p:Cd>
            </#if>
        </p:RateTp>
        <p:Amt>
        ${(field29aRate.getJ().getAmount())!}
        </p:Amt>
        <p:RateSts>${field29aRate.getJ().getRateStatusCode()}</p:RateSts>
    </p:RateTpAndAmtAndRateSts>
</#macro>
<#macro RateAndAmountFormat37ChoiceArr field92aRateArray qualifier possibleXPaths tag>
    <#assign field92aType15 = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(field92aRateArray, possibleXPaths, qualifier)>
    <#if field92aType15.isPresent()>
        <@RateAndAmountFormat37Choice field92aType15.get() tag/>
    </#if>
</#macro>
<#macro RateAndAmountFormat37ChoiceArrWithLoop field92aRateArray qualifier possibleXPaths tag>
    <#assign field92aType15Elmnts = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(field92aRateArray, possibleXPaths, qualifier)>
    <#if field92aType15Elmnts.isPresent()>
        <#list field92aType15Elmnts.get() as field92aType15>
            <@RateAndAmountFormat37Choice field92aType15 tag/>
        </#list>
    </#if>
</#macro>
<#macro PercentageRate field92aRate>
    <#if field92aType15.isPresent()>
        <#if field92aType15.getA().getSignedRate().getSign()??>
            <p:Rate>-${field92aType15.getA().getSignedRate().getRate()}</p:Rate>
        <#else >
            <p:Rate>${field92aType15.getA().getSignedRate().getRate()}</p:Rate>
        </#if>
    </#if>
</#macro>
<#macro RateAndAmountFormat37Choice field92aType15 tag>
    <p:${tag}>
        <#if field92aType15.f??>
            <Amt Ccy="${field92aType15.getF().getCurrency()}">${field92aType15.get().getF().getAmount()}</Amt>
        <#elseif field92aType15.k??>
            <NotSpcfdRate>${field92aType15.getK().getRateTypeCode()}</NotSpcfdRate>
        <#elseif field92aType15.a??>
            <#if field92aType15.getA().getSignedRate().getSign()??>
                <p:Rate>-${field92aType15.getA().getSignedRate().getRate()}</p:Rate>
            <#else >
                <p:Rate>${field92aType15.getA().getSignedRate().getRate()}</p:Rate>
            </#if>
        <#elseif field92aType15.j??>
            <p:RateTp>
                <@macros.ProprietaryIdentification field92aType15.getJ() "rateTypeCode"/>
            </p:RateTp>
            <p:Amt><Amt Ccy="${field92aType15.getJ().getCurrency()}">${field92aType15.get().getJ().getAmount()}</Amt></p:Amt>
            <p:RateSts>
                <@macros.ProprietaryIdentification field92aType15.getJ() "rateStatusCode"/>
            </p:RateSts>
        <#elseif field92aType15.m??>
            <p:AmtToQty>
                <p:Amt Ccy="${field92aType15.getM().getCurrency()}">${field92aType15.getM().getAmount()}</p:Amt>
                <p:Qty>${field92aType15.getM().getQuantity()}</p:Qty>
            </p:AmtToQty>
        </#if>
    </p:${tag}>
</#macro>
<#macro RateFormat12Choice field92aRateArray qualifier possibleXPaths tag>
    <#assign field92aType15 = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(field92aRateArray, possibleXPaths, qualifier)>
    <#if field92aType15.isPresent()>
    <p:${tag}>
        <#if field92aType15.get().getA()??>
            <#if field92aType15.get().getA().getSignedRate().getSign()??>
                <p:Rate>-${field92aType15.get().getA().getSignedRate().getRate()}</p:Rate>
            <#else >
                <p:Rate>${field92aType15.get().getA().getSignedRate().getRate()}</p:Rate>
            </#if>
        <#elseif field92aType15.get().getK()??>
            <NotSpcfdRate>${field92aType15.get().getK().getRateTypeCode()}</NotSpcfdRate>
        </#if>
    </p:${tag}>
    </#if>
</#macro>
<#macro RatioFormat17Choice field92aType18>
    <#if field92aType18.isPresent()>
        <#if (field92aType18.get().getD())??>
        <p:QtyToQty>
            <p:Qty1>${field92aType18.get().getD().getQuantity1()}</p:Qty1>
            <p:Qty2>${field92aType18.get().getD().getQuantity2()}</p:Qty2>
        </p:QtyToQty>
        </#if>
        <#if (field92aType18.get().getK())??>
        <p:NotSpcfdRate>${field92aType18.get().getK().getRateTypeCode()}</p:NotSpcfdRate>
        </#if>
        <#if (field92aType18.get().getL())??>
        <p:AmtToAmt>
            <p:Amt1 Ccy="${field92aType18.get().getL().getCurrency1()}">${field92aType18.get().getL().getAmount()}</p:Amt1>
            <p:Amt2 Ccy="${field92aType18.get().getL().getCurrency1()}">${field92aType18.get().getL().getAmount1()}</p:Amt2>
        </p:AmtToAmt>
        </#if>
    </#if>
</#macro>
<#macro RatioFormat18Choice field92aType18>
    <@RatioFormat17Choice field92aType18=field92aType18/>
    <#if field92aType18.isPresent()>
        <#if (field92aType18.get().getM())??>
        <p:AmtToQty>
            <p:Amt Ccy="${field92aType18.get().getM().getCurrency()}">${field92aType18.get().getM().getAmount()}</p:Amt>
            <p:Qty>${field92aType18.get().getM().getQuantity()}</p:Qty>
        </p:AmtToQty>
        </#if>
        <#if (field92aType18.get().getN())??>
        <p:QtyToAmt>
            <p:Amt Ccy="${field92aType18.get().getN().getCurrency()}">${field92aType18.get().getN().getAmount()}</p:Amt>
            <p:Qty>${field92aType18.get().getN().getQuantity()}</p:Qty>
        </p:QtyToAmt>
        </#if>
    </#if>
</#macro>
<#assign corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths = ["/a/qualifier", "/f/qualifier", "/k/qualifier"]>
<#macro CorpActnDtlsRateAndAmtDtlsRateAndAmtDtls rateVar>
    <p:RateAndAmtDtls>
        <@RateAndAmountFormat37ChoiceArr swiftMessage.getBlock4().getSeqD().getField92aRate() "INTR" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "Intrst"/>
        <@RateFormat12Choice swiftMessage.getBlock4().getSeqD().getField92aRate() "PTSC" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "PctgSght"/>
        <@RateFormat12Choice swiftMessage.getBlock4().getSeqD().getField92aRate() "RINR" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "RltdIndx"/>
        <@RateFormat12Choice swiftMessage.getBlock4().getSeqD().getField92aRate() "RSPR" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "Sprd"/>
        <@RateAndAmountFormat37ChoiceArr swiftMessage.getBlock4().getSeqD().getField92aRate() "BIDI" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "BidIntrvl"/>
        <@RateFormat12Choice swiftMessage.getBlock4().getSeqD().getField92aRate() "PRFC" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "PrvsFctr"/>
        <@RateFormat12Choice swiftMessage.getBlock4().getSeqD().getField92aRate() "NWFC" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "NxtFctr"/>
        <p:RinvstmtDscntRateToMkt>
            <p:Rate>0.0</p:Rate>
        </p:RinvstmtDscntRateToMkt>
        <p:IntrstShrtfll>
            <p:Rate>0.0</p:Rate>
        </p:IntrstShrtfll>
        <p:RealsdLoss>
            <p:Rate>0.0</p:Rate>
        </p:RealsdLoss>
        <p:DclrdRate>
            <p:Rate>0.0</p:Rate>
        </p:DclrdRate>
    </p:RateAndAmtDtls>
</#macro>
<#assign corpActnOptnRateAndAmtDtlsPossibleXpaths = ["/a/qualifier", "/f/qualifier", "/k/qualifier"]>
<#assign corpActnOptnRateAndAmtDtlsPossibleXpathsForPaymentFormat8Choice = ["/a/qualifier", "/f/qualifier", "/k/qualifier", "/j/qualifier"]>
<#macro CorpActnOptnRateAndAmtDtls rateVar>
    <p:RateAndAmtDtls>
        <@RateFormat12Choice rateVar "ATAX" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "AddtlTax"/>
        <@RateAndAmountFormat37ChoiceArr rateVar "GRSS" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "GrssDvddRate"/>
        <p:NetDvddRate>
            <p:Amt Ccy="">0.0</p:Amt>
        </p:NetDvddRate>
        <@RateFormat12Choice rateVar "INDX" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "IndxFctr"/>
        <@InterestRateUsedForPaymentFormat8Choice rateVar "INTP"  "IntrstRateUsdForPmt" corpActnOptnRateAndAmtDtlsPossibleXpathsForPaymentFormat8Choice/>
        <@RateFormat12Choice rateVar "OVEP" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "MaxAllwdOvrsbcptRate"/>
        <@RateFormat12Choice rateVar "PROR" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "PrratnRate"/>
        <@RateFormat12Choice rateVar "TAXR" corpActnDtlsRateAndAmtDtlsRateAndAmtDtlsPossibleXpaths "WhldgTaxRate"/>
        <p:ScndLvlTax>
            <p:Rate>0.0</p:Rate>
        </p:ScndLvlTax>
        <@InterestRateUsedForPaymentFormat8Choice field92aRateArray=rateVar qualifier="INTP" tag="TaxblIncmPerDvddShr" possibleXPaths=["/j/qualifier"]/>
        <p:IssrDclrdXchgRate>
            <p:UnitCcy>p:UnitCcy</p:UnitCcy>
            <p:QtdCcy>p:QtdCcy</p:QtdCcy>
            <p:XchgRate>0.0</p:XchgRate>
        </p:IssrDclrdXchgRate>
        <p:TaxOnIncm>
            <p:Rate>0.0</p:Rate>
        </p:TaxOnIncm>
    </p:RateAndAmtDtls>
</#macro>

<#macro CorpAtnOptnCashMvmntAmtDtls corpActionOptnSeq2>
    <p:AmtDtls>
        <#assign GrssCshAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "GRSS")/>
        <#if GrssCshAmt.isPresent()>
            <p:GrssCshAmt Ccy="${GrssCshAmt.get().getCurrencyAmount().getCurrency()}">${GrssCshAmt.get().getCurrencyAmount().getAmount()}</p:GrssCshAmt>
        </#if>
        <#assign NetCshAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "NETT")/>
        <#if NetCshAmt.isPresent()>
            <p:NetCshAmt Ccy="${NetCshAmt.get().getCurrencyAmount().getCurrency()}">${NetCshAmt.get().getCurrencyAmount().getAmount()}</p:NetCshAmt>
        </#if>
        <#assign SlctnFees=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "SOFE")/>
        <#if SlctnFees.isPresent()>
            <p:SlctnFees Ccy="${SlctnFees.get().getCurrencyAmount().getCurrency()}">${SlctnFees.get().getCurrencyAmount().getAmount()}</p:SlctnFees>
        </#if>
        <#assign CshInLieuOfShr=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "CINL")/>
        <#if CshInLieuOfShr.isPresent()>
            <p:CshInLieuOfShr Ccy="${CshInLieuOfShr.get().getCurrencyAmount().getCurrency()}">${CshInLieuOfShr.get().getCurrencyAmount().getAmount()}</p:CshInLieuOfShr>
        </#if>
        <#assign CptlGn=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "CAPG")/>
        <#if CptlGn.isPresent()>
            <p:CptlGn Ccy="${CptlGn.get().getCurrencyAmount().getCurrency()}">${CptlGn.get().getCurrencyAmount().getAmount()}</p:CptlGn>
        </#if>
        <#assign IntrstAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "INTR")/>
        <#if IntrstAmt.isPresent()>
            <p:IntrstAmt Ccy="${IntrstAmt.get().getCurrencyAmount().getCurrency()}">${IntrstAmt.get().getCurrencyAmount().getAmount()}</p:IntrstAmt>
        </#if>
        <#assign IndmntyAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "INDM")/>
        <#if IndmntyAmt.isPresent()>
            <p:IndmntyAmt Ccy="${IndmntyAmt.get().getCurrencyAmount().getCurrency()}">${IndmntyAmt.get().getCurrencyAmount().getAmount()}</p:IndmntyAmt>
        </#if>
        <#assign ManfctrdDvddPmtAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "MFDV")/>
        <#if ManfctrdDvddPmtAmt.isPresent()>
            <p:ManfctrdDvddPmtAmt Ccy="${ManfctrdDvddPmtAmt.get().getCurrencyAmount().getCurrency()}">${ManfctrdDvddPmtAmt.get().getCurrencyAmount().getAmount()}</p:ManfctrdDvddPmtAmt>
        </#if>
        <#assign RinvstmtAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "REIN")/>
        <#if RinvstmtAmt.isPresent()>
            <p:RinvstmtAmt Ccy="${RinvstmtAmt.get().getCurrencyAmount().getCurrency()}">${RinvstmtAmt.get().getCurrencyAmount().getAmount()}</p:RinvstmtAmt>
        </#if>
        <#assign FullyFrnkdAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "FLFR")/>
        <#if FullyFrnkdAmt.isPresent()>
            <p:FullyFrnkdAmt Ccy="${FullyFrnkdAmt.get().getCurrencyAmount().getCurrency()}">${FullyFrnkdAmt.get().getCurrencyAmount().getAmount()}</p:FullyFrnkdAmt>
        </#if>
        <#assign UfrnkdAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "UNFR")/>
        <#if UfrnkdAmt.isPresent()>
            <p:UfrnkdAmt Ccy="${UfrnkdAmt.get().getCurrencyAmount().getCurrency()}">${UfrnkdAmt.get().getCurrencyAmount().getAmount()}</p:UfrnkdAmt>
        </#if>
        <#assign SndryOrOthrAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "SOIC")/>
        <#if SndryOrOthrAmt.isPresent()>
            <p:SndryOrOthrAmt Ccy="${SndryOrOthrAmt.get().getCurrencyAmount().getCurrency()}">${SndryOrOthrAmt.get().getCurrencyAmount().getAmount()}</p:SndryOrOthrAmt>
        </#if>
        <#assign TaxFreeAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "TAXFR")/>
        <#if TaxFreeAmt.isPresent()>
            <p:TaxFreeAmt Ccy="${TaxFreeAmt.get().getCurrencyAmount().getCurrency()}">${TaxFreeAmt.get().getCurrencyAmount().getAmount()}</p:TaxFreeAmt>
        </#if>
        <#assign TaxDfrrdAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "TXDF")/>
        <#if TaxDfrrdAmt.isPresent()>
            <p:TaxDfrrdAmt Ccy="${TaxDfrrdAmt.get().getCurrencyAmount().getCurrency()}">${TaxDfrrdAmt.get().getCurrencyAmount().getAmount()}</p:TaxDfrrdAmt>
        </#if>
        <#assign ValAddedTaxAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "VATA")/>
        <#if ValAddedTaxAmt.isPresent()>
            <p:ValAddedTaxAmt Ccy="${ValAddedTaxAmt.get().getCurrencyAmount().getCurrency()}">${ValAddedTaxAmt.get().getCurrencyAmount().getAmount()}</p:ValAddedTaxAmt>
        </#if>
        <#assign StmpDtyAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "STAM")/>
        <#if StmpDtyAmt.isPresent()>
            <p:StmpDtyAmt Ccy="${StmpDtyAmt.get().getCurrencyAmount().getCurrency()}">${StmpDtyAmt.get().getCurrencyAmount().getAmount()}</p:StmpDtyAmt>
        </#if>
        <#assign TaxRclmAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "TXRC")/>
        <#if TaxRclmAmt.isPresent()>
            <p:TaxRclmAmt Ccy="${TaxRclmAmt.get().getCurrencyAmount().getCurrency()}">${TaxRclmAmt.get().getCurrencyAmount().getAmount()}</p:TaxRclmAmt>
        </#if>
        <#assign TaxCdtAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "TAXC")/>
        <#if TaxCdtAmt.isPresent()>
            <p:TaxCdtAmt Ccy="${TaxCdtAmt.get().getCurrencyAmount().getCurrency()}">${TaxCdtAmt.get().getCurrencyAmount().getAmount()}</p:TaxCdtAmt>
        </#if>
        <#assign AddtlTaxAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "ATAX")/>
        <#if AddtlTaxAmt.isPresent()>
            <p:AddtlTaxAmt Ccy="${AddtlTaxAmt.get().getCurrencyAmount().getCurrency()}">${AddtlTaxAmt.get().getCurrencyAmount().getAmount()}</p:AddtlTaxAmt>
        </#if>
        <#assign WhldgTaxAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "TAXR")/>
        <#if WhldgTaxAmt.isPresent()>
            <p:WhldgTaxAmt Ccy="${WhldgTaxAmt.get().getCurrencyAmount().getCurrency()}">${WhldgTaxAmt.get().getCurrencyAmount().getAmount()}</p:WhldgTaxAmt>
        </#if>
        <#assign WhldgTaxAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "WITF")/>
        <#if WhldgTaxAmt.isPresent()>
            <p:WhldgTaxAmt Ccy="${WhldgTaxAmt.get().getCurrencyAmount().getCurrency()}">${WhldgTaxAmt.get().getCurrencyAmount().getAmount()}</p:WhldgTaxAmt>
        </#if>
        <p:ScndLvlTaxAmt Ccy="">0.0</p:ScndLvlTaxAmt>
        <#assign FsclStmpAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "FISC")/>
        <#if FsclStmpAmt.isPresent()>
            <p:FsclStmpAmt Ccy="${FsclStmpAmt.get().getCurrencyAmount().getCurrency()}">${FsclStmpAmt.get().getCurrencyAmount().getAmount()}</p:FsclStmpAmt>
        </#if>
        <#assign ExctgBrkrAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "EXEC")/>
        <#if ExctgBrkrAmt.isPresent()>
            <p:ExctgBrkrAmt Ccy="${ExctgBrkrAmt.get().getCurrencyAmount().getCurrency()}">${ExctgBrkrAmt.get().getCurrencyAmount().getAmount()}</p:ExctgBrkrAmt>
        </#if>
        <#assign PngAgtComssnAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "PAMM")/>
        <#if PngAgtComssnAmt.isPresent()>
            <p:PngAgtComssnAmt Ccy="${PngAgtComssnAmt.get().getCurrencyAmount().getCurrency()}">${PngAgtComssnAmt.get().getCurrencyAmount().getAmount()}</p:PngAgtComssnAmt>
        </#if>
        <#assign LclBrkrComssnAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "LOCO")/>
        <#if LclBrkrComssnAmt.isPresent()>
            <p:LclBrkrComssnAmt Ccy="${LclBrkrComssnAmt.get().getCurrencyAmount().getCurrency()}">${LclBrkrComssnAmt.get().getCurrencyAmount().getAmount()}</p:LclBrkrComssnAmt>
        </#if>
        <#assign RgltryFeesAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "REGF")/>
        <#if RgltryFeesAmt.isPresent()>
            <p:RgltryFeesAmt Ccy="${RgltryFeesAmt.get().getCurrencyAmount().getCurrency()}">${RgltryFeesAmt.get().getCurrencyAmount().getAmount()}</p:RgltryFeesAmt>
        </#if>
        <#assign ShppgFeesAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "SHIP")/>
        <#if ShppgFeesAmt.isPresent()>
            <p:ShppgFeesAmt Ccy="${ShppgFeesAmt.get().getCurrencyAmount().getCurrency()}">${ShppgFeesAmt.get().getCurrencyAmount().getAmount()}</p:ShppgFeesAmt>
        </#if>
        <#assign ChrgsAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "CHAR")/>
        <#if ChrgsAmt.isPresent()>
            <p:ChrgsAmt Ccy="${ChrgsAmt.get().getCurrencyAmount().getCurrency()}">${ChrgsAmt.get().getCurrencyAmount().getAmount()}</p:ChrgsAmt>
        </#if>
        <#assign EntitldAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "ENTL")/>
        <#if EntitldAmt.isPresent()>
            <p:EntitldAmt Ccy="${EntitldAmt.get().getCurrencyAmount().getCurrency()}">${EntitldAmt.get().getCurrencyAmount().getAmount()}</p:EntitldAmt>
        </#if>
        <#assign OrgnlAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "OCMT")/>
        <#if OrgnlAmt.isPresent()>
            <p:OrgnlAmt Ccy="${OrgnlAmt.get().getCurrencyAmount().getCurrency()}">${OrgnlAmt.get().getCurrencyAmount().getAmount()}</p:OrgnlAmt>
        </#if>
        <#assign PrncplOrCrps=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "PRIN")/>
        <#if PrncplOrCrps.isPresent()>
            <p:PrncplOrCrps Ccy="${PrncplOrCrps.get().getCurrencyAmount().getCurrency()}">${PrncplOrCrps.get().getCurrencyAmount().getAmount()}</p:PrncplOrCrps>
        </#if>
        <#assign RedPrmAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "REDP")/>
        <#if RedPrmAmt.isPresent()>
            <p:RedPrmAmt Ccy="${RedPrmAmt.get().getCurrencyAmount().getCurrency()}">${RedPrmAmt.get().getCurrencyAmount().getAmount()}</p:RedPrmAmt>
        </#if>
        <#assign IncmPrtn=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "INCO")/>
        <#if IncmPrtn.isPresent()>
            <p:IncmPrtn Ccy="${IncmPrtn.get().getCurrencyAmount().getCurrency()}">${RedPrmAmt.get().getCurrencyAmount().getAmount()}</p:IncmPrtn>
        </#if>
        <#assign StockXchgTax=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "STEX")/>
        <#if StockXchgTax.isPresent()>
            <p:StockXchgTax Ccy="${StockXchgTax.get().getCurrencyAmount().getCurrency()}">${StockXchgTax.get().getCurrencyAmount().getAmount()}</p:StockXchgTax>
        </#if>
        <#assign EUTaxRtntnAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "EUTR")/>
        <#if EUTaxRtntnAmt.isPresent()>
            <p:EUTaxRtntnAmt Ccy="${EUTaxRtntnAmt.get().getCurrencyAmount().getCurrency()}">${EUTaxRtntnAmt.get().getCurrencyAmount().getAmount()}</p:EUTaxRtntnAmt>
        </#if>
        <#assign AcrdIntrstAmt=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActionOptnSeq2.getField19aAmount(), ["/b/qualifier"], "ACRU")/>
        <#if AcrdIntrstAmt.isPresent()>
            <p:AcrdIntrstAmt Ccy="${AcrdIntrstAmt.get().getCurrencyAmount().getCurrency()}">${AcrdIntrstAmt.get().getCurrencyAmount().getAmount()}</p:AcrdIntrstAmt>
        </#if>
        <p:EqulstnAmt Ccy="">0.0</p:EqulstnAmt>
        <p:FATCATaxAmt Ccy="">0.0</p:FATCATaxAmt>
        <p:NRATaxAmt Ccy="">0.0</p:NRATaxAmt>
        <p:BckUpWhldgTaxAmt Ccy="">0.0</p:BckUpWhldgTaxAmt>
        <p:TaxOnIncmAmt Ccy="">0.0</p:TaxOnIncmAmt>
        <p:TxTax Ccy="">0.0</p:TxTax>
    </p:AmtDtls>
</#macro>