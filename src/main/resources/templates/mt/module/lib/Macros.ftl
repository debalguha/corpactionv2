<#macro dumpProperties node>
    <#if node.properties??>
    <table>
        <#list node.properties?keys as key>
            <#assign value = node.properties[key]>
            <tr>
                <td>${key}</td>
                <td>
                    <#if value?is_date>
                    ${value?datetime}
                    <#elseif value?is_sequence>
                        [
                        <#list value as valueValue>
                        ${valueValue},
                        </#list>
                        ]
                    <#else>
                    ${value}
                    </#if>
                </td>
            </tr>
        </#list>
    </table>
    <#else>
    Node doesn't have properties.
    </#if>
</#macro>
<#macro PartyIdentification71ChoiceMulti field95aPartyArr qualifier tag>
    <#assign field95aPartyElms = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(field95aPartyArr, ["/p/qualifier", "/q/qualifier", "/r/qualifier"], qualifier)/>
    <#if field95aPartyElms.isPresent()>
        <#list field95aPartyElms.get() as field95aParty>
            <p:${tag}><@PartyIdentification71Choice field95aParty.get9()/></p:${tag}>
        </#list>
    </#if>
</#macro>
<#macro PartyIdentification71ChoiceSingle field95aPartyArr qualifier tag>
    <#assign field95aParty = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(field95aPartyArr, ["/p/qualifier", "/q/qualifier", "/r/qualifier"], qualifier)/>
    <#if field95aParty.isPresent()>
        <p:${tag}><@PartyIdentification71Choice field95aParty.get9()/></p:${tag}>
    </#if>
</#macro>
<#macro PartyIdentification51Choice field95aParty>
    <#if (field95aParty.p)??>
        <p:AnyBIC>${field95aParty.p.bic.bankCode}${field95aParty.p.bic.countryCode}${field95aParty.p.bic.locationCode}${field95aParty.p.bic.branchCode}/p:AnyBIC>
    <#elseif (field95aParty.r)??>
        <@ProprietaryIdentification field95aParty.r "partyProprietaryCode" />
    </#if>
</#macro>
<#macro PartyIdentification71Choice field95aParty>
    <#if field95aParty.q??>
        <p:Nm>${field95aParty.q.nameAndAddress.name}</p:Nm>
        <p:Adr>
            <p:AdrTp></p:AdrTp>
            <p:AdrLine>${field95aParty.q.nameAndAddress.getLine35x()?join("")}</p:AdrLine>
            <p:Ctry></p:Ctry> <!-- No idea how to populate. Non mandatory. -->
        </p:Adr>
    <#else>
        <@PartyIdentification51Choice field95aParty/>
    </#if>
</#macro>
<#macro GenericIdentification36 genericIdentificationObj idProp>
    <p:Id>${genericIdentificationObj[idProp]}}</p:Id>
    <p:Issr>${genericIdentificationObj.getDataSourceScheme().substring(0, 4)}</p:Issr>
    <#if genericIdentificationObj.getDataSourceScheme().substring(4)??>
        <p:SchmeNm>${genericIdentificationObj.getDataSourceScheme().substring(4)}</p:SchmeNm>
    </#if>
</#macro>
<#macro IssurSchemeNm genericIdentificationObj>
    <p:Issr>${genericIdentificationObj.dataSourceScheme[0..4]}</p:Issr>
    <#if genericIdentificationObj.dataSourceScheme?length > 5>
        <p:SchmeNm>${genericIdentificationObj.dataSourceScheme[4..]}</p:SchmeNm>
    </#if>
</#macro>
<#macro IdentificationFormat3Choice field13aType12 qualifier>
    <#if (field13aType12.getA().getQualifier)?? && field13aType12.getA().getQualifier.equals(qualifier)>
        <p:ShrtId>${field13aType12.getA().getNumberIdentificationCode()}</p:ShrtId>
    <#elseif (field13aType12.getB().getQualifier())?? && field13aType12.getB().getQualifier().equals(qualifier)>
        <#if field13aType12.getB().getDataSourceScheme()??>
            <@GenericIdentification36 field13aType12.getB() "numberIdentificationNumber"/>
        <#else>
            <p:LngId>${field13aType12.getB().getNumberIdentificationNumber()}</p:LngId>
        </#if>
    </#if>
</#macro>
<#macro ProprietaryIdentificationWithOptional propIdntFctnObj nestedProp byWhat codeTag="Cd">
    <#if propIdntFctnObj.isPresent()>
        <@ProprietaryIdentification propIdntFctnObj.get()[nestedProp] byWhat codeTag/>
    </#if>
</#macro>
<#macro ProprietaryIdentification propIdntFctnObj byWhat codeTag="Cd">
    <#if propIdntFctnObj.dataSourceScheme??>
        <p:Prtry>
            <@GenericIdentification36 propIdntFctnObj byWhat/>
        </p:Prtry>
    <#else>
        <p:${codeTag}>${propIdntFctnObj[byWhat]}</p:${codeTag}>
    </#if>
</#macro>
<#macro FinInstrClssfctnTp classFctnObj possibleXPaths qualifier>
    <#--<#assign classificationObj =statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqB().getSeqB1().getField12aTypeOfFinancialInstrument(), ["/c/qualifier", "/a/qualifier"], "CLAS")/>-->
    <#assign classificationObj =statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(classFctnObj.getField12aTypeOfFinancialInstrument(), possibleXPaths, qualifier)/>
    <#if classificationObj.isPresent()>
        <#if classificationObj.get().a??>
            <p:AltrnClssfctn>
                <@GenericIdentification36 classificationObj.get().a "instrumentCodeOrDescription"/>
            </p:AltrnClssfctn>
        <#else>
            <p:ClssfctnFinInstrm>${(classificationObj.get().getC().getCFICode())!}</p:ClssfctnFinInstrm>
        </#if>
    </#if>
</#macro>
<#macro UndrlygSctyOptnStyle classFctnObj possibleXPaths qualifier>
<#--<#assign classificationObj =statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqB().getSeqB1().getField12aTypeOfFinancialInstrument(), ["/c/qualifier", "/a/qualifier"], "CLAS")/>-->
    <#assign scrtyOption =statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(classFctnObj.getField12aTypeOfFinancialInstrument(), possibleXPaths, qualifier)/>
    <#if scrtyOption.isPresent() && scrtyOption.get().b??>
        <@ProprietaryIdentification propIdntFctnObj=scrtyOption.get().b byWhat="instrumentTypeCode"/>
    </#if>
</#macro>
<#function filterArrayWithQualifier objArray qualifier possibleXpaths=[""]>
    <#return statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(objArray, possibleXpaths, qualifier)/>
</#function>
<#function filterArrayWithQualifierForElements objArray qualifier possibleXpaths=[""]>
    <#return statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(objArray, possibleXpaths, qualifier)/>
</#function>
<#function getValueByXpath valueObj xpath>
    <#return statics['app.etl.iso15022.util.Utility'].getValueByXpath(valueObj, xpath)/>
</#function>
<#function getValueAfterFilteringArray objArray qualifierXpath qualifier valueXpath>
    <#assign valueOptn=filterArrayWithQualifier(objArray, qualifier, [qualifierXpath])/>
    <#if valueOptn.isPresent()>
        <#assign xpathValue=getValueByXpath(valueOptn.get(), valueXpath)/>
        <#return xpathValue/>
    </#if>
</#function>
<#function unwrapOptional optionalObj>
    <#if optionalObj.isPresent()>
        <#return optionalObj.get()/>
    </#if>
    <#return ""/>
</#function>
<#function unwrapOptionToIsoDate optionObj>
    <#if optionObj.isPresent()>
        <#return optionObj.get()?date?iso_utc/>
    </#if>
    <#return ""/>
</#function>
<#macro getValueAfterFilteringArrayIntoTag objArray qualifierXpath qualifier valueXpath tag>
    <p:${tag}>${getValueAfterFilteringArray(objArray, qualifierXpath, qualifier, valueXpath)}</p:${tag}>
</#macro>
<#macro FinInstrmId field35BIdentificationOfSecurity>
    <#if (field35BIdentificationOfSecurity.getB().getField35BIdOfSecurity().getIdentificationOfSecurity())??>
        <p:ISIN>${field35BIdentificationOfSecurity.getB().getField35BIdOfSecurity().getIdentificationOfSecurity().getISINNumber()}</p:ISIN>
            <#assign secIdAndCodeSeq= statics['app.etl.iso15022.util.Utility'].buildOtherIdentificationFromField35BIdentificationOfSecurity(field35BIdentificationOfSecurity.getB())/>
        <p:OthrId>
            <p:Id>${(secIdAndCodeSeq[0])!}</p:Id>
            <p:Sfx>p:Sfx</p:Sfx>
            <p:Tp>
                <p:Cd>${(secIdAndCodeSeq[1])!}</p:Cd>
            </p:Tp>
        </p:OthrId>
        <p:Desc>${field35BIdentificationOfSecurity.getB().getField35BIdOfSecurity().getIdentificationOfSecurity().getDescriptionOfSecurity4Lines().getLine35x()?join("")}</p:Desc>
    <#else>
        <p:ISIN>${field35BIdentificationOfSecurity.getB().getField35BIdOfSecurity().getISINNumber()}</p:ISIN>
    </#if>
</#macro>
<#macro PriceFormat45Choice field90aPrice>
    <#if field90aPrice??>
        <#if field90aPrice.getA()??>
        <p:PctgPric>
            <p:PctgPricTp>${field90aPrice.getA().getPercentageTypeCodes()}</p:PctgPricTp>
            <p:PricVal>${field90aPrice.getA().getPrice()}</p:PricVal>
        </p:PctgPric>
        <#elseif field90aPrice.getB()??>
        <p:AmtPric>
            <p:AmtPricTp>${field90aPrice.getB().getAmountTypeCode()}</p:AmtPricTp>
            <p:PricVal Ccy="${field90aPrice.getB().getCurrency()}">${field90aMaxPrice.get().getB().getAmount()}</p:PricVal>
        </p:AmtPric>
        <#elseif field90aPrice.getE()??>
        <p:NotSpcfdPric>${field90aPrice.getE().getPriceCode()}</p:NotSpcfdPric>
        </#if>
    </#if>
</#macro>
<#macro createNarrativeFor narrativeOptn qualifier tag>
    <#assign field70aType27=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(narrativeOptn.getField70aNarrative(), ["/e/qualifier"], qualifier)/>
    <#if field70aType27.isPresent()>
        <p:${tag}>
            <p:AddtlInf>${field70aType27.get().e.narrative10Lines.getLine35x()?join("")}</p:AddtlInf>
        </p:${tag}>
    </#if>
</#macro>
<#macro createNarrativeForMulti narrativeOptn qualifier tag>
    <#assign field70aType27List=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(narrativeOptn.getField70aNarrative(), ["/e/qualifier"], qualifier)/>
    <#if field70aType27List.isPresent()>
        <#list field70aType27List.get() as field70aType27>
            <p:${tag}>
                <p:AddtlInf>${field70aType27.e.narrative10Lines.getLine35x()?join("")}</p:AddtlInf>
            </p:${tag}>
        </#list>
    </#if>
</#macro>
<#function extractDateFromField98A2 swiftMessage qualifier>
    <#if swiftMessage.block4.seqA.field98a2.a??>
        <#if swiftMessage.block4.seqA.field98a2.a.qualifier == qualifier>
            <#return swiftMessage.block4.seqA.field98a2.a.dateYYYYMMDD />
        </#if>
    </#if>
</#function>