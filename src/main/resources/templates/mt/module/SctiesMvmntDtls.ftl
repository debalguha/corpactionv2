<#import "lib/Macros.ftl" as macros/>
<#import "SctyDtls.ftl" as sctyDtls/>
<#import "SctiesQty.ftl" as sctyQty/>
<#import "PriceDtls.ftl" as priceDtls/>
<#import "RateAndAmtDtls.ftl" as rateAndAmnts/>
<#macro SctiesMvmntDtls corpActnOptnSeqE1>
<p:SctiesMvmntDtls>
    <@sctyDtls.SctyDtls corpActnOptnSeqE1/>
    <#assign CdtDbtInd=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField22a1(),  ["/h/qualifier"], "CRDB")/>
    <#if CdtDbtInd.isPresent()>
        <p:CdtDbtInd>${CdtDbtInd.get().getH().getIndicator()}</p:CdtDbtInd>
    </#if>
    <#assign TempFinInstrmInd=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField22a1(),  ["/f/qualifier"], "TEMP")/>
    <#if TempFinInstrmInd.isPresent()>
        <p:TempFinInstrmInd>
            <#if TempFinInstrmInd.get().getF().getDataSourceScheme()??>
                <p:Prtry>
                    <@macros.GenericIdentification36 TempFinInstrmInd.get().getF() "indicator"/>
                </p:Prtry>
            <#else>
                <p:TempInd>${TempFinInstrmInd.get().getF().getIndicator()}</p:TempInd>
            </#if>
        </p:TempFinInstrmInd>
    </#if>
    <#assign NonElgblPrcdsInd=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField22a1(),  ["/f/qualifier"], "NELP")/>
    <#if NonElgblPrcdsInd.isPresent()>
        <p:NonElgblPrcdsInd>
            <#if NonElgblPrcdsInd.get().getF().getDataSourceScheme()??>
                <p:Prtry>
                    <@macros.GenericIdentification36 NonElgblPrcdsInd.get().getF() "indicator"/>
                </p:Prtry>
            <#else>
                <p:Cd>${NonElgblPrcdsInd.get().getF().getIndicator()}</p:Cd>
            </#if>
        </p:NonElgblPrcdsInd>
    </#if>
    <p:IssrOfferrTaxbltyInd>
        <p:Cd>TXBL</p:Cd>
    </p:IssrOfferrTaxbltyInd>
    <p:NewSctiesIssncInd>DEFE</p:NewSctiesIssncInd>
    <p:IncmTp>
        <p:Id>p:Id</p:Id>
        <p:Issr>p:Issr</p:Issr>
        <p:SchmeNm>p:SchmeNm</p:SchmeNm>
    </p:IncmTp>
    <p:XmptnTp>
        <p:Id>p:Id</p:Id>
        <p:Issr>p:Issr</p:Issr>
        <p:SchmeNm>p:SchmeNm</p:SchmeNm>
    </p:XmptnTp>
    <@sctyQty.Quantity6Choice corpActnOptnSeqE1.getField36aQuantityOfFinancialInstrument() "ENTL" "EntitldQty"/>
    <p:SfkpgPlc>
        <p:Id>
            <p:SfkpgPlcTp>SHHE</p:SfkpgPlcTp>
        </p:Id>
    </p:SfkpgPlc>
    <p:CtryOfIncmSrc>p:CtryOfIncmSrc</p:CtryOfIncmSrc>
    <p:FrctnDspstn>
        <#if (corpActnOptnSeqE1.getField22a2().getF())??>
            <@macros.ProprietaryIdentification propIdntFctnObj=corpActnOptnSeqE1.getField22a2().getF() byWhat="indicator"/>
        </#if>
    </p:FrctnDspstn>
    <p:CcyOptn>${(corpActnOptnSeqE1.getField11ACurrency().getA().getCurrencyCode())!}</p:CcyOptn>
    <#assign CorpActnSctiesMvmntTradgPrd = statics['app.etl.iso15022.util.Utility'].buildPeriod3ChoiceFrom(corpActnOptnSeqE1.getField69aPeriod(), "TradgPrd", ["/a/qualifier", "/b/qualifier", "/c/qualifier", "/d/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"])/>
    ${(CorpActnSctiesMvmntTradgPrd)!}
    <p:DtDtls>
        <#assign CorpActnSctiesMvmntPmtDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE1.getField98a2(), "PmtDt", "PAYD", ["/b/qualifier", "/c/qualifier", "/a/qualifier", "/e/qualifier"])/>
        ${(CorpActnSctiesMvmntPmtDt)!}
        <#assign CorpActnSctiesMvmntAvlblDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE1.getField98a2(), "AvlblDt", "AVAL", ["/b/qualifier", "/c/qualifier", "/a/qualifier", "/e/qualifier"])/>
        ${(CorpActnSctiesMvmntAvlblDt)!}
        <#assign CorpActnSctiesMvmntDvddRnkgDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE1.getField98a2(), "DvddRnkgDt", "DIVR", ["/b/qualifier", "/c/qualifier", "/a/qualifier", "/e/qualifier"])/>
        ${(CorpActnSctiesMvmntDvddRnkgDt)!}
        <#assign CorpActnSctiesMvmntEarlstPmtDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE1.getField98a2(), "EarlstPmtDt", "EARL", ["/b/qualifier", "/c/qualifier", "/a/qualifier", "/e/qualifier"])/>
        ${(CorpActnSctiesMvmntEarlstPmtDt)!}
        <#assign CorpActnSctiesMvmntPrpssDt= statics['app.etl.iso15022.util.Utility'].getDateFormat22ChoiceFrom(corpActnOptnSeqE1.getField98a2(), "PrpssDt", "PPDT", ["/b/qualifier", "/c/qualifier", "/a/qualifier", "/e/qualifier"])/>
        ${(CorpActnSctiesMvmntPrpssDt)!}
        <p:LastTradgDt>
            <p:Dt>
                <p:Dt>2001-01-01</p:Dt>
            </p:Dt>
        </p:LastTradgDt>
    </p:DtDtls>
    <p:RateDtls>
        <p:AddtlQtyForSbcbdRsltntScties>
            <@rateAndAmnts.RatioFormat17Choice field92aType18=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField92aRate(), ["/d/qualifier", "/k/qualifier", "/l/qualifier"], "ADSR")/>
        </p:AddtlQtyForSbcbdRsltntScties>
        <p:AddtlQtyForExstgScties>
            <@rateAndAmnts.RatioFormat17Choice field92aType18=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField92aRate(), ["/d/qualifier", "/k/qualifier", "/l/qualifier"], "ADEX")/>
        </p:AddtlQtyForExstgScties>
        <p:NewToOd>
            <@rateAndAmnts.RatioFormat18Choice field92aType18=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField92aRate(), ["/d/qualifier", "/k/qualifier", "/l/qualifier", "/m/qualifier", "/n/qualifier"], "NEWO")/>
        </p:NewToOd>
        <p:TrfrmatnRate>
            <@rateAndAmnts.PercentageRate field92aRate=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField92aRate(), ["/a/qualifier"], "TRAT")/>
        </p:TrfrmatnRate>
        <p:ChrgsFees>
            <p:Rate>0.0</p:Rate>
        </p:ChrgsFees>
        <p:FsclStmp>
            <p:Rate>0.0</p:Rate>
        </p:FsclStmp>
        <p:AplblRate>
            <p:Rate>0.0</p:Rate>
        </p:AplblRate>
        <p:TaxCdtRate>
            <p:Rate>0.0</p:Rate>
        </p:TaxCdtRate>
        <p:FinTxTaxRate>
            <p:Rate>0.0</p:Rate>
        </p:FinTxTaxRate>
    </p:RateDtls>
    <p:PricDtls>
        <@priceDtls.CorpActnOptnDtlsSctiesMvmntPricDtls corpActnOptnSeqE1/>
    </p:PricDtls>
</p:SctiesMvmntDtls>
</#macro>