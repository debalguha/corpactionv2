<#import "lib/Macros.ftl" as macros/>
<#import "SctiesQty.ftl" as sctyQty/>
<#assign field90aPossibleXpaths = ["/a/qualifier", "/b/qualifier", "/e/qualifier"]>
<#macro PriceFormat44Choice field90aPrice>
    <@macros.PriceFormat45Choice field90aPrice=field90aPrice/>
    <#if field90aPrice??>
        <#if field90aPrice.k??>
        <p:IndxPts>${field90aPrice.k.indexPoints}</p:IndxPts>
        </#if>
    </#if>
</#macro>
<#macro PriceFormat47Choice field90aPrice>
    <@macros.PriceFormat45Choice field90aPrice=field90aPrice/>
    <@PriceFormat44Choice field90aPrice=field90aPrice/>
    <#if field90aPrice.f??>
        <p:AmtPricPerFinInstrmQty>
            <p:AmtPricTp>${field90aPrice.f.amountTypeCode}</p:AmtPricTp>
            <p:PricVal Ccy="${field90aPrice.f.currency}">${field90aPrice.f.amount}</p:PricVal>
            <p:FinInstrmQty>
                <@sctyQty.FinancialInstrumentQuantity1Choice field90aPrice.f/>
            </p:FinInstrmQty>
        </p:AmtPricPerFinInstrmQty>
    <#elseif field90aPrice.j??>
        <p:AmtPricPerAmt>
            <p:AmtPricTp>${field90aPrice.j.amountTypeCode}</p:AmtPricTp>
            <p:PricVal Ccy="${field90aPrice.j.currency}">${field90aPrice.j.amount}</p:PricVal>
            <p:Amt Ccy="${field90aPrice.j.currency1}">${field90aPrice.j.amount1}</p:Amt>
        </p:AmtPricPerAmt>
    </#if>
</#macro>
<#macro CorpActnDtlsPricDtls>
    <#assign field90aMaxPrice = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField90aPrice(), field90aPossibleXpaths, "MAXP")>
    <#assign field90aMinPrice = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(swiftMessage.getBlock4().getSeqD().getField90aPrice(), field90aPossibleXpaths, "MINP")>
	<p:PricDtls>
		<#if field90aMaxPrice.isPresent()>
			<p:MaxPric>
				<@PriceFormat45Choice field90aMaxPrice.get()/>
			</p:MaxPric>
		</#if>
		<#if field90aMaxPrice.isPresent()>
			<p:MinPric>
				<<@PriceFormat45Choice field90aMaxPrice.get()/>
			</p:MinPric>
		</#if>
	</p:PricDtls>
</#macro>
<#macro CorpActnOptnDtlsPricDtls corpActionOpt>
    <p:PricDtls>
        <#assign CorpActnOptnDtDtlsPricDtlsCshInLieuOfShrPric = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(corpActionOpt.getField90aPrice(), ["/a/qualifier", "/b/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"], "OFFR")/>
        <#if CorpActnOptnDtDtlsPricDtlsCshInLieuOfShrPric.isPresent()>
            <#list CorpActnOptnDtDtlsPricDtlsCshInLieuOfShrPric.get() as field90aPrice>
                <p:CshInLieuOfShrPric>
                    <@macros.PriceFormat45Choice field90aPrice/>
                </p:CshInLieuOfShrPric>
            </#list>
        </#if>
        <#assign CorpActnOptnDtDtlsPricDtlsOverSbcptDpstPric = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifierForElements(corpActionOpt.getField90aPrice(), ["/a/qualifier", "/b/qualifier", "/e/qualifier", "/f/qualifier", "/j/qualifier"], "OSUB")/>
        <#if CorpActnOptnDtDtlsPricDtlsOverSbcptDpstPric.isPresent()>
            <#list CorpActnOptnDtDtlsPricDtlsOverSbcptDpstPric.get() as field90aPrice>
                <p:OverSbcptDpstPric>
                    <@macros.PriceFormat45Choice field90aPrice/>
                </p:OverSbcptDpstPric>
            </#list>
        </#if>
    </p:PricDtls>
</#macro>
<#macro CorpActnOptnDtlsSctiesMvmntPricDtls corpActnOptnSeqE1>
<p:PricDtls>
    <p:IndctvOrMktPric>
        <p:IndctvPric>
            <#assign CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricIndctvPric = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField90aPrice(), ["/a/qualifier", "/b/qualifier", "/e/qualifier"], "INDC")/>
            <#if CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricIndctvPric.isPresent()>
                <@macros.PriceFormat45Choice CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricIndctvPric.get()/>
            </#if>
        </p:IndctvPric>
        <p:MktPric>
            <#assign CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricMktPric = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField90aPrice(), ["/a/qualifier", "/b/qualifier", "/e/qualifier"], "MRKT")/>
            <#if CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricMktPric.isPresent()>
                <@macros.PriceFormat45Choice CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricMktPric.get()/>
            </#if>
        </p:MktPric>
    </p:IndctvOrMktPric>
    <p:CshInLieuOfShrPric>
        <#assign CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricIndctvPric = statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField90aPrice(), ["/a/qualifier", "/b/qualifier", "/e/qualifier"], "CINL")/>
            <#if CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricIndctvPric.isPresent()>
                <@macros.PriceFormat45Choice CorpActnOptnDtlsSctiesMvmntPricDtlsIndctvOrMktPricIndctvPric.get()/>
            </#if>
    </p:CshInLieuOfShrPric>
    <p:CshValForTax>
        <p:AmtPric>
            <p:AmtPricTp>ACTU</p:AmtPricTp>
            <p:PricVal Ccy="">0.0</p:PricVal>
        </p:AmtPric>
    </p:CshValForTax>
    <p:GncCshPricPdPerPdct>
        <p:PctgPric>
            <p:PctgPricTp>DISC</p:PctgPricTp>
            <p:PricVal>0.0</p:PricVal>
        </p:PctgPric>
    </p:GncCshPricPdPerPdct>
    <p:GncCshPricRcvdPerPdct>
        <p:PctgPric>
            <p:PctgPricTp>DISC</p:PctgPricTp>
            <p:PricVal>0.0</p:PricVal>
        </p:PctgPric>
    </p:GncCshPricRcvdPerPdct>
</p:PricDtls>
</#macro>

