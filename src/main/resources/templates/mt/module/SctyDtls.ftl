<#import "lib/Macros.ftl" as macros/>
<#import "RateAndAmtDtls.ftl" as rateAndAmntDtls/>
<#macro SctyDtls corpActnOptnSeqE1>
    <p:SctyDtls>
        <p:FinInstrmId>
            <@macros.FinInstrmId corpActnOptnSeqE1.getField35BIdentificationOfSecurity()/>
        </p:FinInstrmId>
        <p:PlcOfListg>
            <#if (corpActnOptnSeqE1.getField94aPlace().getB().getPlaceCode())??>
                <#if corpActnOptnSeqE1.getField94aPlace().getB().getDataSourceScheme()??>
                    <p:Prtry>
                        <@macros.GenericIdentification36 TempFinInstrmInd.get().getF() "indicator"/>
                    </p:Prtry>
                <#else>
                    <p:MktIdrCd>${corpActnOptnSeqE1.getField94aPlace().getB().getPlaceCode()}</p:MktIdrCd>
                </#if>
            <#elseif (corpActnOptnSeqE1.getField94aPlace().getB().getNarrative30xFor94B())??>
                <#if corpActnOptnSeqE1.getField94aPlace().getB().getNarrative30xFor94B()?matches("[A-z0-9]{4,4}")>
                    <p:MktIdrCd>${corpActnOptnSeqE1.getField94aPlace().getB().getNarrative30xFor94B()}</p:MktIdrCd>
                <#else>
                    <p:Desc>${corpActnOptnSeqE1.getField94aPlace().getB().getNarrative30xFor94B()}</p:Desc>
                </#if>
            </#if>
        </p:PlcOfListg>
        <#assign DayCntBsis=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getField22a1(),  ["/f/qualifier"], "MICO")/>
        <#if DayCntBsis.isPresent()>
            <p:DayCntBsis>
                <@macros.ProprietaryIdentification propIdntFctnObj=DayCntBsis.get().getF() byWhat="indicator"/>
            </p:DayCntBsis>
        </#if>
        <#if corpActnOptnSeqE1.getSeqE1a()??>
            <#assign ClssfctnFinInstrm=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getSeqE1a().getField12aTypeOfFinancialInstrument(),  ["/a/qualifier", "/c/qualifier"], "CLAS")/>
            <#if ClssfctnFinInstrm.isPresent()>
                <p:ClssfctnTp>
                    <#if ClssfctnFinInstrm.get().getC()??>
                        <p:ClssfctnFinInstrm>${ClssfctnFinInstrm.get().getC().getCFICode()}</p:ClssfctnFinInstrm>
                    <#elseif  ClssfctnFinInstrm.get().getA()??>
                        <p:AltrnClssfctn>
                            <@macros.GenericIdentification36 TempFinInstrmInd.get().getA() "instrumentCodeOrDescription"/>
                        </p:AltrnClssfctn>
                    </#if>
                </p:ClssfctnTp>
            </#if>
            <#assign optStyle=statics['app.etl.iso15022.util.Utility'].filterArrayWithQualifier(corpActnOptnSeqE1.getSeqE1a().getField12aTypeOfFinancialInstrument(),  ["/b/qualifier"], "OPTS")/>
            <#if optStyle.isPresent()>
                <p:OptnStyle>
                    <@macros.ProprietaryIdentification optStyle "instrumentTypeCode"/>
                </p:OptnStyle>
            </#if>
            <p:DnmtnCcy>${(corpActnOptnSeqE1.getSeqE1a().getField11ACurrency().getA().getCurrencyCode())!}</p:DnmtnCcy>
            <p:NxtCpnDt>
                <#assign NxtCpnDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "COUP", "/a/dateYYYYMMDD"))/>
                ${(NxtCpnDt)!}
            </p:NxtCpnDt>
            <p:FltgRateFxgDt>
                <#assign FltgRateFxgDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "FRNR", "/a/dateYYYYMMDD"))/>
                ${(FltgRateFxgDt)!}
            </p:FltgRateFxgDt>
            <p:MtrtyDt>
                <#assign MtrtyDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "MATU", "/a/dateYYYYMMDD"))/>
                ${(MtrtyDt)!}
            </p:MtrtyDt>
            <p:IsseDt>
                <#assign IsseDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "ISSU", "/a/dateYYYYMMDD"))/>
                ${(IsseDt)!}
            </p:IsseDt>
            <p:NxtCllblDt>
                <#assign NxtCllblDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "CALD", "/a/dateYYYYMMDD"))/>
                ${(NxtCllblDt)!}
            </p:NxtCllblDt>
            <p:PutblDt>
                <#assign PutblDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "PUTT", "/a/dateYYYYMMDD"))/>
                ${(PutblDt)!}
            </p:PutblDt>
            <p:DtdDt>
                <#assign DtdDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "DDTE", "/a/dateYYYYMMDD"))/>
                ${(DtdDt)!}
            </p:DtdDt>
            <p:ConvsDt>
                <#assign ConvsDt=macros.unwrapOptionToIsoDate(macros.getValueAfterFilteringArray(corpActnOptnSeqE1.getSeqE1a().getField98a2(), "/a/qualifier", "CONV", "/a/dateYYYYMMDD"))/>
                ${(ConvsDt)!}
            </p:ConvsDt>
        </#if>
        <@rateAndAmntDtls.RateFormat12Choice tag="PrvsFctr" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <@rateAndAmntDtls.RateFormat12Choice tag="NxtFctr" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <@rateAndAmntDtls.RateFormat12Choice tag="IntrstRate" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <@rateAndAmntDtls.RateFormat12Choice tag="NxtIntrstRate" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <@rateAndAmntDtls.RateFormat12Choice tag="MinNmnlQty" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <@rateAndAmntDtls.RateFormat12Choice tag="MinQtyToInst" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <@rateAndAmntDtls.RateFormat12Choice tag="MinMltplQtyToInst" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <@rateAndAmntDtls.RateFormat12Choice tag="CtrctSz" field92aRateArray=corpActnOptnSeqE1.getSeqE1a().getField92aRate() qualifier="PTSC" possibleXPaths=["/a/qualifier", "/k/qualifier"]/>
        <p:IssePric>
            <@macros.PriceFormat45Choice corpActnOptnSeqE1.getSeqE1a().getField90aPrice()/>
        </p:IssePric>
    </p:SctyDtls>
</#macro>