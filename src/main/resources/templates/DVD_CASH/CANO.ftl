<?xml version="1.0" encoding="UTF-8"?>
<Document xmlns="urn:swift:xsd:seev.031.002.04">
    <CorpActnNtfctn>
        <NtfctnGnlInf>
            <NtfctnTp>${STATUS_23G}</NtfctnTp>
            <PrcgSts>
                <Cd>
                    <EvtSts>
                        <EvtCmpltnsSts>COMP</EvtCmpltnsSts>
                        <EvtConfSts>UCON</EvtConfSts>
                    </EvtSts>
                </Cd>
            </PrcgSts>
        </NtfctnGnlInf>
        <CorpActnGnlInf>
            <CorpActnEvtId>${CORP}</CorpActnEvtId>
            <EvtPrcgTp>
                <Cd>DISN</Cd>
            </EvtPrcgTp>
            <EvtTp>
                <Cd>${CAEV}</Cd>
            </EvtTp>
            <MndtryVlntryEvtTp>
                <Cd>MAND</Cd>
            </MndtryVlntryEvtTp>
            <UndrlygScty>
                <FinInstrmId>
                    <OthrId>
                        <Id>${SECID}</Id>
                        <Tp>
                            <Cd>${SECID_TYPE}</Cd>
                        </Tp>
                    </OthrId>
                    <Desc>NA</Desc>
                </FinInstrmId>
            </UndrlygScty>
        </CorpActnGnlInf>
        <AcctDtls>
            <ForAllAccts>
                <IdCd>GENR</IdCd>
            </ForAllAccts>
        </AcctDtls>
        <CorpActnDtls>
            <DtDtls>
                <RcrdDt>
                    <Dt>
                    <Dt>${RDTE}</Dt>
                    </Dt>
                </RcrdDt>
                <ExDvddDt>
                    <Dt>
                    <Dt>${XDTE}</Dt>
                    </Dt>
                </ExDvddDt>
                <AnncmntDt>
                    <Dt>
                    <Dt>${ANOU}</Dt>
                    </Dt>
                </AnncmntDt>
            </DtDtls>
            <DvddTp>
                <Cd>${DIVI}</Cd>
            </DvddTp>
        </CorpActnDtls>
        <CorpActnOptnDtls>
            <OptnNb>001</OptnNb>
            <OptnTp>
                <Cd>CASH</Cd>
            </OptnTp>
            <CcyOptn>${DENO}</CcyOptn>
            <DfltPrcgOrStgInstr>
                <DfltOptnInd>true</DfltOptnInd>
            </DfltPrcgOrStgInstr>
            <RateAndAmtDtls>
                <GrssDvddRate>
                    <RateTpAndAmtAndRateSts>
                        <RateTp>
                            <Cd>INCO</Cd>
                        </RateTp>
                        <Amt Ccy="${CCY_1}">${GRSS_1}</Amt>
                    </RateTpAndAmtAndRateSts>
                </GrssDvddRate>
            </RateAndAmtDtls>
            <CshMvmntDtls>
                <CdtDbtInd>CRDT</CdtDbtInd>
                <DtDtls>
                    <PmtDt>
                        <Dt>
                        <Dt>${PAYD_1}</Dt>
                        </Dt>
                    </PmtDt>
                </DtDtls>
                <RateAndAmtDtls>
                    <GrssDvddRate>
                        <RateTpAndAmtAndRateSts>
                            <RateTp>
                                <Cd>INCO</Cd>
                            </RateTp>
                            <Amt Ccy="${CCY_1}">${GRSS_1}</Amt>
                        </RateTpAndAmtAndRateSts>
                    </GrssDvddRate>
                    <WhldgTaxRate>
                        <Rate>${TAXR_1}</Rate>
                    </WhldgTaxRate>
                    <NetDvddRate>
                        <Amt Ccy="${CCY_1}">${NETT_1}</Amt>
                    </NetDvddRate>
                </RateAndAmtDtls>
            </CshMvmntDtls>
        </CorpActnOptnDtls>
        <AddtlInf>
            <AddtlTxt>
                <UpdDt>${PREP}</UpdDt>
                <AddtlInf>${ADTX}</AddtlInf>
            </AddtlTxt>
        </AddtlInf>
    </CorpActnNtfctn>
</Document>
