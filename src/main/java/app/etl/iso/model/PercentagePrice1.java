
package app.etl.iso.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PercentagePrice1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PercentagePrice1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PctgPricTp" type="{urn:swift:xsd:seev.031.002.04}PriceRateType3Code"/>
 *         &lt;element name="PricVal" type="{urn:swift:xsd:seev.031.002.04}PercentageRate"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PercentagePrice1", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "pctgPricTp",
    "pricVal"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class PercentagePrice1 {

    @XmlElement(name = "PctgPricTp", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected PriceRateType3Code pctgPricTp;
    @XmlElement(name = "PricVal", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BigDecimal pricVal;

    /**
     * Gets the value of the pctgPricTp property.
     * 
     * @return
     *     possible object is
     *     {@link PriceRateType3Code }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public PriceRateType3Code getPctgPricTp() {
        return pctgPricTp;
    }

    /**
     * Sets the value of the pctgPricTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceRateType3Code }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setPctgPricTp(PriceRateType3Code value) {
        this.pctgPricTp = value;
    }

    /**
     * Gets the value of the pricVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public BigDecimal getPricVal() {
        return pricVal;
    }

    /**
     * Sets the value of the pricVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setPricVal(BigDecimal value) {
        this.pricVal = value;
    }

}
