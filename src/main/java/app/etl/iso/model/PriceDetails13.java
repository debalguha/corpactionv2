
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceDetails13 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceDetails13">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GncCshPricPdPerPdct" type="{urn:swift:xsd:seev.031.002.04}PriceFormat27Choice" minOccurs="0"/>
 *         &lt;element name="GncCshPricRcvdPerPdct" type="{urn:swift:xsd:seev.031.002.04}PriceFormat26Choice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceDetails13", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "gncCshPricPdPerPdct",
    "gncCshPricRcvdPerPdct"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class PriceDetails13 {

    @XmlElement(name = "GncCshPricPdPerPdct", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected PriceFormat27Choice gncCshPricPdPerPdct;
    @XmlElement(name = "GncCshPricRcvdPerPdct", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected PriceFormat26Choice gncCshPricRcvdPerPdct;

    /**
     * Gets the value of the gncCshPricPdPerPdct property.
     * 
     * @return
     *     possible object is
     *     {@link PriceFormat27Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public PriceFormat27Choice getGncCshPricPdPerPdct() {
        return gncCshPricPdPerPdct;
    }

    /**
     * Sets the value of the gncCshPricPdPerPdct property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceFormat27Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setGncCshPricPdPerPdct(PriceFormat27Choice value) {
        this.gncCshPricPdPerPdct = value;
    }

    /**
     * Gets the value of the gncCshPricRcvdPerPdct property.
     * 
     * @return
     *     possible object is
     *     {@link PriceFormat26Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public PriceFormat26Choice getGncCshPricRcvdPerPdct() {
        return gncCshPricRcvdPerPdct;
    }

    /**
     * Sets the value of the gncCshPricRcvdPerPdct property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceFormat26Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setGncCshPricRcvdPerPdct(PriceFormat26Choice value) {
        this.gncCshPricRcvdPerPdct = value;
    }

}
