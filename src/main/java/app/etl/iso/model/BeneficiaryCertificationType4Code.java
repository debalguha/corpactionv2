
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BeneficiaryCertificationType4Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BeneficiaryCertificationType4Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACCI"/>
 *     &lt;enumeration value="DOMI"/>
 *     &lt;enumeration value="NDOM"/>
 *     &lt;enumeration value="FULL"/>
 *     &lt;enumeration value="NCOM"/>
 *     &lt;enumeration value="QIBB"/>
 *     &lt;enumeration value="TRBD"/>
 *     &lt;enumeration value="PAPW"/>
 *     &lt;enumeration value="PABD"/>
 *     &lt;enumeration value="FRAC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BeneficiaryCertificationType4Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum BeneficiaryCertificationType4Code {

    ACCI,
    DOMI,
    NDOM,
    FULL,
    NCOM,
    QIBB,
    TRBD,
    PAPW,
    PABD,
    FRAC;

    public String value() {
        return name();
    }

    public static BeneficiaryCertificationType4Code fromValue(String v) {
        return valueOf(v);
    }

}
