
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionEventStage1Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateActionEventStage1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="APPD"/>
 *     &lt;enumeration value="CLDE"/>
 *     &lt;enumeration value="PWAL"/>
 *     &lt;enumeration value="SUAP"/>
 *     &lt;enumeration value="UNAC"/>
 *     &lt;enumeration value="WHOU"/>
 *     &lt;enumeration value="FULL"/>
 *     &lt;enumeration value="LAPS"/>
 *     &lt;enumeration value="PART"/>
 *     &lt;enumeration value="RESC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateActionEventStage1Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum CorporateActionEventStage1Code {

    APPD,
    CLDE,
    PWAL,
    SUAP,
    UNAC,
    WHOU,
    FULL,
    LAPS,
    PART,
    RESC;

    public String value() {
        return name();
    }

    public static CorporateActionEventStage1Code fromValue(String v) {
        return valueOf(v);
    }

}
