
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionEventStatus1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateActionEventStatus1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EvtCmpltnsSts" type="{urn:swift:xsd:seev.031.002.04}EventCompletenessStatus1Code"/>
 *         &lt;element name="EvtConfSts" type="{urn:swift:xsd:seev.031.002.04}EventConfirmationStatus1Code"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateActionEventStatus1", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "evtCmpltnsSts",
    "evtConfSts"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class CorporateActionEventStatus1 {

    @XmlElement(name = "EvtCmpltnsSts", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected EventCompletenessStatus1Code evtCmpltnsSts;
    @XmlElement(name = "EvtConfSts", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected EventConfirmationStatus1Code evtConfSts;

    /**
     * Gets the value of the evtCmpltnsSts property.
     * 
     * @return
     *     possible object is
     *     {@link EventCompletenessStatus1Code }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public EventCompletenessStatus1Code getEvtCmpltnsSts() {
        return evtCmpltnsSts;
    }

    /**
     * Sets the value of the evtCmpltnsSts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventCompletenessStatus1Code }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEvtCmpltnsSts(EventCompletenessStatus1Code value) {
        this.evtCmpltnsSts = value;
    }

    /**
     * Gets the value of the evtConfSts property.
     * 
     * @return
     *     possible object is
     *     {@link EventConfirmationStatus1Code }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public EventConfirmationStatus1Code getEvtConfSts() {
        return evtConfSts;
    }

    /**
     * Sets the value of the evtConfSts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventConfirmationStatus1Code }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEvtConfSts(EventConfirmationStatus1Code value) {
        this.evtConfSts = value;
    }

}
