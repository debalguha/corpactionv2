
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionOption7Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateActionOption7Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ABST"/>
 *     &lt;enumeration value="AMGT"/>
 *     &lt;enumeration value="BSPL"/>
 *     &lt;enumeration value="BUYA"/>
 *     &lt;enumeration value="CASE"/>
 *     &lt;enumeration value="CASH"/>
 *     &lt;enumeration value="CEXC"/>
 *     &lt;enumeration value="CONN"/>
 *     &lt;enumeration value="CONY"/>
 *     &lt;enumeration value="CTEN"/>
 *     &lt;enumeration value="EXER"/>
 *     &lt;enumeration value="LAPS"/>
 *     &lt;enumeration value="MNGT"/>
 *     &lt;enumeration value="MPUT"/>
 *     &lt;enumeration value="NOAC"/>
 *     &lt;enumeration value="NOQU"/>
 *     &lt;enumeration value="OFFR"/>
 *     &lt;enumeration value="OTHR"/>
 *     &lt;enumeration value="OVER"/>
 *     &lt;enumeration value="PROX"/>
 *     &lt;enumeration value="QINV"/>
 *     &lt;enumeration value="SECU"/>
 *     &lt;enumeration value="SLLE"/>
 *     &lt;enumeration value="SPLI"/>
 *     &lt;enumeration value="PRUN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateActionOption7Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum CorporateActionOption7Code {

    ABST,
    AMGT,
    BSPL,
    BUYA,
    CASE,
    CASH,
    CEXC,
    CONN,
    CONY,
    CTEN,
    EXER,
    LAPS,
    MNGT,
    MPUT,
    NOAC,
    NOQU,
    OFFR,
    OTHR,
    OVER,
    PROX,
    QINV,
    SECU,
    SLLE,
    SPLI,
    PRUN;

    public String value() {
        return name();
    }

    public static CorporateActionOption7Code fromValue(String v) {
        return valueOf(v);
    }

}
