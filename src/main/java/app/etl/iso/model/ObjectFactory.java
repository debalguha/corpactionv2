
package app.etl.iso.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the app.etl.iso.model package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Document_QNAME = new QName("urn:swift:xsd:seev.031.002.04", "Document");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: app.etl.iso.model
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link DateAndDateTimeChoice }
     * 
     */
    public DateAndDateTimeChoice createDateAndDateTimeChoice() {
        return new DateAndDateTimeChoice();
    }

    /**
     * Create an instance of {@link RateType26Choice }
     * 
     */
    public RateType26Choice createRateType26Choice() {
        return new RateType26Choice();
    }

    /**
     * Create an instance of {@link AmountPrice4 }
     * 
     */
    public AmountPrice4 createAmountPrice4() {
        return new AmountPrice4();
    }

    /**
     * Create an instance of {@link AmountPrice5 }
     * 
     */
    public AmountPrice5 createAmountPrice5() {
        return new AmountPrice5();
    }

    /**
     * Create an instance of {@link DateFormat23Choice }
     * 
     */
    public DateFormat23Choice createDateFormat23Choice() {
        return new DateFormat23Choice();
    }

    /**
     * Create an instance of {@link CorporateActionOption17Choice }
     * 
     */
    public CorporateActionOption17Choice createCorporateActionOption17Choice() {
        return new CorporateActionOption17Choice();
    }

    /**
     * Create an instance of {@link DateCode8Choice }
     * 
     */
    public DateCode8Choice createDateCode8Choice() {
        return new DateCode8Choice();
    }

    /**
     * Create an instance of {@link ForeignExchangeTerms19 }
     * 
     */
    public ForeignExchangeTerms19 createForeignExchangeTerms19() {
        return new ForeignExchangeTerms19();
    }

    /**
     * Create an instance of {@link RateAndAmountFormat13Choice }
     * 
     */
    public RateAndAmountFormat13Choice createRateAndAmountFormat13Choice() {
        return new RateAndAmountFormat13Choice();
    }

    /**
     * Create an instance of {@link AccountIdentification10 }
     * 
     */
    public AccountIdentification10 createAccountIdentification10() {
        return new AccountIdentification10();
    }

    /**
     * Create an instance of {@link ForeignExchangeTerms15 }
     * 
     */
    public ForeignExchangeTerms15 createForeignExchangeTerms15() {
        return new ForeignExchangeTerms15();
    }

    /**
     * Create an instance of {@link TaxableIncomePerShareCalculatedFormat2Choice }
     * 
     */
    public TaxableIncomePerShareCalculatedFormat2Choice createTaxableIncomePerShareCalculatedFormat2Choice() {
        return new TaxableIncomePerShareCalculatedFormat2Choice();
    }

    /**
     * Create an instance of {@link AdditionalBusinessProcessFormat4Choice }
     * 
     */
    public AdditionalBusinessProcessFormat4Choice createAdditionalBusinessProcessFormat4Choice() {
        return new AdditionalBusinessProcessFormat4Choice();
    }

    /**
     * Create an instance of {@link RestrictedFINActiveCurrencyAnd13DecimalAmount }
     * 
     */
    public RestrictedFINActiveCurrencyAnd13DecimalAmount createRestrictedFINActiveCurrencyAnd13DecimalAmount() {
        return new RestrictedFINActiveCurrencyAnd13DecimalAmount();
    }

    /**
     * Create an instance of {@link SignedQuantityFormat3 }
     * 
     */
    public SignedQuantityFormat3 createSignedQuantityFormat3() {
        return new SignedQuantityFormat3();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus20 }
     * 
     */
    public RateTypeAndAmountAndStatus20 createRateTypeAndAmountAndStatus20() {
        return new RateTypeAndAmountAndStatus20();
    }

    /**
     * Create an instance of {@link AccountIdentification22Choice }
     * 
     */
    public AccountIdentification22Choice createAccountIdentification22Choice() {
        return new AccountIdentification22Choice();
    }

    /**
     * Create an instance of {@link CorporateActionNotification3 }
     * 
     */
    public CorporateActionNotification3 createCorporateActionNotification3() {
        return new CorporateActionNotification3();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus21 }
     * 
     */
    public RateTypeAndAmountAndStatus21 createRateTypeAndAmountAndStatus21() {
        return new RateTypeAndAmountAndStatus21();
    }

    /**
     * Create an instance of {@link SignedQuantityFormat4 }
     * 
     */
    public SignedQuantityFormat4 createSignedQuantityFormat4() {
        return new SignedQuantityFormat4();
    }

    /**
     * Create an instance of {@link SafekeepingPlaceFormat5Choice }
     * 
     */
    public SafekeepingPlaceFormat5Choice createSafekeepingPlaceFormat5Choice() {
        return new SafekeepingPlaceFormat5Choice();
    }

    /**
     * Create an instance of {@link CapitalGainFormat2Choice }
     * 
     */
    public CapitalGainFormat2Choice createCapitalGainFormat2Choice() {
        return new CapitalGainFormat2Choice();
    }

    /**
     * Create an instance of {@link CorporateActionChangeTypeFormat3Choice }
     * 
     */
    public CorporateActionChangeTypeFormat3Choice createCorporateActionChangeTypeFormat3Choice() {
        return new CorporateActionChangeTypeFormat3Choice();
    }

    /**
     * Create an instance of {@link CorporateActionNotification002V04 }
     * 
     */
    public CorporateActionNotification002V04 createCorporateActionNotification002V04() {
        return new CorporateActionNotification002V04();
    }

    /**
     * Create an instance of {@link DateCode12Choice }
     * 
     */
    public DateCode12Choice createDateCode12Choice() {
        return new DateCode12Choice();
    }

    /**
     * Create an instance of {@link RateType18Choice }
     * 
     */
    public RateType18Choice createRateType18Choice() {
        return new RateType18Choice();
    }

    /**
     * Create an instance of {@link RateType14Choice }
     * 
     */
    public RateType14Choice createRateType14Choice() {
        return new RateType14Choice();
    }

    /**
     * Create an instance of {@link BeneficiaryCertificationType7Choice }
     * 
     */
    public BeneficiaryCertificationType7Choice createBeneficiaryCertificationType7Choice() {
        return new BeneficiaryCertificationType7Choice();
    }

    /**
     * Create an instance of {@link CorporateActionProcessingStatus3Choice }
     * 
     */
    public CorporateActionProcessingStatus3Choice createCorporateActionProcessingStatus3Choice() {
        return new CorporateActionProcessingStatus3Choice();
    }

    /**
     * Create an instance of {@link IdentificationSource4Choice }
     * 
     */
    public IdentificationSource4Choice createIdentificationSource4Choice() {
        return new IdentificationSource4Choice();
    }

    /**
     * Create an instance of {@link Quantity9Choice }
     * 
     */
    public Quantity9Choice createQuantity9Choice() {
        return new Quantity9Choice();
    }

    /**
     * Create an instance of {@link CorporateActionEventProcessingType1Choice }
     * 
     */
    public CorporateActionEventProcessingType1Choice createCorporateActionEventProcessingType1Choice() {
        return new CorporateActionEventProcessingType1Choice();
    }

    /**
     * Create an instance of {@link RateType25Choice }
     * 
     */
    public RateType25Choice createRateType25Choice() {
        return new RateType25Choice();
    }

    /**
     * Create an instance of {@link RenounceableEntitlementStatusTypeFormat2Choice }
     * 
     */
    public RenounceableEntitlementStatusTypeFormat2Choice createRenounceableEntitlementStatusTypeFormat2Choice() {
        return new RenounceableEntitlementStatusTypeFormat2Choice();
    }

    /**
     * Create an instance of {@link FinancialInstrumentAttributes45 }
     * 
     */
    public FinancialInstrumentAttributes45 createFinancialInstrumentAttributes45() {
        return new FinancialInstrumentAttributes45();
    }

    /**
     * Create an instance of {@link DefaultProcessingOrStandingInstruction1Choice }
     * 
     */
    public DefaultProcessingOrStandingInstruction1Choice createDefaultProcessingOrStandingInstruction1Choice() {
        return new DefaultProcessingOrStandingInstruction1Choice();
    }

    /**
     * Create an instance of {@link FinancialInstrumentAttributes47 }
     * 
     */
    public FinancialInstrumentAttributes47 createFinancialInstrumentAttributes47() {
        return new FinancialInstrumentAttributes47();
    }

    /**
     * Create an instance of {@link PriceDetails13 }
     * 
     */
    public PriceDetails13 createPriceDetails13() {
        return new PriceDetails13();
    }

    /**
     * Create an instance of {@link RateDetails13 }
     * 
     */
    public RateDetails13 createRateDetails13() {
        return new RateDetails13();
    }

    /**
     * Create an instance of {@link FinancialInstrumentAttributes40 }
     * 
     */
    public FinancialInstrumentAttributes40 createFinancialInstrumentAttributes40() {
        return new FinancialInstrumentAttributes40();
    }

    /**
     * Create an instance of {@link RateFormat7Choice }
     * 
     */
    public RateFormat7Choice createRateFormat7Choice() {
        return new RateFormat7Choice();
    }

    /**
     * Create an instance of {@link SolicitationFeeRateFormat6Choice }
     * 
     */
    public SolicitationFeeRateFormat6Choice createSolicitationFeeRateFormat6Choice() {
        return new SolicitationFeeRateFormat6Choice();
    }

    /**
     * Create an instance of {@link CorporateActionEventStageFormat4Choice }
     * 
     */
    public CorporateActionEventStageFormat4Choice createCorporateActionEventStageFormat4Choice() {
        return new CorporateActionEventStageFormat4Choice();
    }

    /**
     * Create an instance of {@link NetDividendRateFormat16Choice }
     * 
     */
    public NetDividendRateFormat16Choice createNetDividendRateFormat16Choice() {
        return new NetDividendRateFormat16Choice();
    }

    /**
     * Create an instance of {@link DocumentNumber2Choice }
     * 
     */
    public DocumentNumber2Choice createDocumentNumber2Choice() {
        return new DocumentNumber2Choice();
    }

    /**
     * Create an instance of {@link CorporateActionPrice34 }
     * 
     */
    public CorporateActionPrice34 createCorporateActionPrice34() {
        return new CorporateActionPrice34();
    }

    /**
     * Create an instance of {@link RateType12Choice }
     * 
     */
    public RateType12Choice createRateType12Choice() {
        return new RateType12Choice();
    }

    /**
     * Create an instance of {@link SafekeepingPlaceTypeAndAnyBICIdentifier1 }
     * 
     */
    public SafekeepingPlaceTypeAndAnyBICIdentifier1 createSafekeepingPlaceTypeAndAnyBICIdentifier1() {
        return new SafekeepingPlaceTypeAndAnyBICIdentifier1();
    }

    /**
     * Create an instance of {@link CorporateActionMandatoryVoluntary2Choice }
     * 
     */
    public CorporateActionMandatoryVoluntary2Choice createCorporateActionMandatoryVoluntary2Choice() {
        return new CorporateActionMandatoryVoluntary2Choice();
    }

    /**
     * Create an instance of {@link GrossDividendRateFormat13Choice }
     * 
     */
    public GrossDividendRateFormat13Choice createGrossDividendRateFormat13Choice() {
        return new GrossDividendRateFormat13Choice();
    }

    /**
     * Create an instance of {@link SafekeepingPlaceTypeAndText5 }
     * 
     */
    public SafekeepingPlaceTypeAndText5 createSafekeepingPlaceTypeAndText5() {
        return new SafekeepingPlaceTypeAndText5();
    }

    /**
     * Create an instance of {@link DateFormat22Choice }
     * 
     */
    public DateFormat22Choice createDateFormat22Choice() {
        return new DateFormat22Choice();
    }

    /**
     * Create an instance of {@link DateFormat17Choice }
     * 
     */
    public DateFormat17Choice createDateFormat17Choice() {
        return new DateFormat17Choice();
    }

    /**
     * Create an instance of {@link Period4 }
     * 
     */
    public Period4 createPeriod4() {
        return new Period4();
    }

    /**
     * Create an instance of {@link CorporateActionNarrative22 }
     * 
     */
    public CorporateActionNarrative22 createCorporateActionNarrative22() {
        return new CorporateActionNarrative22();
    }

    /**
     * Create an instance of {@link SupplementaryData1 }
     * 
     */
    public SupplementaryData1 createSupplementaryData1() {
        return new SupplementaryData1();
    }

    /**
     * Create an instance of {@link CorporateActionPrice41 }
     * 
     */
    public CorporateActionPrice41 createCorporateActionPrice41() {
        return new CorporateActionPrice41();
    }

    /**
     * Create an instance of {@link LotteryTypeFormat2Choice }
     * 
     */
    public LotteryTypeFormat2Choice createLotteryTypeFormat2Choice() {
        return new LotteryTypeFormat2Choice();
    }

    /**
     * Create an instance of {@link DateCode7Choice }
     * 
     */
    public DateCode7Choice createDateCode7Choice() {
        return new DateCode7Choice();
    }

    /**
     * Create an instance of {@link CorporateActionNarrative25 }
     * 
     */
    public CorporateActionNarrative25 createCorporateActionNarrative25() {
        return new CorporateActionNarrative25();
    }

    /**
     * Create an instance of {@link NonEligibleProceedsIndicator2Choice }
     * 
     */
    public NonEligibleProceedsIndicator2Choice createNonEligibleProceedsIndicator2Choice() {
        return new NonEligibleProceedsIndicator2Choice();
    }

    /**
     * Create an instance of {@link AccountAndBalance24 }
     * 
     */
    public AccountAndBalance24 createAccountAndBalance24() {
        return new AccountAndBalance24();
    }

    /**
     * Create an instance of {@link CorporateActionQuantity6 }
     * 
     */
    public CorporateActionQuantity6 createCorporateActionQuantity6() {
        return new CorporateActionQuantity6();
    }

    /**
     * Create an instance of {@link RatioFormat14Choice }
     * 
     */
    public RatioFormat14Choice createRatioFormat14Choice() {
        return new RatioFormat14Choice();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus10 }
     * 
     */
    public RateTypeAndAmountAndStatus10 createRateTypeAndAmountAndStatus10() {
        return new RateTypeAndAmountAndStatus10();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus19 }
     * 
     */
    public RateTypeAndAmountAndStatus19 createRateTypeAndAmountAndStatus19() {
        return new RateTypeAndAmountAndStatus19();
    }

    /**
     * Create an instance of {@link CorporateActionNarrative17 }
     * 
     */
    public CorporateActionNarrative17 createCorporateActionNarrative17() {
        return new CorporateActionNarrative17();
    }

    /**
     * Create an instance of {@link RateFormat6Choice }
     * 
     */
    public RateFormat6Choice createRateFormat6Choice() {
        return new RateFormat6Choice();
    }

    /**
     * Create an instance of {@link CorporateActionGeneralInformation67 }
     * 
     */
    public CorporateActionGeneralInformation67 createCorporateActionGeneralInformation67() {
        return new CorporateActionGeneralInformation67();
    }

    /**
     * Create an instance of {@link CorporateActionRate41 }
     * 
     */
    public CorporateActionRate41 createCorporateActionRate41() {
        return new CorporateActionRate41();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus17 }
     * 
     */
    public RateTypeAndAmountAndStatus17 createRateTypeAndAmountAndStatus17() {
        return new RateTypeAndAmountAndStatus17();
    }

    /**
     * Create an instance of {@link CorporateActionRate42 }
     * 
     */
    public CorporateActionRate42 createCorporateActionRate42() {
        return new CorporateActionRate42();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus18 }
     * 
     */
    public RateTypeAndAmountAndStatus18 createRateTypeAndAmountAndStatus18() {
        return new RateTypeAndAmountAndStatus18();
    }

    /**
     * Create an instance of {@link ProprietaryQuantity5 }
     * 
     */
    public ProprietaryQuantity5 createProprietaryQuantity5() {
        return new ProprietaryQuantity5();
    }

    /**
     * Create an instance of {@link ProprietaryQuantity4 }
     * 
     */
    public ProprietaryQuantity4 createProprietaryQuantity4() {
        return new ProprietaryQuantity4();
    }

    /**
     * Create an instance of {@link AmountAndQuantityRatio3 }
     * 
     */
    public AmountAndQuantityRatio3 createAmountAndQuantityRatio3() {
        return new AmountAndQuantityRatio3();
    }

    /**
     * Create an instance of {@link RateType27Choice }
     * 
     */
    public RateType27Choice createRateType27Choice() {
        return new RateType27Choice();
    }

    /**
     * Create an instance of {@link CorporateActionProcessingStatus1Choice }
     * 
     */
    public CorporateActionProcessingStatus1Choice createCorporateActionProcessingStatus1Choice() {
        return new CorporateActionProcessingStatus1Choice();
    }

    /**
     * Create an instance of {@link NetDividendRateFormat15Choice }
     * 
     */
    public NetDividendRateFormat15Choice createNetDividendRateFormat15Choice() {
        return new NetDividendRateFormat15Choice();
    }

    /**
     * Create an instance of {@link ConversionTypeFormat2Choice }
     * 
     */
    public ConversionTypeFormat2Choice createConversionTypeFormat2Choice() {
        return new ConversionTypeFormat2Choice();
    }

    /**
     * Create an instance of {@link DistributionTypeFormat3Choice }
     * 
     */
    public DistributionTypeFormat3Choice createDistributionTypeFormat3Choice() {
        return new DistributionTypeFormat3Choice();
    }

    /**
     * Create an instance of {@link AmountPricePerAmount3 }
     * 
     */
    public AmountPricePerAmount3 createAmountPricePerAmount3() {
        return new AmountPricePerAmount3();
    }

    /**
     * Create an instance of {@link RatioFormat13Choice }
     * 
     */
    public RatioFormat13Choice createRatioFormat13Choice() {
        return new RatioFormat13Choice();
    }

    /**
     * Create an instance of {@link OriginalAndCurrentQuantities3 }
     * 
     */
    public OriginalAndCurrentQuantities3 createOriginalAndCurrentQuantities3() {
        return new OriginalAndCurrentQuantities3();
    }

    /**
     * Create an instance of {@link RateAndAmountFormat17Choice }
     * 
     */
    public RateAndAmountFormat17Choice createRateAndAmountFormat17Choice() {
        return new RateAndAmountFormat17Choice();
    }

    /**
     * Create an instance of {@link OriginalAndCurrentQuantities4 }
     * 
     */
    public OriginalAndCurrentQuantities4 createOriginalAndCurrentQuantities4() {
        return new OriginalAndCurrentQuantities4();
    }

    /**
     * Create an instance of {@link Quantity7Choice }
     * 
     */
    public Quantity7Choice createQuantity7Choice() {
        return new Quantity7Choice();
    }

    /**
     * Create an instance of {@link CorporateActionEventReference2 }
     * 
     */
    public CorporateActionEventReference2 createCorporateActionEventReference2() {
        return new CorporateActionEventReference2();
    }

    /**
     * Create an instance of {@link RateStatus2Choice }
     * 
     */
    public RateStatus2Choice createRateStatus2Choice() {
        return new RateStatus2Choice();
    }

    /**
     * Create an instance of {@link DateFormat13Choice }
     * 
     */
    public DateFormat13Choice createDateFormat13Choice() {
        return new DateFormat13Choice();
    }

    /**
     * Create an instance of {@link CorporateActionBalanceDetails20 }
     * 
     */
    public CorporateActionBalanceDetails20 createCorporateActionBalanceDetails20() {
        return new CorporateActionBalanceDetails20();
    }

    /**
     * Create an instance of {@link IndicativeOrMarketPrice6Choice }
     * 
     */
    public IndicativeOrMarketPrice6Choice createIndicativeOrMarketPrice6Choice() {
        return new IndicativeOrMarketPrice6Choice();
    }

    /**
     * Create an instance of {@link RateFormat3Choice }
     * 
     */
    public RateFormat3Choice createRateFormat3Choice() {
        return new RateFormat3Choice();
    }

    /**
     * Create an instance of {@link SecuritiesOption37 }
     * 
     */
    public SecuritiesOption37 createSecuritiesOption37() {
        return new SecuritiesOption37();
    }

    /**
     * Create an instance of {@link RateAndAmountFormat16Choice }
     * 
     */
    public RateAndAmountFormat16Choice createRateAndAmountFormat16Choice() {
        return new RateAndAmountFormat16Choice();
    }

    /**
     * Create an instance of {@link NameAndAddress12 }
     * 
     */
    public NameAndAddress12 createNameAndAddress12() {
        return new NameAndAddress12();
    }

    /**
     * Create an instance of {@link BalanceFormat2Choice }
     * 
     */
    public BalanceFormat2Choice createBalanceFormat2Choice() {
        return new BalanceFormat2Choice();
    }

    /**
     * Create an instance of {@link DocumentIdentification17 }
     * 
     */
    public DocumentIdentification17 createDocumentIdentification17() {
        return new DocumentIdentification17();
    }

    /**
     * Create an instance of {@link OptionAvailabilityStatus2Choice }
     * 
     */
    public OptionAvailabilityStatus2Choice createOptionAvailabilityStatus2Choice() {
        return new OptionAvailabilityStatus2Choice();
    }

    /**
     * Create an instance of {@link PartyIdentification51Choice }
     * 
     */
    public PartyIdentification51Choice createPartyIdentification51Choice() {
        return new PartyIdentification51Choice();
    }

    /**
     * Create an instance of {@link DocumentIdentification2Choice }
     * 
     */
    public DocumentIdentification2Choice createDocumentIdentification2Choice() {
        return new DocumentIdentification2Choice();
    }

    /**
     * Create an instance of {@link OfferTypeFormat4Choice }
     * 
     */
    public OfferTypeFormat4Choice createOfferTypeFormat4Choice() {
        return new OfferTypeFormat4Choice();
    }

    /**
     * Create an instance of {@link SecuritiesOption28 }
     * 
     */
    public SecuritiesOption28 createSecuritiesOption28() {
        return new SecuritiesOption28();
    }

    /**
     * Create an instance of {@link CorporateActionEventReference2Choice }
     * 
     */
    public CorporateActionEventReference2Choice createCorporateActionEventReference2Choice() {
        return new CorporateActionEventReference2Choice();
    }

    /**
     * Create an instance of {@link OptionFeaturesFormat11Choice }
     * 
     */
    public OptionFeaturesFormat11Choice createOptionFeaturesFormat11Choice() {
        return new OptionFeaturesFormat11Choice();
    }

    /**
     * Create an instance of {@link CorporateActionRate33 }
     * 
     */
    public CorporateActionRate33 createCorporateActionRate33() {
        return new CorporateActionRate33();
    }

    /**
     * Create an instance of {@link AmountPricePerFinancialInstrumentQuantity4 }
     * 
     */
    public AmountPricePerFinancialInstrumentQuantity4 createAmountPricePerFinancialInstrumentQuantity4() {
        return new AmountPricePerFinancialInstrumentQuantity4();
    }

    /**
     * Create an instance of {@link DocumentIdentification19 }
     * 
     */
    public DocumentIdentification19 createDocumentIdentification19() {
        return new DocumentIdentification19();
    }

    /**
     * Create an instance of {@link UpdatedAdditionalInformation6 }
     * 
     */
    public UpdatedAdditionalInformation6 createUpdatedAdditionalInformation6() {
        return new UpdatedAdditionalInformation6();
    }

    /**
     * Create an instance of {@link UpdatedAdditionalInformation4 }
     * 
     */
    public UpdatedAdditionalInformation4 createUpdatedAdditionalInformation4() {
        return new UpdatedAdditionalInformation4();
    }

    /**
     * Create an instance of {@link UpdatedAdditionalInformation5 }
     * 
     */
    public UpdatedAdditionalInformation5 createUpdatedAdditionalInformation5() {
        return new UpdatedAdditionalInformation5();
    }

    /**
     * Create an instance of {@link DateFormat18Choice }
     * 
     */
    public DateFormat18Choice createDateFormat18Choice() {
        return new DateFormat18Choice();
    }

    /**
     * Create an instance of {@link RateFormat9Choice }
     * 
     */
    public RateFormat9Choice createRateFormat9Choice() {
        return new RateFormat9Choice();
    }

    /**
     * Create an instance of {@link RateType24Choice }
     * 
     */
    public RateType24Choice createRateType24Choice() {
        return new RateType24Choice();
    }

    /**
     * Create an instance of {@link TemporaryFinancialInstrumentIndicator2Choice }
     * 
     */
    public TemporaryFinancialInstrumentIndicator2Choice createTemporaryFinancialInstrumentIndicator2Choice() {
        return new TemporaryFinancialInstrumentIndicator2Choice();
    }

    /**
     * Create an instance of {@link CorporateAction11 }
     * 
     */
    public CorporateAction11 createCorporateAction11() {
        return new CorporateAction11();
    }

    /**
     * Create an instance of {@link OtherIdentification2 }
     * 
     */
    public OtherIdentification2 createOtherIdentification2() {
        return new OtherIdentification2();
    }

    /**
     * Create an instance of {@link TaxCreditRateFormat6Choice }
     * 
     */
    public TaxCreditRateFormat6Choice createTaxCreditRateFormat6Choice() {
        return new TaxCreditRateFormat6Choice();
    }

    /**
     * Create an instance of {@link CorporateActionEventStatus1 }
     * 
     */
    public CorporateActionEventStatus1 createCorporateActionEventStatus1() {
        return new CorporateActionEventStatus1();
    }

    /**
     * Create an instance of {@link IdentificationFormat2Choice }
     * 
     */
    public IdentificationFormat2Choice createIdentificationFormat2Choice() {
        return new IdentificationFormat2Choice();
    }

    /**
     * Create an instance of {@link IntermediateSecuritiesDistributionTypeFormat10Choice }
     * 
     */
    public IntermediateSecuritiesDistributionTypeFormat10Choice createIntermediateSecuritiesDistributionTypeFormat10Choice() {
        return new IntermediateSecuritiesDistributionTypeFormat10Choice();
    }

    /**
     * Create an instance of {@link QuantityToQuantityRatio2 }
     * 
     */
    public QuantityToQuantityRatio2 createQuantityToQuantityRatio2() {
        return new QuantityToQuantityRatio2();
    }

    /**
     * Create an instance of {@link ProcessingPosition3Choice }
     * 
     */
    public ProcessingPosition3Choice createProcessingPosition3Choice() {
        return new ProcessingPosition3Choice();
    }

    /**
     * Create an instance of {@link CorporateActionPrice25 }
     * 
     */
    public CorporateActionPrice25 createCorporateActionPrice25() {
        return new CorporateActionPrice25();
    }

    /**
     * Create an instance of {@link GenericIdentification24 }
     * 
     */
    public GenericIdentification24 createGenericIdentification24() {
        return new GenericIdentification24();
    }

    /**
     * Create an instance of {@link GenericIdentification23 }
     * 
     */
    public GenericIdentification23 createGenericIdentification23() {
        return new GenericIdentification23();
    }

    /**
     * Create an instance of {@link Period3Choice }
     * 
     */
    public Period3Choice createPeriod3Choice() {
        return new Period3Choice();
    }

    /**
     * Create an instance of {@link GenericIdentification20 }
     * 
     */
    public GenericIdentification20 createGenericIdentification20() {
        return new GenericIdentification20();
    }

    /**
     * Create an instance of {@link RateType9Choice }
     * 
     */
    public RateType9Choice createRateType9Choice() {
        return new RateType9Choice();
    }

    /**
     * Create an instance of {@link ClassificationType3Choice }
     * 
     */
    public ClassificationType3Choice createClassificationType3Choice() {
        return new ClassificationType3Choice();
    }

    /**
     * Create an instance of {@link GrossDividendRateFormat14Choice }
     * 
     */
    public GrossDividendRateFormat14Choice createGrossDividendRateFormat14Choice() {
        return new GrossDividendRateFormat14Choice();
    }

    /**
     * Create an instance of {@link Quantity8Choice }
     * 
     */
    public Quantity8Choice createQuantity8Choice() {
        return new Quantity8Choice();
    }

    /**
     * Create an instance of {@link Quantity10Choice }
     * 
     */
    public Quantity10Choice createQuantity10Choice() {
        return new Quantity10Choice();
    }

    /**
     * Create an instance of {@link GenericIdentification26 }
     * 
     */
    public GenericIdentification26 createGenericIdentification26() {
        return new GenericIdentification26();
    }

    /**
     * Create an instance of {@link RestrictedFINActiveCurrencyAndAmount }
     * 
     */
    public RestrictedFINActiveCurrencyAndAmount createRestrictedFINActiveCurrencyAndAmount() {
        return new RestrictedFINActiveCurrencyAndAmount();
    }

    /**
     * Create an instance of {@link GenericIdentification25 }
     * 
     */
    public GenericIdentification25 createGenericIdentification25() {
        return new GenericIdentification25();
    }

    /**
     * Create an instance of {@link SupplementaryDataEnvelope1 }
     * 
     */
    public SupplementaryDataEnvelope1 createSupplementaryDataEnvelope1() {
        return new SupplementaryDataEnvelope1();
    }

    /**
     * Create an instance of {@link CorporateActionEventType21Choice }
     * 
     */
    public CorporateActionEventType21Choice createCorporateActionEventType21Choice() {
        return new CorporateActionEventType21Choice();
    }

    /**
     * Create an instance of {@link Pagination }
     * 
     */
    public Pagination createPagination() {
        return new Pagination();
    }

    /**
     * Create an instance of {@link PriceFormat27Choice }
     * 
     */
    public PriceFormat27Choice createPriceFormat27Choice() {
        return new PriceFormat27Choice();
    }

    /**
     * Create an instance of {@link DateCodeAndTimeFormat2 }
     * 
     */
    public DateCodeAndTimeFormat2 createDateCodeAndTimeFormat2() {
        return new DateCodeAndTimeFormat2();
    }

    /**
     * Create an instance of {@link FinancialInstrumentQuantity22Choice }
     * 
     */
    public FinancialInstrumentQuantity22Choice createFinancialInstrumentQuantity22Choice() {
        return new FinancialInstrumentQuantity22Choice();
    }

    /**
     * Create an instance of {@link SecurityIdentification15 }
     * 
     */
    public SecurityIdentification15 createSecurityIdentification15() {
        return new SecurityIdentification15();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus9 }
     * 
     */
    public RateTypeAndAmountAndStatus9 createRateTypeAndAmountAndStatus9() {
        return new RateTypeAndAmountAndStatus9();
    }

    /**
     * Create an instance of {@link DateCode13Choice }
     * 
     */
    public DateCode13Choice createDateCode13Choice() {
        return new DateCode13Choice();
    }

    /**
     * Create an instance of {@link RateTypeAndAmountAndStatus7 }
     * 
     */
    public RateTypeAndAmountAndStatus7 createRateTypeAndAmountAndStatus7() {
        return new RateTypeAndAmountAndStatus7();
    }

    /**
     * Create an instance of {@link InterestComputationMethodFormat2Choice }
     * 
     */
    public InterestComputationMethodFormat2Choice createInterestComputationMethodFormat2Choice() {
        return new InterestComputationMethodFormat2Choice();
    }

    /**
     * Create an instance of {@link CorporateActionAmounts26 }
     * 
     */
    public CorporateActionAmounts26 createCorporateActionAmounts26() {
        return new CorporateActionAmounts26();
    }

    /**
     * Create an instance of {@link DividendTypeFormat4Choice }
     * 
     */
    public DividendTypeFormat4Choice createDividendTypeFormat4Choice() {
        return new DividendTypeFormat4Choice();
    }

    /**
     * Create an instance of {@link FractionDispositionType20Choice }
     * 
     */
    public FractionDispositionType20Choice createFractionDispositionType20Choice() {
        return new FractionDispositionType20Choice();
    }

    /**
     * Create an instance of {@link CorporateActionOption56 }
     * 
     */
    public CorporateActionOption56 createCorporateActionOption56() {
        return new CorporateActionOption56();
    }

    /**
     * Create an instance of {@link ElectionTypeFormat2Choice }
     * 
     */
    public ElectionTypeFormat2Choice createElectionTypeFormat2Choice() {
        return new ElectionTypeFormat2Choice();
    }

    /**
     * Create an instance of {@link CashAccountIdentification6Choice }
     * 
     */
    public CashAccountIdentification6Choice createCashAccountIdentification6Choice() {
        return new CashAccountIdentification6Choice();
    }

    /**
     * Create an instance of {@link SecurityDate10 }
     * 
     */
    public SecurityDate10 createSecurityDate10() {
        return new SecurityDate10();
    }

    /**
     * Create an instance of {@link DocumentIdentification20 }
     * 
     */
    public DocumentIdentification20 createDocumentIdentification20() {
        return new DocumentIdentification20();
    }

    /**
     * Create an instance of {@link CorporateActionDate28 }
     * 
     */
    public CorporateActionDate28 createCorporateActionDate28() {
        return new CorporateActionDate28();
    }

    /**
     * Create an instance of {@link PriceFormat25Choice }
     * 
     */
    public PriceFormat25Choice createPriceFormat25Choice() {
        return new PriceFormat25Choice();
    }

    /**
     * Create an instance of {@link PartyIdentification57Choice }
     * 
     */
    public PartyIdentification57Choice createPartyIdentification57Choice() {
        return new PartyIdentification57Choice();
    }

    /**
     * Create an instance of {@link PriceFormat31Choice }
     * 
     */
    public PriceFormat31Choice createPriceFormat31Choice() {
        return new PriceFormat31Choice();
    }

    /**
     * Create an instance of {@link CorporateActionDate26 }
     * 
     */
    public CorporateActionDate26 createCorporateActionDate26() {
        return new CorporateActionDate26();
    }

    /**
     * Create an instance of {@link CorporateActionPeriod8 }
     * 
     */
    public CorporateActionPeriod8 createCorporateActionPeriod8() {
        return new CorporateActionPeriod8();
    }

    /**
     * Create an instance of {@link CorporateActionPeriod7 }
     * 
     */
    public CorporateActionPeriod7 createCorporateActionPeriod7() {
        return new CorporateActionPeriod7();
    }

    /**
     * Create an instance of {@link FinancialInstrumentQuantity15Choice }
     * 
     */
    public FinancialInstrumentQuantity15Choice createFinancialInstrumentQuantity15Choice() {
        return new FinancialInstrumentQuantity15Choice();
    }

    /**
     * Create an instance of {@link MarketIdentification4Choice }
     * 
     */
    public MarketIdentification4Choice createMarketIdentification4Choice() {
        return new MarketIdentification4Choice();
    }

    /**
     * Create an instance of {@link InterestRateUsedForPaymentFormat6Choice }
     * 
     */
    public InterestRateUsedForPaymentFormat6Choice createInterestRateUsedForPaymentFormat6Choice() {
        return new InterestRateUsedForPaymentFormat6Choice();
    }

    /**
     * Create an instance of {@link PriceFormat26Choice }
     * 
     */
    public PriceFormat26Choice createPriceFormat26Choice() {
        return new PriceFormat26Choice();
    }

    /**
     * Create an instance of {@link CorporateActionDate16 }
     * 
     */
    public CorporateActionDate16 createCorporateActionDate16() {
        return new CorporateActionDate16();
    }

    /**
     * Create an instance of {@link OptionStyle5Choice }
     * 
     */
    public OptionStyle5Choice createOptionStyle5Choice() {
        return new OptionStyle5Choice();
    }

    /**
     * Create an instance of {@link AmountToAmountRatio3 }
     * 
     */
    public AmountToAmountRatio3 createAmountToAmountRatio3() {
        return new AmountToAmountRatio3();
    }

    /**
     * Create an instance of {@link PercentagePrice1 }
     * 
     */
    public PercentagePrice1 createPercentagePrice1() {
        return new PercentagePrice1();
    }

    /**
     * Create an instance of {@link BalanceFormat4Choice }
     * 
     */
    public BalanceFormat4Choice createBalanceFormat4Choice() {
        return new BalanceFormat4Choice();
    }

    /**
     * Create an instance of {@link CertificationTypeFormat2Choice }
     * 
     */
    public CertificationTypeFormat2Choice createCertificationTypeFormat2Choice() {
        return new CertificationTypeFormat2Choice();
    }

    /**
     * Create an instance of {@link FractionDispositionType16Choice }
     * 
     */
    public FractionDispositionType16Choice createFractionDispositionType16Choice() {
        return new FractionDispositionType16Choice();
    }

    /**
     * Create an instance of {@link FinancialInstrumentQuantity21Choice }
     * 
     */
    public FinancialInstrumentQuantity21Choice createFinancialInstrumentQuantity21Choice() {
        return new FinancialInstrumentQuantity21Choice();
    }

    /**
     * Create an instance of {@link CashOption29 }
     * 
     */
    public CashOption29 createCashOption29() {
        return new CashOption29();
    }

    /**
     * Create an instance of {@link UpdatedURLlnformation1 }
     * 
     */
    public UpdatedURLlnformation1 createUpdatedURLlnformation1() {
        return new UpdatedURLlnformation1();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Document }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:swift:xsd:seev.031.002.04", name = "Document")
    public JAXBElement<Document> createDocument(Document value) {
        return new JAXBElement<Document>(_Document_QNAME, Document.class, null, value);
    }

}
