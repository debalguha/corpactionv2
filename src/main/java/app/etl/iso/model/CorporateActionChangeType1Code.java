
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionChangeType1Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateActionChangeType1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BERE"/>
 *     &lt;enumeration value="CERT"/>
 *     &lt;enumeration value="DEPH"/>
 *     &lt;enumeration value="GPPH"/>
 *     &lt;enumeration value="GTGP"/>
 *     &lt;enumeration value="GTPH"/>
 *     &lt;enumeration value="NAME"/>
 *     &lt;enumeration value="PHDE"/>
 *     &lt;enumeration value="REBE"/>
 *     &lt;enumeration value="TERM"/>
 *     &lt;enumeration value="DECI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateActionChangeType1Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum CorporateActionChangeType1Code {

    BERE,
    CERT,
    DEPH,
    GPPH,
    GTGP,
    GTPH,
    NAME,
    PHDE,
    REBE,
    TERM,
    DECI;

    public String value() {
        return name();
    }

    public static CorporateActionChangeType1Code fromValue(String v) {
        return valueOf(v);
    }

}
