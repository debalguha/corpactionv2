
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionGeneralInformation67 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateActionGeneralInformation67">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorpActnEvtId" type="{urn:swift:xsd:seev.031.002.04}RestrictedFINXMax16Text"/>
 *         &lt;element name="OffclCorpActnEvtId" type="{urn:swift:xsd:seev.031.002.04}RestrictedFINXMax16Text" minOccurs="0"/>
 *         &lt;element name="ClssActnNb" type="{urn:swift:xsd:seev.031.002.04}RestrictedFINXMax16Text" minOccurs="0"/>
 *         &lt;element name="EvtPrcgTp" type="{urn:swift:xsd:seev.031.002.04}CorporateActionEventProcessingType1Choice" minOccurs="0"/>
 *         &lt;element name="EvtTp" type="{urn:swift:xsd:seev.031.002.04}CorporateActionEventType21Choice"/>
 *         &lt;element name="MndtryVlntryEvtTp" type="{urn:swift:xsd:seev.031.002.04}CorporateActionMandatoryVoluntary2Choice"/>
 *         &lt;element name="UndrlygScty" type="{urn:swift:xsd:seev.031.002.04}FinancialInstrumentAttributes45"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateActionGeneralInformation67", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "corpActnEvtId",
    "offclCorpActnEvtId",
    "clssActnNb",
    "evtPrcgTp",
    "evtTp",
    "mndtryVlntryEvtTp",
    "undrlygScty"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class CorporateActionGeneralInformation67 {

    @XmlElement(name = "CorpActnEvtId", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String corpActnEvtId;
    @XmlElement(name = "OffclCorpActnEvtId", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String offclCorpActnEvtId;
    @XmlElement(name = "ClssActnNb", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String clssActnNb;
    @XmlElement(name = "EvtPrcgTp", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected CorporateActionEventProcessingType1Choice evtPrcgTp;
    @XmlElement(name = "EvtTp", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected CorporateActionEventType21Choice evtTp;
    @XmlElement(name = "MndtryVlntryEvtTp", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected CorporateActionMandatoryVoluntary2Choice mndtryVlntryEvtTp;
    @XmlElement(name = "UndrlygScty", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FinancialInstrumentAttributes45 undrlygScty;

    /**
     * Gets the value of the corpActnEvtId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getCorpActnEvtId() {
        return corpActnEvtId;
    }

    /**
     * Sets the value of the corpActnEvtId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setCorpActnEvtId(String value) {
        this.corpActnEvtId = value;
    }

    /**
     * Gets the value of the offclCorpActnEvtId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getOffclCorpActnEvtId() {
        return offclCorpActnEvtId;
    }

    /**
     * Sets the value of the offclCorpActnEvtId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setOffclCorpActnEvtId(String value) {
        this.offclCorpActnEvtId = value;
    }

    /**
     * Gets the value of the clssActnNb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getClssActnNb() {
        return clssActnNb;
    }

    /**
     * Sets the value of the clssActnNb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setClssActnNb(String value) {
        this.clssActnNb = value;
    }

    /**
     * Gets the value of the evtPrcgTp property.
     * 
     * @return
     *     possible object is
     *     {@link CorporateActionEventProcessingType1Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public CorporateActionEventProcessingType1Choice getEvtPrcgTp() {
        return evtPrcgTp;
    }

    /**
     * Sets the value of the evtPrcgTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateActionEventProcessingType1Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEvtPrcgTp(CorporateActionEventProcessingType1Choice value) {
        this.evtPrcgTp = value;
    }

    /**
     * Gets the value of the evtTp property.
     * 
     * @return
     *     possible object is
     *     {@link CorporateActionEventType21Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public CorporateActionEventType21Choice getEvtTp() {
        return evtTp;
    }

    /**
     * Sets the value of the evtTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateActionEventType21Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEvtTp(CorporateActionEventType21Choice value) {
        this.evtTp = value;
    }

    /**
     * Gets the value of the mndtryVlntryEvtTp property.
     * 
     * @return
     *     possible object is
     *     {@link CorporateActionMandatoryVoluntary2Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public CorporateActionMandatoryVoluntary2Choice getMndtryVlntryEvtTp() {
        return mndtryVlntryEvtTp;
    }

    /**
     * Sets the value of the mndtryVlntryEvtTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateActionMandatoryVoluntary2Choice }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMndtryVlntryEvtTp(CorporateActionMandatoryVoluntary2Choice value) {
        this.mndtryVlntryEvtTp = value;
    }

    /**
     * Gets the value of the undrlygScty property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialInstrumentAttributes45 }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public FinancialInstrumentAttributes45 getUndrlygScty() {
        return undrlygScty;
    }

    /**
     * Sets the value of the undrlygScty property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialInstrumentAttributes45 }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setUndrlygScty(FinancialInstrumentAttributes45 value) {
        this.undrlygScty = value;
    }

}
