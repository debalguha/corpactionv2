
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionTaxableIncomePerShareCalculated1Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateActionTaxableIncomePerShareCalculated1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TDIY"/>
 *     &lt;enumeration value="TDIN"/>
 *     &lt;enumeration value="UKWN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateActionTaxableIncomePerShareCalculated1Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum CorporateActionTaxableIncomePerShareCalculated1Code {

    TDIY,
    TDIN,
    UKWN;

    public String value() {
        return name();
    }

    public static CorporateActionTaxableIncomePerShareCalculated1Code fromValue(String v) {
        return valueOf(v);
    }

}
