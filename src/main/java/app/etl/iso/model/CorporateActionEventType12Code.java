
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionEventType12Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateActionEventType12Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACCU"/>
 *     &lt;enumeration value="ACTV"/>
 *     &lt;enumeration value="INFO"/>
 *     &lt;enumeration value="ATTI"/>
 *     &lt;enumeration value="BRUP"/>
 *     &lt;enumeration value="DFLT"/>
 *     &lt;enumeration value="BONU"/>
 *     &lt;enumeration value="EXRI"/>
 *     &lt;enumeration value="CAPD"/>
 *     &lt;enumeration value="CAPG"/>
 *     &lt;enumeration value="CAPI"/>
 *     &lt;enumeration value="DRCA"/>
 *     &lt;enumeration value="DVCA"/>
 *     &lt;enumeration value="CHAN"/>
 *     &lt;enumeration value="CLSA"/>
 *     &lt;enumeration value="COOP"/>
 *     &lt;enumeration value="CONS"/>
 *     &lt;enumeration value="CONV"/>
 *     &lt;enumeration value="CREV"/>
 *     &lt;enumeration value="DECR"/>
 *     &lt;enumeration value="DETI"/>
 *     &lt;enumeration value="DSCL"/>
 *     &lt;enumeration value="DVOP"/>
 *     &lt;enumeration value="DRIP"/>
 *     &lt;enumeration value="DRAW"/>
 *     &lt;enumeration value="DTCH"/>
 *     &lt;enumeration value="EXOF"/>
 *     &lt;enumeration value="REDM"/>
 *     &lt;enumeration value="MCAL"/>
 *     &lt;enumeration value="INCR"/>
 *     &lt;enumeration value="PPMT"/>
 *     &lt;enumeration value="INTR"/>
 *     &lt;enumeration value="PRII"/>
 *     &lt;enumeration value="RHDI"/>
 *     &lt;enumeration value="LIQU"/>
 *     &lt;enumeration value="EXTM"/>
 *     &lt;enumeration value="MRGR"/>
 *     &lt;enumeration value="NOOF"/>
 *     &lt;enumeration value="CERT"/>
 *     &lt;enumeration value="ODLT"/>
 *     &lt;enumeration value="OTHR"/>
 *     &lt;enumeration value="PARI"/>
 *     &lt;enumeration value="PCAL"/>
 *     &lt;enumeration value="PRED"/>
 *     &lt;enumeration value="PINK"/>
 *     &lt;enumeration value="PLAC"/>
 *     &lt;enumeration value="PDEF"/>
 *     &lt;enumeration value="PRIO"/>
 *     &lt;enumeration value="REDO"/>
 *     &lt;enumeration value="REMK"/>
 *     &lt;enumeration value="BIDS"/>
 *     &lt;enumeration value="SPLR"/>
 *     &lt;enumeration value="RHTS"/>
 *     &lt;enumeration value="DVSC"/>
 *     &lt;enumeration value="SHPR"/>
 *     &lt;enumeration value="SMAL"/>
 *     &lt;enumeration value="SOFF"/>
 *     &lt;enumeration value="DVSE"/>
 *     &lt;enumeration value="SPLF"/>
 *     &lt;enumeration value="TREC"/>
 *     &lt;enumeration value="TEND"/>
 *     &lt;enumeration value="DLST"/>
 *     &lt;enumeration value="SUSP"/>
 *     &lt;enumeration value="EXWA"/>
 *     &lt;enumeration value="WTRC"/>
 *     &lt;enumeration value="WRTH"/>
 *     &lt;enumeration value="BPUT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateActionEventType12Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum CorporateActionEventType12Code {

    ACCU,
    ACTV,
    INFO,
    ATTI,
    BRUP,
    DFLT,
    BONU,
    EXRI,
    CAPD,
    CAPG,
    CAPI,
    DRCA,
    DVCA,
    CHAN,
    CLSA,
    COOP,
    CONS,
    CONV,
    CREV,
    DECR,
    DETI,
    DSCL,
    DVOP,
    DRIP,
    DRAW,
    DTCH,
    EXOF,
    REDM,
    MCAL,
    INCR,
    PPMT,
    INTR,
    PRII,
    RHDI,
    LIQU,
    EXTM,
    MRGR,
    NOOF,
    CERT,
    ODLT,
    OTHR,
    PARI,
    PCAL,
    PRED,
    PINK,
    PLAC,
    PDEF,
    PRIO,
    REDO,
    REMK,
    BIDS,
    SPLR,
    RHTS,
    DVSC,
    SHPR,
    SMAL,
    SOFF,
    DVSE,
    SPLF,
    TREC,
    TEND,
    DLST,
    SUSP,
    EXWA,
    WTRC,
    WRTH,
    BPUT;

    public String value() {
        return name();
    }

    public static CorporateActionEventType12Code fromValue(String v) {
        return valueOf(v);
    }

}
