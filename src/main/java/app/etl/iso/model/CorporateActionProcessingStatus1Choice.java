
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionProcessingStatus1Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateActionProcessingStatus1Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="EvtSts" type="{urn:swift:xsd:seev.031.002.04}CorporateActionEventStatus1"/>
 *         &lt;element name="ForInfOnlyInd" type="{urn:swift:xsd:seev.031.002.04}YesNoIndicator"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateActionProcessingStatus1Choice", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "evtSts",
    "forInfOnlyInd"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class CorporateActionProcessingStatus1Choice {

    @XmlElement(name = "EvtSts", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected CorporateActionEventStatus1 evtSts;
    @XmlElement(name = "ForInfOnlyInd", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Boolean forInfOnlyInd;

    /**
     * Gets the value of the evtSts property.
     * 
     * @return
     *     possible object is
     *     {@link CorporateActionEventStatus1 }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public CorporateActionEventStatus1 getEvtSts() {
        return evtSts;
    }

    /**
     * Sets the value of the evtSts property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateActionEventStatus1 }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEvtSts(CorporateActionEventStatus1 value) {
        this.evtSts = value;
    }

    /**
     * Gets the value of the forInfOnlyInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Boolean isForInfOnlyInd() {
        return forInfOnlyInd;
    }

    /**
     * Sets the value of the forInfOnlyInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setForInfOnlyInd(Boolean value) {
        this.forInfOnlyInd = value;
    }

}
