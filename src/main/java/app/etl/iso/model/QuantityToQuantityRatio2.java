
package app.etl.iso.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuantityToQuantityRatio2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuantityToQuantityRatio2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Qty1" type="{urn:swift:xsd:seev.031.002.04}RestrictedFINDecimalNumber"/>
 *         &lt;element name="Qty2" type="{urn:swift:xsd:seev.031.002.04}RestrictedFINDecimalNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuantityToQuantityRatio2", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "qty1",
    "qty2"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class QuantityToQuantityRatio2 {

    @XmlElement(name = "Qty1", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BigDecimal qty1;
    @XmlElement(name = "Qty2", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BigDecimal qty2;

    /**
     * Gets the value of the qty1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public BigDecimal getQty1() {
        return qty1;
    }

    /**
     * Sets the value of the qty1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setQty1(BigDecimal value) {
        this.qty1 = value;
    }

    /**
     * Gets the value of the qty2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public BigDecimal getQty2() {
        return qty2;
    }

    /**
     * Sets the value of the qty2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setQty2(BigDecimal value) {
        this.qty2 = value;
    }

}
