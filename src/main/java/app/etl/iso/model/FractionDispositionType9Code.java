
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FractionDispositionType9Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FractionDispositionType9Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DIST"/>
 *     &lt;enumeration value="RDDN"/>
 *     &lt;enumeration value="STAN"/>
 *     &lt;enumeration value="RDUP"/>
 *     &lt;enumeration value="UKWN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FractionDispositionType9Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum FractionDispositionType9Code {

    DIST,
    RDDN,
    STAN,
    RDUP,
    UKWN;

    public String value() {
        return name();
    }

    public static FractionDispositionType9Code fromValue(String v) {
        return valueOf(v);
    }

}
