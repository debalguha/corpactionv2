
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateActionFrequencyType2Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateActionFrequencyType2Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FINL"/>
 *     &lt;enumeration value="INTE"/>
 *     &lt;enumeration value="REGR"/>
 *     &lt;enumeration value="SPEC"/>
 *     &lt;enumeration value="REIN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateActionFrequencyType2Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum CorporateActionFrequencyType2Code {

    FINL,
    INTE,
    REGR,
    SPEC,
    REIN;

    public String value() {
        return name();
    }

    public static CorporateActionFrequencyType2Code fromValue(String v) {
        return valueOf(v);
    }

}
