
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NetDividendRateType3Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NetDividendRateType3Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CAPO"/>
 *     &lt;enumeration value="FLFR"/>
 *     &lt;enumeration value="INCO"/>
 *     &lt;enumeration value="INTR"/>
 *     &lt;enumeration value="REES"/>
 *     &lt;enumeration value="SOIC"/>
 *     &lt;enumeration value="TXBL"/>
 *     &lt;enumeration value="TXDF"/>
 *     &lt;enumeration value="TXFR"/>
 *     &lt;enumeration value="UNFR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NetDividendRateType3Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum NetDividendRateType3Code {

    CAPO,
    FLFR,
    INCO,
    INTR,
    REES,
    SOIC,
    TXBL,
    TXDF,
    TXFR,
    UNFR;

    public String value() {
        return name();
    }

    public static NetDividendRateType3Code fromValue(String v) {
        return valueOf(v);
    }

}
