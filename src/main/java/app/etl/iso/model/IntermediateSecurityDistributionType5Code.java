
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntermediateSecurityDistributionType5Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IntermediateSecurityDistributionType5Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BIDS"/>
 *     &lt;enumeration value="DRIP"/>
 *     &lt;enumeration value="DVCA"/>
 *     &lt;enumeration value="DVOP"/>
 *     &lt;enumeration value="EXRI"/>
 *     &lt;enumeration value="PRIO"/>
 *     &lt;enumeration value="DVSC"/>
 *     &lt;enumeration value="DVSE"/>
 *     &lt;enumeration value="INTR"/>
 *     &lt;enumeration value="LIQU"/>
 *     &lt;enumeration value="SOFF"/>
 *     &lt;enumeration value="SPLF"/>
 *     &lt;enumeration value="BONU"/>
 *     &lt;enumeration value="EXOF"/>
 *     &lt;enumeration value="MRGR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IntermediateSecurityDistributionType5Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum IntermediateSecurityDistributionType5Code {

    BIDS,
    DRIP,
    DVCA,
    DVOP,
    EXRI,
    PRIO,
    DVSC,
    DVSE,
    INTR,
    LIQU,
    SOFF,
    SPLF,
    BONU,
    EXOF,
    MRGR;

    public String value() {
        return name();
    }

    public static IntermediateSecurityDistributionType5Code fromValue(String v) {
        return valueOf(v);
    }

}
