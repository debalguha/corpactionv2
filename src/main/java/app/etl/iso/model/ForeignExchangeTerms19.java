
package app.etl.iso.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ForeignExchangeTerms19 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForeignExchangeTerms19">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UnitCcy" type="{urn:swift:xsd:seev.031.002.04}ActiveCurrencyCode"/>
 *         &lt;element name="QtdCcy" type="{urn:swift:xsd:seev.031.002.04}ActiveCurrencyCode"/>
 *         &lt;element name="XchgRate" type="{urn:swift:xsd:seev.031.002.04}BaseOneRate"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForeignExchangeTerms19", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "unitCcy",
    "qtdCcy",
    "xchgRate"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class ForeignExchangeTerms19 {

    @XmlElement(name = "UnitCcy", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String unitCcy;
    @XmlElement(name = "QtdCcy", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String qtdCcy;
    @XmlElement(name = "XchgRate", namespace = "urn:swift:xsd:seev.031.002.04", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BigDecimal xchgRate;

    /**
     * Gets the value of the unitCcy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getUnitCcy() {
        return unitCcy;
    }

    /**
     * Sets the value of the unitCcy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setUnitCcy(String value) {
        this.unitCcy = value;
    }

    /**
     * Gets the value of the qtdCcy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getQtdCcy() {
        return qtdCcy;
    }

    /**
     * Sets the value of the qtdCcy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setQtdCcy(String value) {
        this.qtdCcy = value;
    }

    /**
     * Gets the value of the xchgRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public BigDecimal getXchgRate() {
        return xchgRate;
    }

    /**
     * Sets the value of the xchgRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setXchgRate(BigDecimal value) {
        this.xchgRate = value;
    }

}
