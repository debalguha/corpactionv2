
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyIdentification51Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyIdentification51Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="AnyBIC" type="{urn:swift:xsd:seev.031.002.04}AnyBICIdentifier"/>
 *         &lt;element name="PrtryId" type="{urn:swift:xsd:seev.031.002.04}GenericIdentification24"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyIdentification51Choice", namespace = "urn:swift:xsd:seev.031.002.04", propOrder = {
    "anyBIC",
    "prtryId"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class PartyIdentification51Choice {

    @XmlElement(name = "AnyBIC", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String anyBIC;
    @XmlElement(name = "PrtryId", namespace = "urn:swift:xsd:seev.031.002.04")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected GenericIdentification24 prtryId;

    /**
     * Gets the value of the anyBIC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getAnyBIC() {
        return anyBIC;
    }

    /**
     * Sets the value of the anyBIC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAnyBIC(String value) {
        this.anyBIC = value;
    }

    /**
     * Gets the value of the prtryId property.
     * 
     * @return
     *     possible object is
     *     {@link GenericIdentification24 }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public GenericIdentification24 getPrtryId() {
        return prtryId;
    }

    /**
     * Sets the value of the prtryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericIdentification24 }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setPrtryId(GenericIdentification24 value) {
        this.prtryId = value;
    }

}
