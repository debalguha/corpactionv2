
package app.etl.iso.model;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OptionFeatures4Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OptionFeatures4Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CAOS"/>
 *     &lt;enumeration value="COND"/>
 *     &lt;enumeration value="MAXC"/>
 *     &lt;enumeration value="MAXS"/>
 *     &lt;enumeration value="OPLF"/>
 *     &lt;enumeration value="PROR"/>
 *     &lt;enumeration value="QOVE"/>
 *     &lt;enumeration value="QREC"/>
 *     &lt;enumeration value="VVPR"/>
 *     &lt;enumeration value="NOSE"/>
 *     &lt;enumeration value="PINS"/>
 *     &lt;enumeration value="ASVO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OptionFeatures4Code", namespace = "urn:swift:xsd:seev.031.002.04")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-02-05T06:48:08+00:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum OptionFeatures4Code {

    CAOS,
    COND,
    MAXC,
    MAXS,
    OPLF,
    PROR,
    QOVE,
    QREC,
    VVPR,
    NOSE,
    PINS,
    ASVO;

    public String value() {
        return name();
    }

    public static OptionFeatures4Code fromValue(String v) {
        return valueOf(v);
    }

}
