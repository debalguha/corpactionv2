package app.etl.iso15022.util;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import biz.c24.io.api.data.ComplexDataObject;
import biz.c24.io.swift2016.*;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathNotFoundException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.commons.compiler.CompilerFactoryFactory;
import org.codehaus.commons.compiler.IScriptEvaluator;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utility {
    private static final Logger LOG = LoggerFactory.getLogger(Utility.class);
    private static final Map<String, String> secIdentifierCodeMap = new HashMap<>();
    static{
        secIdentifierCodeMap.put("TS", "TIKR");secIdentifierCodeMap.put("CH", "VALO");
        secIdentifierCodeMap.put("XS", "COMM");secIdentifierCodeMap.put("DE", "WKNR");
        secIdentifierCodeMap.put("BE", "BELC");secIdentifierCodeMap.put("BLOM", "BLOM");
        secIdentifierCodeMap.put("US", "CUSP");secIdentifierCodeMap.put("CMED", "CMED");
        secIdentifierCodeMap.put("GB", "SEDL");secIdentifierCodeMap.put("CTAC", "CTAC");
        secIdentifierCodeMap.put("JP", "SICC");secIdentifierCodeMap.put("ISDU", "ISDU");
        secIdentifierCodeMap.put("ISDX", "ISDX");secIdentifierCodeMap.put("LCHD", "LCHD");
        secIdentifierCodeMap.put("OCCS", "OCCS");secIdentifierCodeMap.put("OPRA", "OPRA");
        secIdentifierCodeMap.put("RCMD", "RCMD");secIdentifierCodeMap.put("RICC", "RICC");
        secIdentifierCodeMap.put("XX", "XX");secIdentifierCodeMap.put("RICC", "RICC");
    }
    public static Object executeJaninoScriptAndReturnResult(MT564Message swiftMessage, String janinoScript) throws Exception{
        IScriptEvaluator evaluator = CompilerFactoryFactory.getDefaultCompilerFactory().newScriptEvaluator();
        evaluator.setDefaultImports(new String[]{"biz.c24.io.swift2016.*", "org.apache.commons.lang3.StringUtils", "org.joda.time.DateTime", "java.util.Objects", "java.util.Optional"
                , "org.joda.time.format.DateTimeFormat", "org.joda.time.format.ISODateTimeFormat", "java.util.Date", "app.etl.iso15022.util.Utility"});
        evaluator.setReturnType(Object.class);
        evaluator.setParameters(new String[]{"swiftMessage"}, new Class[]{MT564Message.class});
        evaluator.cook(janinoScript);
        try{
            return evaluator.evaluate(new Object[]{swiftMessage});
        } catch(Exception e){LOG.trace("Janino error", e);return null;}
    }
    public static String buildIntrmdtSctyBalance(MT564Message swiftMessage, String tag, String qualifier){
        if(Objects.isNull(swiftMessage.getBlock4()) || Objects.isNull(swiftMessage.getBlock4().getSeqC()))
            return "";
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("<p:%s>", tag));
        try {
            for (Field93aType9 field93aType9 : swiftMessage.getBlock4().getSeqC().getField93aBalance()) {
                if(!Objects.isNull(field93aType9.getB()) && field93aType9.getB().getQualifier().equals(qualifier))
                    builder.append(buildBalanceXml(field93aType9.getB()));
                else if(!Objects.isNull(field93aType9.getC()) && field93aType9.getC().getQualifier().equals(qualifier)){
                    if(field93aType9.getC().getBalanceTypeCode().equals("ELIG"))
                        buildElgblBalanceXml(field93aType9.getC(), "ElgblBal");
                    else
                        buildElgblBalanceXml(field93aType9.getC(), "NotElgblBal");
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        builder.append(String.format("</p:%s>", tag));
        return builder.toString();
    }
    public static String buildBalanceXml(Field93aBalanceOptionB field93aBalanceOptionB){
        StringBuilder builder = new StringBuilder();
        builder.append("<p:Bal>");
        if(StringUtils.isEmpty(field93aBalanceOptionB.getSignedBalance().getSign()))
            builder.append(String.format("<p:ShrtLngPos>LONG</p:ShrtLngPos>"));
        else
            builder.append(String.format("<p:ShrtLngPos>SHOR</p:ShrtLngPos>"));
        if(StringUtils.isEmpty(field93aBalanceOptionB.getDataSourceScheme()))
            builder.append(buildQuantityXml(field93aBalanceOptionB));
        else
            builder.append(buildPrtyQuantityXml(field93aBalanceOptionB));
        builder.append("</p:Bal>");
        return builder.toString();
    }
    public static String buildElgblBalanceXml(Field93aBalanceOptionC field93aBalanceOptionC, String tag){
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("<p:%s>", tag));
        if(StringUtils.isEmpty(field93aBalanceOptionC.getSignedBalance().getSign()))
            builder.append(String.format("<p:ShrtLngPos>LONG</p:ShrtLngPos>"));
        else
            builder.append(String.format("<p:ShrtLngPos>SHOR</p:ShrtLngPos>"));
        builder.append(buildQuantityXml(field93aBalanceOptionC));
        builder.append(String.format("</p:%s>", tag));
        return builder.toString();
    }
    public static String buildQuantityXml(Field93aBalanceOptionB field93aBalanceOptionB){
        if(field93aBalanceOptionB.equals("UNIT"))
            return String.format("<p:Unit>%s</p:Unit>", field93aBalanceOptionB.getSignedBalance().getBalance().toString());
        else if(field93aBalanceOptionB.getQuantityTypeCode().equals("FAMT"))
            return String.format("<p:FaceAmt>%s</p:FaceAmt>", field93aBalanceOptionB.getSignedBalance().getBalance().toString());
        else if(field93aBalanceOptionB.getQuantityTypeCode().equals("AMOR"))
            return String.format("<p:AmtsdVal>%s</p:AmtsdVal>", field93aBalanceOptionB.getSignedBalance().getBalance().toString());
        return null;
    }
    public static String buildQuantityXml(Field93aBalanceOptionC field93aBalanceOptionC){
        if(field93aBalanceOptionC.equals("UNIT"))
            return String.format("<p:Unit>%s</p:Unit>", field93aBalanceOptionC.getSignedBalance().getBalance().toString());
        else if(field93aBalanceOptionC.getQuantityTypeCode().equals("FAMT"))
            return String.format("<p:FaceAmt>%s</p:FaceAmt>", field93aBalanceOptionC.getSignedBalance().getBalance().toString());
        else if(field93aBalanceOptionC.getQuantityTypeCode().equals("AMOR"))
            return String.format("<p:AmtsdVal>%s</p:AmtsdVal>", field93aBalanceOptionC.getSignedBalance().getBalance().toString());
        return null;
    }
    public static String buildPrtyQuantityXml(Field93aBalanceOptionB field93aBalanceOptionB){
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("<p:Qty>%s</p:Qty>", field93aBalanceOptionB.getSignedBalance().getBalance().toString()));
        builder.append(String.format("<p:QtyTp>str%sing</p:QtyTp>", field93aBalanceOptionB.getQuantityTypeCode()));
        builder.append(buildIssuerAndQuantity(field93aBalanceOptionB.getDataSourceScheme()));
        return builder.toString();
    }
    public static String getDateFormat31ChoiceFrom(Object[] field98aType49s, String tag, String qualifier, String []possibleXpaths) throws Exception{
        Optional<Object> objOptional = (Optional<Object>)filterArrayWithQualifier(field98aType49s, possibleXpaths, qualifier);
        if(!objOptional.isPresent())
            return "";
        Object field98aTypeXX = objOptional.get();
        StringBuilder builder = new StringBuilder(String.format("<p:%s>", tag));
        Optional<Object> field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "a"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateFromField98aDateOptionA((Field98aDateOptionA)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "c"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateTimeFromField98aDateOptionC((Field98aDateOptionC)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "b"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateTimeFromField98aDateOptionB((Field98aDateOptionB)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "e"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateTimeFromField98aDateOptionE((Field98aDateOptionE)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        builder.append(String.format("</p:%s>", tag));
        return builder.toString();
    }
    public static String getDateFormat22ChoiceFrom(Object[] field98aType49s, String tag, String qualifier, String []possibleXpaths) throws Exception{
        Optional<Object> objOptional = (Optional<Object>)filterArrayWithQualifier(field98aType49s, possibleXpaths, qualifier);
        if(!objOptional.isPresent())
            return "";
        Object field98aTypeXX = objOptional.get();
        StringBuilder builder = new StringBuilder(String.format("<p:%s>", tag));
        Optional<Object> field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "a"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateFromField98aDateOptionA((Field98aDateOptionA)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "c"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateTimeFromField98aDateOptionC((Field98aDateOptionC)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "b"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateTimeFromField98aDateOptionB((Field98aDateOptionB)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "e"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateTimeFromField98aDateOptionE((Field98aDateOptionE)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        field98ADateOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field98aTypeXX, "f"));
        if(field98ADateOptionX.isPresent())
            return builder.append(buildDateTimeFromField98aDateOptionF((Field98aDateOptionF)field98ADateOptionX.get())).append(String.format("</p:%s>", tag)).toString();
        builder.append(String.format("</p:%s>", tag));
        return builder.toString();
    }

    private static String buildDateTimeFromField98aDateOptionF(Field98aDateOptionF field98aDateOptionF) throws Exception {
        StringBuilder builder = new StringBuilder("<p:DtCdAndTm>");
        builder.append(buildDateTimeFromField98aDateOptionB(field98aDateOptionF));
        builder.append("<p:Tm>").append(ISODateTimeFormat.time().print(new DateTime(field98aDateOptionF.getTimeHHMMSS()))).append("</p:Tm>");
        builder.append("</p:DtCdAndTm>");
        return builder.toString();
    }
    private static String buildDateTimeFromField98aDateOptionB(ComplexDataObject field98aDateOptionB) throws Exception{
        StringBuilder builder = new StringBuilder("<p:DtCd>");
        if(StringUtils.isEmpty((String)PropertyUtils.getSimpleProperty(field98aDateOptionB, "dataSourceScheme"))){
            builder.append("<p:Cd>").append(PropertyUtils.getSimpleProperty(field98aDateOptionB, "dateCode")).append("</p:Cd>");
        }else{
            builder.append("<p:Id>").append(PropertyUtils.getSimpleProperty(field98aDateOptionB, "dateCode")).append("</p:Id>");
            builder.append(buildIssuerAndQuantity((String)PropertyUtils.getSimpleProperty(field98aDateOptionB, "dataSourceScheme")));
        }
        builder.append("</p:DtCd>");
        return builder.toString();
    }

    private static String buildDateTimeFromField98aDateOptionC(Field98aDateOptionC field98aDateOptionC) {
        return new StringBuilder("<p:Dt>").append("<p:DtTm>").append(makeISODateTimeFromDateAndTime(field98aDateOptionC.getDateYYYYMMDD(), field98aDateOptionC.getTimeHHMMSS()))
                .append("</p:DtTm>").append("</p:Dt>").toString();
    }

    private static String makeISODateTimeFromDateAndTime(Date datePart, Date timePart){
        DateTime timePartISO = new DateTime(timePart);
        return ISODateTimeFormat.dateTime().print(new DateTime(datePart).withHourOfDay(timePartISO.getHourOfDay()).withMinuteOfHour(timePartISO.getMinuteOfHour()).withSecondOfMinute(timePartISO.getSecondOfMinute()));
    }

    private static String buildDateFromField98aDateOptionA(Field98aDateOptionA field98aDateOptionA) throws Exception{
        //dateYYYYMMDD
        //return new StringBuilder("<p:Dt>").append("<p:Dt>").append(ISODateTimeFormat.date().print(new DateTime(field98aDateOptionA.getDateYYYYMMDD()))).append("</p:Dt>").append("</p:Dt>").toString();
        return new StringBuilder("<p:Dt>").append("<p:Dt>").append(ISODateTimeFormat.date().print(new DateTime(PropertyUtils.getSimpleProperty(field98aDateOptionA, "dateYYYYMMDD")))).append("</p:Dt>").append("</p:Dt>").toString();
    }
    private static String buildDateTimeFromField98aDateOptionE(Field98aDateOptionE field98aDateOptionE) {
        if(Objects.nonNull(field98aDateOptionE.getDecimals()))
            return new StringBuilder("<p:Dt>").append("<p:DtTm>").append(makeISODateTimeFromDateAndTime(field98aDateOptionE.getDateYYYYMMDD(), field98aDateOptionE.getTimeHHMMSS()))
                .append(field98aDateOptionE.getDecimals()).append(field98aDateOptionE.getUTCIndicator().getSign())
                .append(field98aDateOptionE.getUTCIndicator().getUTCOffsetPart1()).append(":").append(field98aDateOptionE.getUTCIndicator().getUTCOffsetPart2())
                .append("</p:DtTm>").append("</p:Dt>").toString();
        else
            return new StringBuilder("<p:Dt>").append("<p:DtTm>").append(makeISODateTimeFromDateAndTime(field98aDateOptionE.getDateYYYYMMDD(), field98aDateOptionE.getTimeHHMMSS()))
                    .append(field98aDateOptionE.getUTCIndicator().getSign()).append(field98aDateOptionE.getUTCIndicator()
                            .getUTCOffsetPart1()).append(":").append(field98aDateOptionE.getUTCIndicator().getUTCOffsetPart2())
                    .append("</p:DtTm>").append("</p:Dt>").toString();
    }

    public static String buildIssuerAndQuantity(String dataSourceScheme){
        String []result = new String[2];
        try {
            result[0] = dataSourceScheme.substring(0, 5);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("<p:Issr>%s</p:Issr>", dataSourceScheme.substring(0, 5)));
        if(dataSourceScheme.length() > 5)
            builder.append(String.format("<p:SchmeNm>string</p:SchmeNm>", dataSourceScheme.substring(5)));
        return builder.toString();
    }

    public static String buildPeriod3ChoiceMulti(Object[] field69aPeriod, String tag, String qualifier, String []possibleXpaths) throws Exception{
        Optional<Object> objOptional = (Optional<Object>)filterArrayWithQualifierForElements(field69aPeriod, possibleXpaths, qualifier);
        if(objOptional.isPresent()){
            Collection<Object> matchingElements = (Collection<Object>)objOptional.get();
            return matchingElements.stream().map(elmnt -> {
                try {
                    return buildPeriod3ChoiceFrom(elmnt, tag, possibleXpaths);
                } catch (Exception e) {throw new RuntimeException(e);}
            }).reduce((stra, strb) -> stra.concat(strb)).get();
        }
        return "";
    }

    public static String buildPeriod3Choice(Object[] field69aPeriod, String tag, String qualifier, String []possibleXpaths) throws Exception{
        Optional<Object> objOptional = (Optional<Object>)filterArrayWithQualifier(field69aPeriod, possibleXpaths, qualifier);
        if(!objOptional.isPresent())
            return "";
        return buildPeriod3ChoiceFrom(objOptional.get(), tag, possibleXpaths);
    }
    public static String buildPeriod3ChoiceFrom(Object field69aType4, String tag, String []possibleXpaths) throws Exception{
        StringBuilder builder = new StringBuilder(String.format("<p:%s>", tag));
        if(Objects.nonNull(field69aType4)){
            Optional<Object> field69aPeriodOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field69aType4, "a"));
            if(field69aPeriodOptionX.isPresent())
                builder.append(buildPeriod4FromField69aPeriodOptionA(field69aPeriodOptionX.get()));
            field69aPeriodOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field69aType4, "b"));
            if(field69aPeriodOptionX.isPresent())
                builder.append(buildPeriod4FromField69aPeriodOptionB(field69aPeriodOptionX.get()));
            field69aPeriodOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field69aType4, "c"));
            if(field69aPeriodOptionX.isPresent())
                builder.append(buildPeriod4FromField69aPeriodOptionC(field69aPeriodOptionX.get()));
            field69aPeriodOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field69aType4, "d"));
            if(field69aPeriodOptionX.isPresent())
                builder.append(buildPeriod4FromField69aPeriodOptionD(field69aPeriodOptionX.get()));
            field69aPeriodOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field69aType4, "e"));
            if(field69aPeriodOptionX.isPresent())
                builder.append(buildPeriod4FromField69aPeriodOptionE(field69aPeriodOptionX.get()));
            field69aPeriodOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field69aType4, "f"));
            if(field69aPeriodOptionX.isPresent())
                builder.append(buildPeriod4FromField69aPeriodOptionF(field69aPeriodOptionX.get()));
            field69aPeriodOptionX = Optional.ofNullable(PropertyUtils.getSimpleProperty(field69aType4, "j"));
            if(field69aPeriodOptionX.isPresent())
                builder.append(buildPeriod4FromField69aPeriodOptionJ(field69aPeriodOptionX.get()));
        }
        builder.append(String.format("</p:%s>", tag));
        return builder.toString();
    }

    public static String buildPeriod4FromField69aPeriodOptionA(Object field69aPeriodOptionA) throws Exception{
        return new StringBuilder("<p:Prd><p:StartDt><p:Dt><p:Dt>").append(ISODateTimeFormat.date().print(new DateTime(PropertyUtils.getProperty(field69aPeriodOptionA, "dateYYYYMMDD")))).append("</p:Dt></p:Dt></p:StartDt>")
                .append("<p:EndDt><p:Dt><p:Dt>").append(ISODateTimeFormat.date().print(new DateTime(PropertyUtils.getProperty(field69aPeriodOptionA, "dateYYYYMMDD1")))).append("</p:Dt></p:Dt></p:EndDt></p:Prd>").toString();
    }
    public static String buildPeriod4FromField69aPeriodOptionB(Object field69aPeriodOptionB) throws Exception {
        return new StringBuilder("<p:StartDt><p:Dt><p:DtTm>").append(makeISODateTimeFromDateAndTime((Date) PropertyUtils.getProperty(field69aPeriodOptionB, "dateYYYYMMDD"), (Date)PropertyUtils.getProperty(field69aPeriodOptionB, "timeHHMMSS"))).append("</p:DtTm></p:Dt></p:StartDt>")
                .append("<p:EndDt><p:Dt><p:DtTm>").append(makeISODateTimeFromDateAndTime((Date) PropertyUtils.getProperty(field69aPeriodOptionB, "dateYYYYMMDD1"), (Date)PropertyUtils.getProperty(field69aPeriodOptionB, "timeHHMMSS1"))).append("</p:DtTm></p:Dt></p:EndDt></p:Prd>").toString();
    }
    public static String buildPeriod4FromField69aPeriodOptionC(Object field69aPeriodOptionC) throws Exception{
        return new StringBuilder("<p:StartDt><p:Dt><p:Dt>").append(ISODateTimeFormat.date().print(new DateTime(PropertyUtils.getProperty(field69aPeriodOptionC, "dateYYYYMMDD")))).append("</p:Dt><p/:Dt></p:StartDt>")
                .append("<p:EndDt><p:NotSpcfdDt>").append(PropertyUtils.getProperty(field69aPeriodOptionC, "dateCode")).append("</p:NotSpcfdDt></p:EndDt></p:Prd>").toString();
    }
    public static String buildPeriod4FromField69aPeriodOptionD(Object field69aPeriodOptionD) throws Exception{
        return new StringBuilder("<p:StartDt><p:Dt><p:DtTm>").append(makeISODateTimeFromDateAndTime((Date)PropertyUtils.getProperty(field69aPeriodOptionD, "dateYYYYMMDD"), (Date)PropertyUtils.getProperty(field69aPeriodOptionD, "timeHHMMSS"))).append("</p:DtTm><p/:Dt></p:StartDt>")
                .append("<p:EndDt><p:NotSpcfdDt>").append(PropertyUtils.getProperty(field69aPeriodOptionD, "dateCode")).append("</p:NotSpcfdDt></p:EndDt></p:Prd>").toString();
    }
    public static String buildPeriod4FromField69aPeriodOptionE(Object field69aPeriodOptionE) throws Exception{
        return new StringBuilder("<p:StartDt><p:Dt><p:NotSpcfdDt>").append(PropertyUtils.getProperty(field69aPeriodOptionE, "dateCode")).append("</p:NotSpcfdDt><p/:Dt></p:StartDt>")
                .append("<p:EndDt><p:Dt>").append(ISODateTimeFormat.date().print(new DateTime(PropertyUtils.getProperty(field69aPeriodOptionE, "dateYYYYMMDD")))).append("</p:Dt></p:EndDt></p:Prd>").toString();
    }
    public static String buildPeriod4FromField69aPeriodOptionF(Object field69aPeriodOptionF) throws Exception{
        return new StringBuilder("<p:StartDt><p:Dt><p:NotSpcfdDt>").append(PropertyUtils.getProperty(field69aPeriodOptionF, "dateCode")).append("</p:NotSpcfdDt><p/:Dt></p:StartDt>")
                .append("<p:EndDt><p:DtTm>").append(makeISODateTimeFromDateAndTime((Date)PropertyUtils.getProperty(field69aPeriodOptionF, "dateYYYYMMDD"), (Date)PropertyUtils.getProperty(field69aPeriodOptionF, "timeHHMMSS"))).append("</p:DtTm></p:EndDt></p:Prd>").toString();
    }
    public static String buildPeriod4FromField69aPeriodOptionJ(Object field69aPeriodOptionJ) throws Exception{
        return new StringBuilder("<p:PrdCd>").append(PropertyUtils.getProperty(field69aPeriodOptionJ, "dateCode")).append("</p:PrdCd>").toString();
    }

    public static String[] buildOtherIdentificationFromField35BIdentificationOfSecurity(Field35BIdentificationOfSecurityOptionB field35BIdentificationOfSecurity){
        String retVal[] = new String[2];
        DescriptionOfSecurity4Lines descriptionOfSecurity4Lines = field35BIdentificationOfSecurity.getField35BIdOfSecurity().getIdentificationOfSecurity().getDescriptionOfSecurity4Lines();
        if(Objects.nonNull(descriptionOfSecurity4Lines)){
            Optional<String> secIdentification = Arrays.stream(descriptionOfSecurity4Lines.getLine35x()).filter(line -> line.startsWith("/")).findFirst();
            if(secIdentification.isPresent()){
                String []secDtls = secIdentification.get().split("/");
                if(secIdentifierCodeMap.keySet().contains(secDtls[1])){
                    retVal[1] = secIdentifierCodeMap.get(secDtls[1]);
                }else{
                    retVal[1] =secDtls[1].concat("CD"); //Considering 2 digit ISO country code if not matched
                }
                if(secDtls.length >= 3)
                    retVal[0] =secDtls[2];
                else
                    retVal[0] = "Not available";
                return retVal;
            }
        }
        return null;
    }
    public static Optional<?> filterArrayWithQualifier(Object[] objs, String [] possibleXpaths,String qualifier){
        if(qualifier.equals("CLAS"))
            System.out.println("Gotcha!!");
        if(ArrayUtils.isEmpty(objs))
            return Optional.ofNullable(null);
        return filterArrayWithQualifierForElementsAndGetStream(objs, possibleXpaths, qualifier).findFirst();
    }

    public static Optional<Object> filterArrayWithQualifierForElements(Object[] objs, String [] possibleXpaths, String qualifier){
        if(ArrayUtils.isEmpty(objs))
            return Optional.ofNullable(null);
        return Optional.ofNullable(filterArrayWithQualifierForElementsAndGetStream(objs, possibleXpaths, qualifier).collect(Collectors.toList()));
    }

    private static Stream<?> filterArrayWithQualifierForElementsAndGetStream(Object[] objs, String [] possibleXpaths,String qualifier){
        return Arrays.asList(objs).stream().filter(complexDataObject -> {
            try {
                for(String xpath: possibleXpaths) {
                    try{
                        Optional<Object> optionalValue = Optional.ofNullable(JXPathContext.newContext(complexDataObject).getValue(xpath));
                        if(optionalValue.isPresent() && optionalValue.get().toString().equals(qualifier))
                            return true;
                    }catch(JXPathNotFoundException xpnfe){}
                }
                return false;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static Optional<?> getValueByXpath(Object obj, String valuexpath) throws Exception {
        return Optional.ofNullable(JXPathContext.newContext(obj).getValue(valuexpath));
    }
}
