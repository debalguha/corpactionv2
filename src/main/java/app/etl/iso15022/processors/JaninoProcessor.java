package app.etl.iso15022.processors;

import app.etl.iso15022.util.Utility;
import biz.c24.io.swift2016.MT564Message;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component("janinoProcessor")
public class JaninoProcessor implements InitializingBean{
    private static final Logger LOG = LoggerFactory.getLogger(JaninoProcessor.class);

    private Map<String, String> janinoExpressionMap;

    @Value("#{systemProperties['janin.path'] ?: 'config/janino'}")
    private String janinoCollectionPath;

    @Override
    public void afterPropertiesSet() throws Exception {
        janinoExpressionMap = FileUtils.listFiles(new File(janinoCollectionPath), new String[]{"janino"}, false).stream().map(aFile -> {
            try {
                return new String[]{FilenameUtils.getBaseName(aFile.getName()), FileUtils.readFileToString(aFile, "UTF-8")};
            } catch (IOException e) {LOG.error("Unable to process File {}", aFile.getAbsoluteFile(), e); throw new RuntimeException(e);}
        }).collect(Collectors.toMap(array -> array[0], array -> array[1]));
    }

    public Map<String, Object> transformSwiftMessageToMap(final MT564Message swiftMessage) throws Exception {
        Map<String, Object> out = new HashMap<>();
        janinoExpressionMap.forEach((variable, expression) -> {
            try {
                out.put(variable, Utility.executeJaninoScriptAndReturnResult(swiftMessage, expression.toString()));
            } catch (Exception e) {
                LOG.debug("Error while executing script for variable {}", variable, e);
                throw new RuntimeException(e);
            }
        });
        out.put("swiftMessage", swiftMessage);
        return out;
    }
}
