package app.etl.iso15022.parser;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;

public class RawFileParser {
    public static Collection<String> parseFileAndProduceMessages(File inputFile) throws Exception {
        Collection<String> messages = new ArrayList<>();
        boolean endOfMessage = false;
        try(BufferedReader in = new BufferedReader(new FileReader(inputFile))) {
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = in.readLine()) != null){
                if(StringUtils.isEmpty(line)) continue;
                if(line.endsWith("$")) endOfMessage=true;
                if(!endOfMessage){
                    builder.append(line.concat("\r\n"));
                }else{
                    builder.append(line.substring(0, line.length()-1));
                    messages.add(builder.toString());
                    builder = new StringBuilder();
                    endOfMessage=false;
                }
            }
        }
        return messages;
    }
}
