package app.etl.iso15022.converter;

import biz.c24.io.api.C24;
import biz.c24.io.swift2016.MT564Message;
import org.apache.camel.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

@Converter
public class CamelRouteConverters {
    private static final Logger LOG = LoggerFactory.getLogger(CamelRouteConverters.class);
    @Converter
    public static MT564Message convertTo(String str) throws Exception {
        return C24.parse(MT564Message.class).from(str);
    }
    @Converter
    public static Document convertTo(MT564Message aMessage) throws Exception {
        LOG.info("Function of message: "+ aMessage.getBlock4().getSeqA().getField23GFunctionOfTheMessage().getG().getFunction());
        StringWriter strWriter = new StringWriter();
        C24.write(aMessage).as(C24.Format.XML).to(strWriter);
        return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(strWriter.toString())));
    }
}
