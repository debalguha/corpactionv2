package app.etl;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class BBMain {
    public static void main(String args[]) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(BBMain.class, args);
        ClassPathXmlApplicationContext springCamelCtx = new ClassPathXmlApplicationContext(new String[]{"META-INF/spring/camel-context-bb.xml"}, applicationContext);
        CamelContext ctx = new SpringCamelContext(springCamelCtx);
        ctx.start();
    }
}
