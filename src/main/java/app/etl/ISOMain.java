package app.etl;

import app.etl.iso15022.parser.RawFileParser;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

@SpringBootApplication
public class ISOMain {
    public static void main(String args[]) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(ISOMain.class, args);
        ClassPathXmlApplicationContext springCamelCtx = new ClassPathXmlApplicationContext(new String[]{"META-INF/spring/camel-context-iso.xml"}, applicationContext);
        CamelContext ctx = new SpringCamelContext(springCamelCtx);
        ctx.start();
/*        ProducerTemplate producerTemplate = ctx.createProducerTemplate();
        RawFileParser.parseFileAndProduceMessages(new File())
        producerTemplate.sendBody();*/
    }
}
