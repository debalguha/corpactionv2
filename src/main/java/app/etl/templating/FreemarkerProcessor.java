package app.etl.templating;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Configuration;
import freemarker.template.TemplateHashModel;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static org.springframework.ui.freemarker.FreeMarkerTemplateUtils.processTemplateIntoString;

@Component("templateProcessor")
public class FreemarkerProcessor implements InitializingBean{
    @Autowired
    private Configuration cfg;
    private TemplateHashModel staticModels;

    public String processTemplateNewForBB(Map<String, Object> dataMap) throws Exception {
        String corpActionType = dataMap.get("MNEMONIC").toString();
        dataMap.put("statics", staticModels);
        return processTemplateIntoString(cfg.getTemplate(corpActionType+"/CANO.ftl"), dataMap);
    }

    public String processTemplateNewForISO(Map<String, Object> dataMap) throws Exception {
        dataMap.put("statics", staticModels);
        return processTemplateIntoString(cfg.getTemplate("mt/CANO.ftl"), dataMap);
    }

    public String processTemplateUpdate(Map<String, Object> dataMap) throws Exception {
        return "Hello Update!!";
    }

    public String processTemplateCancel(Map<String, Object> dataMap) throws Exception {
        return "Hello Cancel";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        staticModels = new BeansWrapperBuilder(Configuration.VERSION_2_3_25).build().getStaticModels();
    }
}
