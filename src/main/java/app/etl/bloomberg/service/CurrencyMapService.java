package app.etl.bloomberg.service;

import app.etl.bloomberg.persistence.model.CurrencyMap;
import app.etl.bloomberg.persistence.repository.CurrencyMapRepo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Qualifier("currencyMapService")
@Transactional
public class CurrencyMapService implements InitializingBean{
    private static final Logger LOG = LoggerFactory.getLogger(CurrencyMapService.class);
    @Autowired
    private CurrencyMapRepo currencyMapRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, readOnly = true)
    public CurrencyMap lookupCurrency(String bbCurrency){
        return currencyMapRepo.findByBloombergCurrencyCode(bbCurrency);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void createCurrencyMaps(Collection<CurrencyMap> currencyMaps){
        currencyMapRepo.save(currencyMaps);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createCurrencyMaps(loadCurrencyMaps());
    }

    private Collection<CurrencyMap> loadCurrencyMaps() throws Exception {
        List<String> currencyMapLines = FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("masters/currencies.txt").getFile()), "UTF-8");
        return currencyMapLines.stream().map(aLine -> buildCurrencyMap(aLine)).filter(obj -> Objects.nonNull(obj)).collect(Collectors.toList());
    }

    private CurrencyMap buildCurrencyMap(String aLine) {
        String[] split = aLine.split("\t", -1);
        try {
            return new CurrencyMap(split[0], split[1], Float.parseFloat(StringUtils.isEmpty(split[2])?"-1":split[2]), split[3], Integer.parseInt(StringUtils.isEmpty(split[4])?"-1":split[4]),
                    Integer.parseInt(StringUtils.isEmpty(split[5])?"-1":split[5]), split[6], split[7]);
        } catch (Exception e) {
            LOG.error("Exception for Line {}", aLine, e);
            throw e;
        }
    }
}
