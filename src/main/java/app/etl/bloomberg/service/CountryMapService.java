package app.etl.bloomberg.service;

import app.etl.bloomberg.persistence.model.CountryMap;
import app.etl.bloomberg.persistence.repository.CountryMapRepo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
@Qualifier("countryMapService")
public class CountryMapService implements InitializingBean{
    private static final Logger LOG = LoggerFactory.getLogger(CountryMapService.class);
    @Autowired
    private CountryMapRepo countryMapRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, readOnly = true)
    public void createCountryMaps(Collection<CountryMap> countryMaps) {
        countryMapRepo.save(countryMaps);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, readOnly = true)
    public String lookupIsoCountryCode(String bbCountryCode){
        CountryMap countryMap = countryMapRepo.findByExchangeCode(bbCountryCode);
        if(Objects.isNull(countryMap))
            countryMap = countryMapRepo.findByCompositeCode(bbCountryCode);
        if(Objects.isNull(countryMap))
            return bbCountryCode;
        else
            return countryMap.getIsoCountryCode();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createCountryMaps(loadCountryMaps());
    }

    private Collection<CountryMap> loadCountryMaps() throws Exception{
        List<String> countryMapLines = FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("masters/countries.txt").getFile()), "UTF-8");
        return countryMapLines.stream().map(aLine -> buildCountryMap(aLine)).filter(obj -> Objects.nonNull(obj)).collect(Collectors.toList());
    }

    private CountryMap buildCountryMap(String aLine){
        String[] split = aLine.split("\t", -1);
        try {
            return new CountryMap(split[0], split[1], split[2], split[3], split[4], split[5], split[6]);
        } catch (Exception e) {
            LOG.error("Exception for Line {}", aLine, e);
            throw e;
        }
    }
}
