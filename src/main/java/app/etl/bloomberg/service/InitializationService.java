package app.etl.bloomberg.service;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.io.File;
import java.util.List;
import java.util.function.Function;

@Deprecated
public class InitializationService implements InitializingBean{
    @Autowired
    private EntityManagerFactory emf;
    private EntityManager em;

    @Override
    public void afterPropertiesSet() throws Exception {
        em = emf.createEntityManager();
        Function<List<String>, Void> fn = param -> {param.forEach(sql -> em.createNativeQuery(sql).executeUpdate()); return null;};
        executeWithTransaction(loadCountryMapSqls(), fn);
        executeWithTransaction(loadCurrencyMapSqls(), fn);
        executeWithTransaction(loadMetaDataSqls(), fn);
    }
    public void executeWithTransaction(List<String> sqls, Function<List<String>, Void> fn) {
        EntityTransaction transaction = em.getTransaction();
        try{
            transaction.begin();
            fn.apply(sqls);
        } finally {
            transaction.commit();
        }
    }
    public List<String> loadCountryMapSqls() throws Exception {
        return FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("sqls/country_map.sql").getFile()), "UTF-8");
    }
    public List<String> loadCurrencyMapSqls() throws Exception {
        return FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("sqls/currency_map.sql").getFile()), "UTF-8");
    }
    public List<String> loadMetaDataSqls() throws Exception {
        return FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("sqls/metadata.sql").getFile()), "UTF-8");
    }
    /*public void loadCountryMapSqls() throws Exception {
        List<String> sqls = FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("sqls/country_map.sql").getFile()), "UTF-8");
        sqls.forEach(sql -> em.createNativeQuery(sql).executeUpdate());
    }
    public void loadCurrencyMapSqls() throws Exception {
        List<String> sqls = FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("sqls/currency_map.sql").getFile()), "UTF-8");
        sqls.forEach(sql -> em.createNativeQuery(sql).executeUpdate());
    }
    public void loadMetaDataSqls() throws Exception {
        List<String> sqls = FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("sqls/metadata.sql").getFile()), "UTF-8");
        sqls.forEach(sql -> em.createNativeQuery(sql).executeUpdate());
    }*/
}
