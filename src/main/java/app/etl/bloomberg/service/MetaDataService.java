package app.etl.bloomberg.service;

import app.etl.bloomberg.persistence.model.MetaData;
import app.etl.bloomberg.persistence.repository.MetaDataRepo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
@Qualifier("metaDataService")
public class MetaDataService implements InitializingBean{
    private static final Logger LOG = LoggerFactory.getLogger(MetaDataService.class);
    @Autowired
    private MetaDataRepo metaDataRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, readOnly = true)
    public MetaData lookupMetaData(String column, String source) {
        return metaDataRepo.findByColumnAndSource(column, source);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class, readOnly = true)
    public void createMetaData(Collection<MetaData> metaData){
        metaDataRepo.save(metaData);
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        createMetaData(loadMetaDataFromFile());
    }

    private Collection<MetaData> loadMetaDataFromFile() throws Exception{
        List<String> metaDataLines = FileUtils.readLines(new File(Thread.currentThread().getContextClassLoader().getResource("masters/metadata.txt").getFile()), "UTF-8");
        return metaDataLines.stream().map(aLine -> buildMetaData(aLine)).filter(obj -> Objects.nonNull(obj)).collect(Collectors.toList());
    }

    private MetaData buildMetaData(String aLine) {
        String[] split = aLine.split("\t", -1);
        try {
            return new MetaData(split[0], split[1], split[2], split[3]);
        } catch (Exception e) {
            LOG.error("Exception for Line {}", aLine, e);
            throw e;
        }
    }
}
