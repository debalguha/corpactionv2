package app.etl.bloomberg;

/**
 * Created by munia on 07/02/18.
 */
public class MessageFailureException extends Exception {
    public MessageFailureException(String message) {
        super(message);
    }

    public MessageFailureException(Throwable cause) {
        super(cause);
    }

    public MessageFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
