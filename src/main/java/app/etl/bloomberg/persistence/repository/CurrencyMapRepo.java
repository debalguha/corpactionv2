package app.etl.bloomberg.persistence.repository;

import app.etl.bloomberg.persistence.model.CurrencyMap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyMapRepo extends JpaRepository<CurrencyMap, Integer> {
    CurrencyMap findByBloombergCurrencyCode(String bllombergCurrencyCode);
}
