package app.etl.bloomberg.persistence.repository;

import app.etl.bloomberg.persistence.model.CountryMap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryMapRepo extends JpaRepository<CountryMap, Integer>{
    CountryMap findByExchangeCode(String exchangeCode);
    CountryMap findByCompositeCode(String compositeCode);
}
