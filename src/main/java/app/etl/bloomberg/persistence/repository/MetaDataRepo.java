package app.etl.bloomberg.persistence.repository;

import app.etl.bloomberg.persistence.model.MetaData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MetaDataRepo extends JpaRepository<MetaData, Long> {
    @Query("from MetaData m where field = :field and source = :source")
    MetaData findByColumnAndSource(@Param("field") String field, @Param("source") String source);
}
