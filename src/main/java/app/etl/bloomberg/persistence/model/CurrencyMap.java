package app.etl.bloomberg.persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "BB_CURRENCY_MAP")
public class CurrencyMap {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "bb_ccy_code")
    private String bloombergCurrencyCode;
    @Column(name = "bb_ccy_name")
    private String currencyName;
    @Column(name = "div_factor")
    private Float divFactor;
    @Column(name = "iso_ccy_code")
    private String isoCurrencyCode;
    @Column(name = "iso_ccy_num_code")
    private Integer isoCurrencyCodeNumber;
    @Column(name = "iso_minor_unit")
    private Integer isoMinorUnit;
    @Column(name = "iso_ccy_name")
    private String isoCurrencyName;
    @Column(name = "iso_cntry_code")
    private String isoCountryCode;

    public CurrencyMap(){}
    public CurrencyMap(String bloombergCurrencyCode, String currencyName, Float divFactor, String isoCurrencyCode, Integer isoCurrencyCodeNumber, Integer isoMinorUnit, String isoCurrencyName, String isoCountryCode) {
        this.bloombergCurrencyCode = bloombergCurrencyCode;
        this.currencyName = currencyName;
        this.divFactor = divFactor;
        this.isoCurrencyCode = isoCurrencyCode;
        this.isoCurrencyCodeNumber = isoCurrencyCodeNumber;
        this.isoMinorUnit = isoMinorUnit;
        this.isoCurrencyName = isoCurrencyName;
        this.isoCountryCode = isoCountryCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBloombergCurrencyCode() {
        return bloombergCurrencyCode;
    }

    public void setBloombergCurrencyCode(String bloombergCurrencyCode) {
        this.bloombergCurrencyCode = bloombergCurrencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Float getDivFactor() {
        return divFactor;
    }

    public void setDivFactor(Float divFactor) {
        this.divFactor = divFactor;
    }

    public String getIsoCurrencyCode() {
        return isoCurrencyCode;
    }

    public void setIsoCurrencyCode(String isoCurrencyCode) {
        this.isoCurrencyCode = isoCurrencyCode;
    }

    public Integer getIsoCurrencyCodeNumber() {
        return isoCurrencyCodeNumber;
    }

    public void setIsoCurrencyCodeNumber(Integer isoCurrencyCodeNumber) {
        this.isoCurrencyCodeNumber = isoCurrencyCodeNumber;
    }

    public Integer getIsoMinorUnit() {
        return isoMinorUnit;
    }

    public void setIsoMinorUnit(Integer isoMinorUnit) {
        this.isoMinorUnit = isoMinorUnit;
    }

    public String getIsoCurrencyName() {
        return isoCurrencyName;
    }

    public void setIsoCurrencyName(String isoCurrencyName) {
        this.isoCurrencyName = isoCurrencyName;
    }

    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    public void setIsoCountryCode(String isoCountryCode) {
        this.isoCountryCode = isoCountryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CurrencyMap currencyMap = (CurrencyMap) o;

        return bloombergCurrencyCode.equals(currencyMap.bloombergCurrencyCode);
    }

    @Override
    public int hashCode() {
        return bloombergCurrencyCode.hashCode();
    }

    @Override
    public String toString() {
        return "CurrencyMap{" +
                "id=" + id +
                ", bloombergCurrencyCode='" + bloombergCurrencyCode + '\'' +
                ", currencyName='" + currencyName + '\'' +
                ", divFactor=" + divFactor +
                ", isoCurrencyCode='" + isoCurrencyCode + '\'' +
                ", isoCurrencyCodeNumber=" + isoCurrencyCodeNumber +
                ", isoMinorUnit=" + isoMinorUnit +
                ", isoCurrencyName='" + isoCurrencyName + '\'' +
                ", isoCountryCode='" + isoCountryCode + '\'' +
                '}';
    }
}
