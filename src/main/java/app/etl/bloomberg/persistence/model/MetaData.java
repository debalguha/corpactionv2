package app.etl.bloomberg.persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "BB_METADATA_LKUP")
public class MetaData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "field")
    private String column;
    private String source;
    private String target;
    private String description;
    public MetaData(){}
    public MetaData(String column, String source, String target, String description) {
        this.column = column;
        this.source = source;
        this.target = target;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "MetaData{" +
                "id=" + id +
                ", column='" + column + '\'' +
                ", source='" + source + '\'' +
                ", target='" + target + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetaData metaData = (MetaData) o;

        return id.equals(metaData.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void setDescription(String description) {
        this.description = description;
    }


}

