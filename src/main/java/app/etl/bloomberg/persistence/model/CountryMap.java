package app.etl.bloomberg.persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "BB_COUNTRY_MAP")
public class CountryMap {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "EXCH_CODE")
    private String exchangeCode;
    @Column(name = "EXCH_NAME")
    private String exchangeName;
    @Column(name = "MIC_CODE")
    private String micCode;
    @Column(name = "OP_MIC_CODE")
    private String operatingMicCode;
    @Column(name = "OP_MIC_NAME")
    private String micExchangeName;
    @Column(name = "COMP_EXCH")
    private String compositeCode;
    @Column(name = "ISO_CNTRY_CODE")
    private String isoCountryCode;

    public CountryMap(){}
    public CountryMap(String exchangeCode, String exchangeName, String micCode, String operatingMicCode, String micExchangeName, String compositeCode, String isoCountryCode) {
        this.exchangeCode = exchangeCode;
        this.exchangeName = exchangeName;
        this.micCode = micCode;
        this.operatingMicCode = operatingMicCode;
        this.micExchangeName = micExchangeName;
        this.compositeCode = compositeCode;
        this.isoCountryCode = isoCountryCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getMicCode() {
        return micCode;
    }

    public void setMicCode(String micCode) {
        this.micCode = micCode;
    }

    public String getOperatingMicCode() {
        return operatingMicCode;
    }

    public void setOperatingMicCode(String operatingMicCode) {
        this.operatingMicCode = operatingMicCode;
    }

    public String getMicExchangeName() {
        return micExchangeName;
    }

    public void setMicExchangeName(String micExchangeName) {
        this.micExchangeName = micExchangeName;
    }

    public String getCompositeCode() {
        return compositeCode;
    }

    public void setCompositeCode(String compositeCode) {
        this.compositeCode = compositeCode;
    }

    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    public void setIsoCountryCode(String isoCountryCode) {
        this.isoCountryCode = isoCountryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryMap that = (CountryMap) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "CountryMap{" +
                "id=" + id +
                ", exchangeCode='" + exchangeCode + '\'' +
                ", exchangeName='" + exchangeName + '\'' +
                ", micCode='" + micCode + '\'' +
                ", operatingMicCode='" + operatingMicCode + '\'' +
                ", micExchangeName='" + micExchangeName + '\'' +
                ", compositeCode='" + compositeCode + '\'' +
                ", isoCountryCode='" + isoCountryCode + '\'' +
                '}';
    }
}
