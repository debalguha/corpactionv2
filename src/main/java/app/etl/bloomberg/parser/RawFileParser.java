package app.etl.bloomberg.parser;

import app.etl.bloomberg.BloombergRawData;
import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.format.DateTimeFormat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.*;

public class RawFileParser {
    public static Collection<BloombergRawData> parseRawFile(File file) throws Exception {
        Collection<BloombergRawData> data = new ArrayList<>();
        try(BufferedReader in = new BufferedReader(new FileReader(file))) {
            String line;
            boolean startOfData = false;
            while ((line = in.readLine()) != null
                    && !line.equals("END-OF-DATA")) {
                if(line.equals("START-OF-DATA")){
                    startOfData = true;
                    continue;
                }
                if(!startOfData){
                    continue;
                }
                BloombergRawData datam = parseLine(line);
                if(Objects.nonNull(datam)){
                    data.add(datam);
                }
            }
        }
        return data;
    }

    private static BloombergRawData parseLine(String line) throws Exception {
        String []recordElements = line.split("\\|", -1);
        int lastbarPosition = -1;
        BloombergRawData data = new BloombergRawData();
        for(Field f : BloombergRawData.class.getDeclaredFields()){
            BlmColumn annotation = f.getAnnotation(BlmColumn.class);
            if(Objects.isNull(annotation))
                continue;
            lastbarPosition = annotation.barPosition() > lastbarPosition ? annotation.barPosition() : lastbarPosition;
            Optional<String> recordElement =  Optional.of(recordElements[annotation.barPosition()]);
            if(recordElement.isPresent()) {
                if (Arrays.asList(annotation.invalidRecords()).contains(recordElement.get()))
                    return null;
                if (f.getType().equals(Date.class)) {
                    if(!recordElement.get().equals("N.A.")) {
                        BeanUtils.setProperty(data, f.getName(), DateTimeFormat.forPattern("yyyyMMdd").parseDateTime(recordElement.get()).toDate());
                    }
                }else
                    BeanUtils.setProperty(data, f.getName(), recordElement.get());
            }
        }
        if(recordElements.length > lastbarPosition+1){
            Map<String, String> dataElems = new HashMap<>();
            for(int i=0; i<data.getNumberOfFieldsToFollow(); i++){
                try {
                    dataElems.put(recordElements[++lastbarPosition], recordElements[++lastbarPosition]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            data.setDataElements(dataElems);
        }
        return data;
    }
}
