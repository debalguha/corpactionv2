package app.etl.bloomberg.parser;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface BlmColumn {
    int tabPosition();
    int barPosition();
    String[] invalidRecords() default {};
}
