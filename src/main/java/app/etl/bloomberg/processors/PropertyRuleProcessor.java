package app.etl.bloomberg.processors;

import app.etl.bloomberg.MessageFailureException;
import app.etl.rules.JsonRuleBuilder;
import app.etl.rules.Rule;
import app.etl.rules.exceptions.RuleFailureException;
import app.etl.rules.exceptions.RuleNotFoundException;
import app.etl.rules.model.FailureAction;
import app.etl.rules.model.TransformResult;
import app.etl.rules.transform.RuleBasedTransformer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * Created by munia on 07/02/18.
 */
@Component("propertyRuleProcessor")
public class PropertyRuleProcessor implements InitializingBean{
    @Autowired
    private RuleBasedTransformer transformer;
    @Value("classpath:mapping-validations.json")
    private Resource ruleResource;

    private Collection<Rule> rules;

    public Map<String, Object> processForProperty(Map<String, Object> data, String property) throws Exception {
        TransformResult result = null;
        try {
            result = transformer.transform(data.get(property), property, rules);
        } catch (RuleNotFoundException e) {}
          catch (RuleFailureException rfe) {
            if(rfe.getRule().getFailureAction().equals(FailureAction.DROP)) {
                throw new MessageFailureException(rfe);
            }else if(rfe.getRule().getFailureAction().equals(FailureAction.RAISE)) {
                throw rfe;
            }else{
                if(!Objects.isNull(rfe.getRule().getDefaultValue()))
                    data.put(property, rfe.getRule().getDefaultValue());
                else
                    data.put(property, "");
            }
          } catch(Throwable t){
            t.printStackTrace();
            throw (t);
        }
        if(!Objects.isNull(result))
            data.put(property, result.getValue());
        return data;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        rules = JsonRuleBuilder.buildRules(ruleResource);
    }
}
