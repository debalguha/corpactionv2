package app.etl.bloomberg.processors;

import app.etl.bloomberg.BloombergRawData;
import org.apache.camel.Exchange;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("bbDefaultEnricher")
public class BBBlankContentEnricher implements org.apache.camel.Processor{
    private static final Logger LOG = LoggerFactory.getLogger(BBBlankContentEnricher.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        DateTime dt = DateTime.now();
        BloombergRawData bloombergRawData = exchange.getIn().getBody(BloombergRawData.class);
        bloombergRawData.getDataElements().put("BB_STATUS", "");
        bloombergRawData.getDataElements().put("CREATION_TS", DateTimeFormat.forPattern("yyyyMMdd").print(DateTime.now()));
        bloombergRawData.getDataElements().put("LAST_MODIFIED_TS", DateTimeFormat.forPattern("yyyyMMdd").print(DateTime.now()));
        bloombergRawData.getDataElements().put("DELETE_TS", DateTimeFormat.forPattern("yyyyMMdd").print(DateTime.now()));
        bloombergRawData.getDataElements().put("EXCHANGE", "");
        bloombergRawData.getDataElements().put("CAEV", "");
        bloombergRawData.getDataElements().put("DIVI", "");
        //bloombergRawData.getDataElements().put("CP_TAX_AMT", "0");
        exchange.getIn().setBody(bloombergRawData);
    }
}
