package app.etl.bloomberg.processors;

import app.etl.bloomberg.BloombergRawData;
import app.etl.bloomberg.parser.RawFileParser;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Collection;

@Component("feedProcessor")
public class FeedProcessor implements org.apache.camel.Processor{
    private static final Logger LOG = LoggerFactory.getLogger(FeedProcessor.class);
    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn().getBody(File.class);
        LOG.info("File processing: {}", file.getName());
        Collection<BloombergRawData> bloombergRawData = RawFileParser.parseRawFile(file);
        exchange.getIn().setBody(bloombergRawData);
    }
}
