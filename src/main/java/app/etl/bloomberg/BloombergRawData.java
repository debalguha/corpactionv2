package app.etl.bloomberg;

import app.etl.bloomberg.parser.BlmColumn;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class BloombergRawData {
    @BlmColumn(tabPosition = 1, barPosition = 0)
    private String identifier;
    @BlmColumn(tabPosition = 1, barPosition = 1)
    private Long bloombergId;
    @BlmColumn(tabPosition = 1, barPosition = 2)
    private Long bloomberSecurityId;
    @BlmColumn(tabPosition = 1, barPosition = 3, invalidRecords = {"300", "400"})
    private Integer returnCode;
    @BlmColumn(tabPosition = 1, barPosition = 4)
    private Long actionId;
    @BlmColumn(tabPosition = 1, barPosition = 5)
    private String mnemonic;
    @BlmColumn(tabPosition = 1, barPosition = 6)
    private String flag;
    @BlmColumn(tabPosition = 1, barPosition = 7)
    private String companyName;
    @BlmColumn(tabPosition = 1, barPosition = 8)
    private String securityIdType;
    @BlmColumn(tabPosition = 1, barPosition = 9)
    private String secId;
    @BlmColumn(tabPosition = 1, barPosition = 10)
    private String isoCurrency;
    @BlmColumn(tabPosition = 1, barPosition = 11)
    private String marketSectorDescription;
    @BlmColumn(tabPosition = 1, barPosition = 12)
    private String bloomberUniqueId;
    @BlmColumn(tabPosition = 1, barPosition = 13)
    private Date annDate;
    @BlmColumn(tabPosition = 1, barPosition = 14)
    private Date effectDate;
    @BlmColumn(tabPosition = 1, barPosition = 15)
    private Date amenDate;
    @BlmColumn(tabPosition = 1, barPosition = 16)
    private String bloombergGlobalId;
    @BlmColumn(tabPosition = 1, barPosition = 17)
    private String bloombergGlobalCompany;
    @BlmColumn(tabPosition = 1, barPosition = 18)
    private String bloombergSecIdNumberDescription;
    @BlmColumn(tabPosition = 1, barPosition = 19)
    private String feedSource;
    @BlmColumn(tabPosition = 1, barPosition = 20)
    private Integer numberOfFieldsToFollow;
    private Map<String, String> dataElements;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Long getBloombergId() {
        return bloombergId;
    }

    public void setBloombergId(Long bloombergId) {
        this.bloombergId = bloombergId;
    }

    public Long getBloomberSecurityId() {
        return bloomberSecurityId;
    }

    public void setBloomberSecurityId(Long bloomberSecurityId) {
        this.bloomberSecurityId = bloomberSecurityId;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public String getMnemonic() {
        return mnemonic;
    }

    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSecurityIdType() {
        return securityIdType;
    }

    public void setSecurityIdType(String securityIdType) {
        this.securityIdType = securityIdType;
    }

    public String getSecId() {
        return secId;
    }

    public void setSecId(String secId) {
        this.secId = secId;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public String getMarketSectorDescription() {
        return marketSectorDescription;
    }

    public void setMarketSectorDescription(String marketSectorDescription) {
        this.marketSectorDescription = marketSectorDescription;
    }

    public String getBloomberUniqueId() {
        return bloomberUniqueId;
    }

    public void setBloomberUniqueId(String bloomberUniqueId) {
        this.bloomberUniqueId = bloomberUniqueId;
    }

    public Date getAnnDate() {
        return annDate;
    }

    public void setAnnDate(Date annDate) {
        this.annDate = annDate;
    }

    public Date getEffectDate() {
        return effectDate;
    }

    public void setEffectDate(Date effectDate) {
        this.effectDate = effectDate;
    }

    public Date getAmenDate() {
        return amenDate;
    }

    public void setAmenDate(Date amenDate) {
        this.amenDate = amenDate;
    }

    public String getBloombergGlobalId() {
        return bloombergGlobalId;
    }

    public void setBloombergGlobalId(String bloombergGlobalId) {
        this.bloombergGlobalId = bloombergGlobalId;
    }

    public String getBloombergGlobalCompany() {
        return bloombergGlobalCompany;
    }

    public void setBloombergGlobalCompany(String bloombergGlobalCompany) {
        this.bloombergGlobalCompany = bloombergGlobalCompany;
    }

    public String getBloombergSecIdNumberDescription() {
        return bloombergSecIdNumberDescription;
    }

    public void setBloombergSecIdNumberDescription(String bloombergSecIdNumberDescription) {
        this.bloombergSecIdNumberDescription = bloombergSecIdNumberDescription;
    }

    public String getFeedSource() {
        return feedSource;
    }

    public void setFeedSource(String feedSource) {
        this.feedSource = feedSource;
    }

    public Integer getNumberOfFieldsToFollow() {
        return numberOfFieldsToFollow;
    }

    public void setNumberOfFieldsToFollow(Integer numberOfFieldsToFollow) {
        this.numberOfFieldsToFollow = numberOfFieldsToFollow;
    }

    public Map<String, String> getDataElements() {
        return dataElements;
    }

    public void setDataElements(Map<String, String> dataElements) {
        this.dataElements = dataElements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BloombergRawData that = (BloombergRawData) o;
        return Objects.equals(bloombergId, that.bloombergId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bloombergId);
    }

    @Override
    public String toString() {
        return "BloombergRawData{" +
                "identifier='" + identifier + '\'' +
                ", bloombergId=" + bloombergId +
                ", bloomberSecurityId=" + bloomberSecurityId +
                ", returnCode=" + returnCode +
                ", actionId=" + actionId +
                ", mnemonic='" + mnemonic + '\'' +
                ", flag='" + flag + '\'' +
                ", companyName='" + companyName + '\'' +
                ", securityIdType='" + securityIdType + '\'' +
                ", secId='" + secId + '\'' +
                ", isoCurrency='" + isoCurrency + '\'' +
                ", marketSectorDescription='" + marketSectorDescription + '\'' +
                ", bloomberUniqueId='" + bloomberUniqueId + '\'' +
                ", annDate=" + annDate +
                ", effectDate=" + effectDate +
                ", amenDate=" + amenDate +
                ", bloombergGlobalId='" + bloombergGlobalId + '\'' +
                ", bloombergGlobalCompany='" + bloombergGlobalCompany + '\'' +
                ", bloombergSecIdNumberDescription='" + bloombergSecIdNumberDescription + '\'' +
                ", feedSource='" + feedSource + '\'' +
                ", numberOfFieldsToFollow=" + numberOfFieldsToFollow +
                ", dataElements=" + dataElements +
                '}';
    }
}


