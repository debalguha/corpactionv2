package app.etl.bloomberg.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Objects;

public class BBFunctions {
    public static Date toDateFromString(String value, String format) throws Exception {
        if(StringUtils.isEmpty(value) || value.equals("N.A."))
            return null;
        try {
            return DateTimeFormat.forPattern(format).parseDateTime(value).toDate();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    public static String toStringFromDate(Date date, String format) throws Exception {
        if(Objects.isNull(date))
            return "";
        return new DateTime(date).toString(DateTimeFormat.forPattern(format));
    }
}
