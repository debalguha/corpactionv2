package app.etl.bloomberg.util;

import app.etl.bloomberg.BloombergRawData;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Component("bbMappingUtil")
public class BBMappingUtil {
    public Map<String, Object> convertMap(BloombergRawData rawData) throws Exception{
        Map<String, Object> outMap = new HashMap<>();
        CSVFormat.TDF.withHeader("Source", "Target", "IsField", "Default", "Function").withFirstRecordAsHeader()
                .parse(new FileReader(Thread.currentThread().getContextClassLoader().getResource("converterMap_bbtransformed.txt").getFile()))
                .forEach(csvRecord ->{
                    Boolean isField = Boolean.valueOf(csvRecord.get("IsField"));
                    Object value = null;
                    String defaultValue = csvRecord.get("Default");
                    if(isField){
                        try {
                            value = FieldUtils.readField(rawData, csvRecord.get("Source"), true);
                            if(Objects.isNull(value) && !Objects.isNull(defaultValue))
                                value = defaultValue;
                        } catch (Exception e) { e.printStackTrace();}
                    }else{
                        value = Optional.ofNullable(rawData.getDataElements().get(csvRecord.get("Source"))).orElse(defaultValue);
                    }
                    outMap.put(csvRecord.get("Target"), value);
                });

        return outMap;
    }
}
