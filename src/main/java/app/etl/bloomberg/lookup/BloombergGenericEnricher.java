package app.etl.bloomberg.lookup;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.etl.bloomberg.BloombergRawData;
import app.etl.bloomberg.persistence.model.CurrencyMap;
import app.etl.bloomberg.persistence.model.MetaData;
import app.etl.bloomberg.service.CurrencyMapService;
import app.etl.bloomberg.service.MetaDataService;

@Component("bbEnricher")
public class BloombergGenericEnricher {
    private static final Logger LOG = LoggerFactory.getLogger(BloombergGenericEnricher.class);
    @Autowired
    private CurrencyMapService currencyService;
    @Autowired
    private MetaDataService mdService;

    public BloombergRawData enrichMetaData(BloombergRawData bbData, String fieldToEnrich, String fieldToLookup, String sourceField) {
        Optional<MetaData> metaData = Optional.ofNullable(mdService.lookupMetaData(fieldToLookup, sourceField));
        /*if(bbData.getBloombergId().longValue() == 43277226)
            System.out.println("Gotcha!!");*/
        if (!metaData.isPresent())
            return bbData;
        try {
            enrichBBData(bbData, fieldToEnrich, metaData.get().getTarget());
        } catch (Exception e) {
            LOG.error("Unable to enrich Meta data for field {} and source {}", fieldToEnrich, sourceField, e);
            e.printStackTrace();
        }
        return bbData;
    }

    public BloombergRawData enrichCurrency(BloombergRawData bbData, String fieldToEnrich, String fieldToLookup, String fieldValue) {
        Optional<CurrencyMap> lookupCurrency = Optional.ofNullable(currencyService.lookupCurrency(fieldValue));
        if (!lookupCurrency.isPresent())
            return bbData;
        try {
            enrichBBData(bbData, fieldToEnrich, BeanUtils.getProperty(lookupCurrency.get(), fieldToLookup));
        } catch (Exception e) {
            LOG.error("Unable to enrich Currency for field {} and value {}", fieldToEnrich, fieldValue, e);
            e.printStackTrace();
        }
        return bbData;
    }

    private void enrichBBData(BloombergRawData bbData, String fieldToEnrich, String value) throws Exception {
        Field f = null;
        try {
            f = BloombergRawData.class.getDeclaredField(fieldToEnrich);
        } catch (NoSuchFieldException e) {
            LOG.debug("No field {} found", fieldToEnrich);
        }
        if (f != null) {
                enrichField(bbData, f, value);
        } else {
            bbData.getDataElements().put(fieldToEnrich, value);
        }
    }

    private void enrichField(BloombergRawData bbData, Field f, String value) throws Exception {
        if (f.getType().equals(Date.class)) {
            BeanUtils.setProperty(bbData, f.getName(), DateTimeFormat.forPattern("yyyyMMdd").parseDateTime(value).toDate());
        } else {
            BeanUtils.setProperty(bbData, f.getName(), value);
        }
    }
}
