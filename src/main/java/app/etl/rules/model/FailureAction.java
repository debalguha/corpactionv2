
package app.etl.rules.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FailureAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FailureAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DROP"/>
 *     &lt;enumeration value="RAISE"/>
 *     &lt;enumeration value="Ignore"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FailureAction")
@XmlEnum
public enum FailureAction {

    DROP("DROP"),
    RAISE("RAISE"),
    IGNORE("IGNORE");
    private final String value;

    FailureAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FailureAction fromValue(String v) {
        for (FailureAction c: FailureAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
