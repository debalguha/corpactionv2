package app.etl.rules.model;


import app.etl.rules.Rule;

import java.util.Collection;

public class RuleImpl implements Rule {
    private String forKey;
    private Class<?> type;
    private String srcFormat;
    private String targetFormat;
    private boolean useDefaultIfNotPresent;
    private FailureAction failureAction;
    private Collection<String> validValues;
    private Collection<String> invalidValues;
    private String defaultValue;

    public String getForKey() {
        return forKey;
    }

    public void setForKey(String forKey) {
        this.forKey = forKey;
    }


    public boolean isUseDefaultIfNotPresent() {
        return useDefaultIfNotPresent;
    }

    public void setUseDefaultIfNotPresent(boolean useDefaultIfNotPresent) {
        this.useDefaultIfNotPresent = useDefaultIfNotPresent;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public FailureAction getFailureAction() {
        return failureAction;
    }

    public void setFailureAction(FailureAction failureAction) {
        this.failureAction = failureAction;
    }

    public Collection<String> getValidValues() {
        return validValues;
    }

    public void setValidValues(Collection<String> validValues) {
        this.validValues = validValues;
    }

    public Collection<String> getInvalidValues() {
        return invalidValues;
    }

    public void setInvalidValues(Collection<String> invalidValues) {
        this.invalidValues = invalidValues;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String getSrcFormat() {
        return srcFormat;
    }

    public void setSrcFormat(String srcFormat) {
        this.srcFormat = srcFormat;
    }

    @Override
    public String getTargetFormat() {
        return targetFormat;
    }

    public void setTargetFormat(String targetFormat) {
        this.targetFormat = targetFormat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RuleImpl rule = (RuleImpl) o;

        return forKey != null ? forKey.equals(rule.forKey) : rule.forKey == null;
    }

    @Override
    public int hashCode() {
        return forKey != null ? forKey.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "RuleImpl{" +
                "forKey='" + forKey + '\'' +
                ", type=" + type +
                ", srcFormat='" + srcFormat + '\'' +
                ", targetFormat='" + targetFormat + '\'' +
                ", useDefaultIfNotPresent=" + useDefaultIfNotPresent +
                ", failureAction=" + failureAction +
                ", validValues=" + validValues +
                ", invalidValues=" + invalidValues +
                ", defaultValue='" + defaultValue + '\'' +
                '}';
    }
}
