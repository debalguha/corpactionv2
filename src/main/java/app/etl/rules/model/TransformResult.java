package app.etl.rules.model;

import app.etl.rules.Rule;
public class TransformResult {
    private Object value;
    private Rule rule;

    public TransformResult(Object value, Rule rule) {
        this.value = value;
        this.rule = rule;
    }

    public Object getValue() {
        return value;
    }

    public Rule getRule() {
        return rule;
    }
}
