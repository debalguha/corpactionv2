package app.etl.rules.exceptions;

import app.etl.rules.Rule;

/**
 * Created by munia on 07/02/18.
 */
public class RulesException extends Exception{
    protected Rule rule;
    protected Object value;
    public RulesException(String message, Rule rule, Object value) {
        super(message);
        this.rule = rule;
        this.value = value;
    }
    public RulesException(String message) {
        super(message);
    }

    public Rule getRule() {
        return rule;
    }

    public Object getValue() {
        return value;
    }
}
