package app.etl.rules.exceptions;

import app.etl.rules.Rule;

/**
 * Created by munia on 07/02/18.
 */
public class RuleFailureException extends RulesException {
    protected FailureReason reason;

    public RuleFailureException(String message, Rule rule, Object value, FailureReason reason) {
        super(message, rule, value);
        this.reason = reason;
    }

    public FailureReason getReason() {
        return reason;
    }

}
