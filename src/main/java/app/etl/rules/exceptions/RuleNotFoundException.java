package app.etl.rules.exceptions;

import app.etl.rules.Rule;

/**
 * Created by munia on 07/02/18.
 */
public class RuleNotFoundException extends RulesException{
    public RuleNotFoundException(String message, Object value) {
        super(message, null, value);
    }

    public RuleNotFoundException(String message) {
        super(message);
    }
}
