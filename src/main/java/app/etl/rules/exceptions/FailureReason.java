package app.etl.rules.exceptions;

/**
 * Created by munia on 07/02/18.
 */
public enum FailureReason {
    INVALID_VALUE_FOUND, NO_VALID_VALUE_MATCHED
}
