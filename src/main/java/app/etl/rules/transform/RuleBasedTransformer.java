package app.etl.rules.transform;

import app.etl.bloomberg.util.BBFunctions;
import app.etl.rules.Rule;
import app.etl.rules.exceptions.FailureReason;
import app.etl.rules.exceptions.RuleFailureException;
import app.etl.rules.exceptions.RuleNotFoundException;
import app.etl.rules.model.TransformResult;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component("ruleBasedTransformer")
public class RuleBasedTransformer {
    public TransformResult transform(Object value, String property, Collection<Rule> rules) throws Exception{
        List<Rule> ruleList = rules.stream().filter(rule -> rule.getForKey().equals(property)).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(ruleList))
            throw new RuleNotFoundException(String.format("No matching rule found for property: %s", property));
        return transform(value, ruleList.iterator().next());
    }
    private TransformResult transform(Object value, Rule rule) throws Exception {
        if(!CollectionUtils.isEmpty(rule.getInvalidValues())
                && rule.getInvalidValues().contains(value.toString()))
            throw new RuleFailureException("Rule processing failed", rule, value, FailureReason.INVALID_VALUE_FOUND);
        if(!CollectionUtils.isEmpty(rule.getValidValues())
                && !rule.getValidValues().contains(value.toString()))
            throw new RuleFailureException("Rule processing failed", rule, value, FailureReason.NO_VALID_VALUE_MATCHED);
        if(Objects.isNull(value) && !Objects.isNull(rule.getDefaultValue()))
            return new TransformResult(rule.getDefaultValue(), rule);
        if(rule.getType().equals(Date.class) && !StringUtils.isEmpty(rule.getTargetFormat()))
            return transformDateFormat(value, rule);
        return new TransformResult(value, rule);
    }

    private TransformResult transformDateFormat(Object value, Rule rule) throws Exception {
        if(value.getClass().equals(Date.class))
            return new TransformResult(BBFunctions.toStringFromDate((Date) value, rule.getTargetFormat()), rule);
        return new TransformResult(BBFunctions.toStringFromDate(BBFunctions.toDateFromString(value.toString(), rule.getSrcFormat()), rule.getTargetFormat()), rule);
    }
}
