package app.etl.rules;


import app.etl.rules.model.FailureAction;

import java.util.Collection;

public interface Rule {
    String getForKey();
    Class<?> getType();
    String getSrcFormat();
    String getTargetFormat();
    boolean isUseDefaultIfNotPresent();
    FailureAction getFailureAction();
    Collection<String> getValidValues();
    Collection<String> getInvalidValues();
    String getDefaultValue();
}
