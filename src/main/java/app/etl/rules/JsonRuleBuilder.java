package app.etl.rules;

import app.etl.rules.model.RuleImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

public class JsonRuleBuilder {
    public static Collection<Rule> buildRules(Resource resource) throws Exception {
        return new ObjectMapper().readValue(resource.getInputStream(), new TypeReference<List<RuleImpl>>() { });
    }
}
